@extends('blades.base')

{{-- @section('page title', 'Index') --}}

@section('css')
<link rel="stylesheet" href="{{ asset('css/custom.css')}}">
@endsection

@section('innerbanner')

<!-- innerbanner -->    

<div class="banner">
	<s-banner></s-banner>
</div>

<!-- //innerbanner -->

@endsection

@section('breadcrumbs')
@endsection

@section('horizontal tab')

@endsection

@section('vertical tab')

<!-- innerbanner -->	

<!-- //innerbanner -->

<!-- breadcrumbs -->
	<div class="w3layouts-breadcrumbs text-center">
		<div class="container">
			<span class="agile-breadcrumbs"><a href="/"><i class="fa fa-home home_1"></i></a> / <span>Support</span></span>
		</div>
	</div>
	<!-- //breadcrumbs -->

	<!-- Support-page -->
	    <div class="support w3layouts-content">
		<div class="container">
			<h3 class="w3-head">Support Page</h3>
            <div class="col-md-8 w3ls-supportform">
	       <form action="{{ route('frontend.support_post') }}" method="POST" name="sentMessage" id="contactForm" novalidate>
			   {{ csrf_field() }}
                <select required name="request_type">
                      <option value="Payment">Payment</option>
                      <option value="Recharge">Recharge</option>
                      <option value="Booking">Booking</option>
                      <option value="Account">Account</option>
                      <option value="Others">Others</option>
                    </select>
					<div class="control-group form-group">
						<div class="controls">
							<input type="text" class="form-control" name="name_is" placeholder="Enter your Name" id="name" required data-validation-required-message="Please enter your name.">
						</div>
					</div>
					<div class="control-group form-group">
						<div class="controls">
							<input type="email" class="form-control" name="email" placeholder="Enter your Email ID" id="email" required data-validation-required-message="Please enter your email address.">
						</div>
					</div>
                    <div class="control-group form-group">
                        <div class="controls">
                            <input type="tel" class="form-control" name="phone" placeholder="Enter mobile Number" id="phone" required="" data-validation-required-message="Please enter your phone number." aria-invalid="false">
							<p class="help-block"></p>
						</div>
                    </div>
						<div class="control-group form-group">
							<div class="controls">
								<textarea rows="10" cols="100" class="form-control" placeholder="Please write here" id="message" name="message" required data-validation-required-message="Please enter your message" maxlength="999" style="resize:none"></textarea>
							</div>
						</div>
						<div id="success"></div>
						<!-- For success/fail messages -->
						<button type="submit" class="submit btn btn-primary">Create Ticket</button>
					<div class="clearfix"></div>	
					</form>
                </div>
            <div class="col-md-4 agileits-support">
                <ul>
                    <li><strong>Mail to:</strong> <a href="mailto:care@zapwallet.in">care@zapwallet.in</a></li>
                    <li><a class="w3-faq" href="{{ route('frontend.faq') }}">Faq</a></li>
                </ul>
            </div>
            <div class="clearfix"></div>
<!-- js files for contact from validation -->
		<script src="/js/jqBootstrapValidation.js"></script>
		<script src="/js/contact_me.js"></script>
	<!-- //js files for contact from validation -->

	</div>
</div>
    <!--//Support-page-->


<!-- subscribe -->
@endsection

@section('tab title')
    Plan
@endsection
<!-- /html -->