@extends('blades.base')



{{-- @section('page title', 'Index') --}}



@section('innerbanner')

    <!-- innerbanner -->

    <div class="banner-board-band" style="min-height:471px !important">



    </div>

    <!-- //innerbanner -->

@endsection



@section('breadcrumbs')

@endsection



@section('horizontal tab')



@endsection



@section('vertical tab')



    <!-- innerbanner -->



    <!-- //innerbanner -->



    <!-- breadcrumbs -->

    <div class="w3layouts-breadcrumbs text-center">

        <div class="container">

            <span class="agile-breadcrumbs"><a href="/"><i class="fa fa-home home_1"></i></a> / <span>Mobile Confirmation</span></span>

        </div>

    </div>

    <!-- //breadcrumbs -->



    <!-- Support-page -->

    <div class="support w3layouts-content">

        <div class="container">

            <h3 class="w3-head">Mobile Confirmation</h3>


            <div class="col-md-8 w3ls-supportform">

                <div id="" style="padding:10px;">

                    @php
                    session_start();
                        if(!isset($_SESSION['first_time'])){
                            $register=1;
                            $_SESSION['first_time']=1;

                        }else{
                            $register=0;
                        }

                        if(!isset($socea)){
                            $socea=0;
                        
                        }
                        if(!isset($curu)){
                            $curu =0;
                        }

                        if(!isset($wrong)){
                            $wrong =0;
                        }
                    @endphp
                    @if( $wrong == 1)
                        You have specified a wrong OTP number.<br><br>
                        @endif

                    @if( Request::get('expired'))
                        Your code has expired. Please resend.<br><br>
                    @endif 

                    @if( Request::get('toomany')  or $curu ==1)
                    Too many re-sent SMSes. Please try again in {{$remainingTime}}<br><br>
                    @endif
                    @if($register==1)
                        We have sent a OTP number to your phone number. Please complete the field.<br><br>
                        @endif
                    @if($socea==1 or $register == 1)
                        Time left:<div id="secs"></div>
                    @endif


                    <br>


                </div>




            <form action="{{url('/verifyOtp')}}" method="get" name="sentMessage" id="socea" novalidate>



                    <div class="control-group form-group">

                        <div class="controls">

                            <input type="number"  style="width:24%;" class="form-control" name="otp" placeholder="Enter your OTP" id="otp" required data-validation-required-message="Please enter your name.">

                        </div>

                    </div>



                    <div id="success"></div>

                    <!-- For success/fail messages -->

                    <input type="submit" onclick="document.getElementById('socea').submit();return false;" class="submit btn btn-primary">

                    <a class="submit btn btn-primary" href="/sendOtp" >RE-SEND</a>
                    <div class="clearfix"></div>

                </form>

            </div>

            <div class="col-md-4 agileits-support">

                <ul>

                    <li><strong>Join us:</strong> <a href="https://chat.whatsapp.com/invite/GvKV1tcvQzsEgl5FK8R1Xe"><img src="http://pngimg.com/uploads/whatsapp/whatsapp_PNG11.png" width="10%"></a> </li>

                    <li><strong>Mail to:</strong> <a href="mailto:care@zapwallet.in">care@zapwallet.in</a></li>

                    <li><a class="w3-faq" href="frontend/faq">Help?</a></li>

                </ul>

            </div>

            <div class="clearfix"></div>

            <!-- js files for contact from validation -->

            <script src="/js/jqBootstrapValidation.js"></script>

            <script src="/js/contact_me.js"></script>

            <!-- //js files for contact from validation -->



        </div>

    </div>

    <!--//Support-page-->





    <!-- subscribe -->

@endsection



@section('tab title')

    Plan

@endsection

<!-- /html -->

@if(! Request::get('expired') && ! Request::get('toomany') )
    <script>

        var seconds_left = 60;
        var interval = setInterval(function() {
            document.getElementById('secs').innerHTML = --seconds_left;

            if (seconds_left <= 0)
            {

                window.location.href = "http://zapwallet.in/resetOtp";

                clearInterval(interval);
            }
        }, 1000);
    </script>            @endif