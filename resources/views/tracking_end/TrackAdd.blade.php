@extends('layouts.system')
@section('content')
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{url('/admin/trackadd')}}">Tracking / Add New</a>
            <i class="fa fa-circle"></i>
        </li>

    </ul>
</div>
<h1 class="page-title"> Add New Page
    <small>sender information, recieve information</small>
</h1>
<div class="row">
  <div class="col-md-12">
    <!-- BEGIN VALIDATION STATES-->
    <div class="portlet light portlet-fit portlet-form bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-settings font-red"></i>
                <span class="caption-subject font-red sbold uppercase">Add New Track</span>
            </div>

        </div>
        <div class="portlet-body">
            <!-- BEGIN FORM-->
            <form action="" method="post" id="form_sample_1" class="form-horizontal">
                <div class="form-body">
                    <div class="alert alert-danger display-hide">
                        <button class="close" data-close="alert"></button> You have some form errors. Please check below. 
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-md-3">Tracking Number
                            <span class="required"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input name="number" id="track_num" type="text" class="form-control" /> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Destination
                            <span class="required"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input type="text" name="des" id="des" data-required="1" class="form-control" /> 
                        </div>
                    </div>

                    <h3 class="form-section">Sender Information</h3>

                    <div class="form-group">
                        <label class="control-label col-md-3">Company Name
                            <span class="required"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input type="text" name="comname" id="comname" data-required="1" class="form-control" /> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Location/Address
                            <span class="required"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input type="text" name="s_loc" id="s_loc" data-required="1" class="form-control" /> 
                        </div>
                    </div>
                    <h3 class="form-section">Recieve Information</h3>
                    <div class="form-group">
                        <label class="control-label col-md-3">Name
                            <span class="required"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input type="text" name="r_name" id="r_name" data-required="1" class="form-control" /> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Address
                            <span class="required"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input type="text" name="r_address" id="r_address" data-required="1" class="form-control" /> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Contact
                            <span class="required"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input type="text" name="r_contact" id="r_contact" data-required="1" class="form-control" /> 
                        </div>
                    </div>
                    <h3 class="form-section">shipment Information</h3>
                    <div class="form-group">
                        <label class="control-label col-md-3">No. of itmes
                            <span class="required"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input type="text" name="sh_no" id="sh_no" data-required="1" class="form-control" /> 
                        </div>
                        
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">COD Amount
                            <span class="required"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input type="text" name="sh_cod" id="sh_cod" data-required="1" class="form-control" /> 
                        </div>
                        
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Customs Value
                            <span class="required"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input type="text" name="sh_cus" id="sh_cus" data-required="1" class="form-control" /> 
                        </div>
                        
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Location
                            <span class="required"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input type="text" name="sh_loc" id="sh_loc" data-required="1" class="form-control" /> 
                        </div>
                        
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Good Des
                            <span class="required"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input type="text" name="good_des" id="good_des" data-required="1" class="form-control" /> 
                        </div>
                        
                    </div>

                    <div class="form-group">
                       <label class="control-label col-md-3">Weight
                        <span class="required"> * </span>
                    </label>
                    <div class="col-md-4">
                        <input type="text" name="weight" id="weight" data-required="1" class="form-control" /> 
                    </div>
                    

                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <button type="submit" class="btn green">Add information</button>
                            <button type="button" onclick="form_empty()" class="btn grey-salsa btn-outline">Cancel</button>
                        </div>
                    </div>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
    <!-- END VALIDATION STATES-->
</div>
<script type="text/javascript">
   
   var ajax_url = '{{URL::to("admin/track_add")}}';
   function form_empty(){
       $("#track_num").val('');
       $("#des").val('');
       $("#comname").val('');
       $("#r_name").val('');
       $("#r_address").val('');
       $("#r_contact").val('');
       $("#sh_no").val('');
       $("#sh_cus").val('');
       $("#sh_cod").val('');
       $("#sh_loc").val('');
       $("#s_loc").val(''); 
   }

</script>


<!-- END VALIDATION STATES-->
</div>
</div>



@endsection