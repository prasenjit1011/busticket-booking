<?php

namespace App\Console\Commands;

use App\models\Transaction;
use Illuminate\Console\Command;
use GuzzleHttp\Client;
use App\models\Dthtransaction;
use App\models\Mobiletransaction;
use App\models\Postpaidtransaction;
use App\models\Prepaidoperator;

class UpdateTransactionStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'UpdateTransactionStatus:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
		$arr['Prepaid'] = Mobiletransaction::where('status','PENDING')->get();
        $arr['Postpaid'] = Postpaidtransaction::where('status','PENDING')->get();
        $arr ['Dth'] = Dthtransaction::where('status','PENDING')->get();  
        
        
         try {
            foreach ($arr['Prepaid'] as $prTransaction) {
                if ($prTransaction->status == 'PENDING') {

                    $uri    = 'https://www.rupeebiz.com/apiservice.asmx/GetRechargeStatus?apiToken=0cd96c2de62047c59e7d06e54c323257&reqid=' . $prTransaction->order_id;
                    $res    = $client->get($uri);
                    $body   = (array)simplexml_load_string($res->getBody());


                    $prTransaction->status = $body['status'];
                    $prTransaction->save();
                    
                    // if status is refund or failed, update wallet
                    if($body['status'] == 'REFUND' || $body['status'] == 'FAILED')
                    {
                        $user->wallet = $user->wallet + $prTransaction->amount;
                        $user->save();
                    }
                }
            }

            foreach ($arr['Postpaid'] as $postTransaction) {
                if ($postTransaction->status == 'PENDING') {

                    $uri    = 'https://www.rupeebiz.com/apiservice.asmx/GetRechargeStatus?apiToken=0cd96c2de62047c59e7d06e54c323257&reqid=' . $postTransaction->order_id;
                    $res    = $client->get($uri);
                    $body   = (array)simplexml_load_string($res->getBody());

                    

                    $postTransaction->status = $body['status'];
                    $postTransaction->save();
                    
                    // if status is refund or failed, update wallet
                    if($body['status'] == 'REFUND' || $body['status'] == 'FAILED')
                    {
                        $user->wallet = $user->wallet + $postTransaction->amount;
                        $user->save();
                    }
                }
            }

            foreach ($arr['Dth'] as $dthTransaction) {
                if ($dthTransaction->status == 'PENDING') {

                    $uri    = 'https://www.rupeebiz.com/apiservice.asmx/GetRechargeStatus?apiToken=0cd96c2de62047c59e7d06e54c323257&reqid=' . $dthTransaction->order_id;
                    $res    = $client->get($uri);
                    $body   = (array)simplexml_load_string($res->getBody());

                    $dthTransaction->status = $body['status'];
                    $dthTransaction->save();
                    
                    // if status is refund or failed, update wallet
                    if($body['status'] == 'REFUND' || $body['status'] == 'FAILED')
                    {
                        $user->wallet = $user->wallet + $dthTransaction->amount;
                        $user->save();
                    }
                }
            }
        } catch (Exception $e) {
            \Log::info([ $e->getMessage()]);
        }
        
	}
}
