@extends('layouts.app_admin')

@section('content')

<div class="content-wrapper">
    
    <section class="content-header">
        <h1>
            Coupon
        	<small>Preview page</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Coupon</li>
        </ol>
    </section>
    
    <section class="content">
        <div class="row">
        	<div class="col-md-8 col-sm-offset-2">
            	<div class="box box-success">
                	<div class="box-header with-border">
                    	<h3 class="box-title">Coupon</h3> 
                	</div>
                	<div class="box-body">

                		<div class="col-sm-12">
							
							<div class="card">
								<div class="card-header">
									<h3 class="text-center"><i class="fa fa-pencil"></i> Create Form of <code><b>Coupon</b></code> </h3>
								</div>
								<div class="card-body">

									<form class="form-horizontal" method="POST" action="{{ url('/coupon') }}" enctype="multipart/form-data">
										{{ csrf_field() }}
										<div class="required form-group {{ $errors->has('coupon_code') ? 'has-error' : ''}}">
									        <label class="control-label col-sm-3">Coupon Code:</label>
									        <div class="col-xs-9 col-sm-9">
									        		<input type="text" class="form-control"  placeholder="Enter Coupon Code" name="coupon_code" required="required">
									    	        <span class="text-danger">
									    			    {{ $errors->first('coupon_code') }}
									    		    </span>
									        </div>
										</div>

										<div class="required form-group {{ $errors->has('coupon_name') ? 'has-error' : ''}}">
									        <label class="control-label col-sm-3">Coupon Name:</label>
									        <div class="col-xs-9 col-sm-9">
									        		<input type="text" class="form-control"  placeholder="Enter Coupon Name" name="coupon_name" required="required">
									    	        <span class="text-danger">
									    			    {{ $errors->first('coupon_name') }}
									    		    </span>
									        </div>
										</div>

										<div class="form-group required {{ $errors->has('coupon_type') ? 'has-error' : '' }}">
									    
									    <label class="control-label col-sm-3 col-xs-3">Coupon Type:</label>
									    <div class="col-xs-9 col-sm-9">
									    	<select class="form-control"  placeholder="Select Coupon Type" name="coupon_type" id="coupon_type" required="required">
									    		<option value="">Select Coupon Type</option>
											    <option value="Discount">Discount</option>
											    <option value="Cash Back">Cash Back</option>
											    <option value="Gift">Gift</option>
											</select>
									        <span class="text-danger">
									            {{ $errors->first('coupon_type') }}
									        </span>
									       
									    </div>
									</div>

									

									<span id="type_info_show"></span>

									<span id='radio_info_show'></span>

									<div class="form-group {{ $errors->has('start_date') ? 'has-error' : ''}}">
								        <label class="control-label col-xs-3 col-sm-3" for="start_date">Start Date:</label>
								        <div class="col-xs-9 col-sm-9">
							    	        <input type='text' class="form-control"  id='start_date' placeholder="Enter Date" name="start_date" />
							    	        <span class="text-danger">
							    			    {{ $errors->first('start_date') }}
							    		    </span>
								        </div>
									</div>

									<div class="form-group {{ $errors->has('end_date') ? 'has-error' : ''}}">
								        <label class="control-label col-xs-3 col-sm-3" for="end_date">End Date:</label>
								        <div class="col-xs-9 col-sm-9">
							    	        <input type='text' class="form-control"  id='end_date' placeholder="Enter Date" name="end_date" />
							    	        <span class="text-danger">
							    			    {{ $errors->first('end_date') }}
							    		    </span>
								        </div>
									</div>

										<div class="form-group {{ $errors->has('usage_limit') ? 'has-error' : ''}}">
									        <label class="control-label col-sm-3" for="">Usage Limit:</label>
									        <div class="col-xs-9 col-sm-9">
								        		<input type="number" class="form-control"  placeholder="Enter Usage Limit" name="usage_limit">
								    	        <span class="text-danger">
								    			    {{ $errors->first('usage_limit') }}
								    		    </span>
									        </div>
										</div>

										<div class="form-group {{ $errors->has('action') ? 'has-error' : ''}}">
									        <label class="control-label col-xs-3 col-sm-3" for="action">Action:</label>
									        <div class="col-xs-9 col-sm-9">
								    	        <select class="form-control" id='action' placeholder="Enter Action" name="action">
	                                                <option value="">Select Action</option>
	                                                <option value="Sign Up">Sign Up</option>
	                                                <option value="Add Money">Add Money</option>
	                                            </select>
									        </div>
										</div>

										<div class="form-group {{ $errors->has('operator') ? 'has-error' : ''}}">
									        <label class="control-label col-xs-3 col-sm-3" for="operator">Operator:</label>
									        <div class="col-xs-9 col-sm-9">
								    	        <select class="form-control" id='operator' placeholder="Enter Operator" name="operator">
	                                                <option value="">Select Operator</option>
	                                                @foreach($prepaidOperators as $prepaidOperator)
	                                                    <option value="{{ $prepaidOperator->name }}">{{ $prepaidOperator->name }}</option>
	                                                @endforeach
	                                            </select>
									        </div>
										</div>

										<div class="form-group {{ $errors->has('payment') ? 'has-error' : ''}}">
									        <label class="control-label col-xs-3 col-sm-3" for="payment">Payment:</label>
									        <div class="col-xs-9 col-sm-9">
								    	        <select class="form-control" id='payment' placeholder="Enter Payment" name="payment">
	                                                <option value="">Select Payment</option>
	                                                <option value="Debit Card">Debit Card</option>
	                                                <option value="Credit Card">Credit Card</option>
	                                                <option value="Wallet">Wallet</option>
	                                                <option value="Paytm">Paytm</option>
	                                                <option value="Mobikwiki">Mobikwiki</option>
	                                                <option value="Netbanking">Netbanking</option>
	                                            </select>
									        </div>
										</div>

										<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
									        <label class="control-label col-sm-3" for="">Description:</label>
									        <div class="col-xs-9 col-sm-9">
								        		<!-- <input type="text" class="form-control"  placeholder="Enter Description" name="description"> -->
								        		<textarea class="form-control" rows="3" name="description" placeholder="Enter Description"></textarea>
								    	        <span class="text-danger">
								    			    {{ $errors->first('description') }}
								    		    </span>
									        </div>
										</div>

										<div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
									        <label class="control-label col-sm-3" for="">Image:</label>
									        <div class="col-xs-9 col-sm-9">
								        		<input type="file" class="form-control"  placeholder="Enter Usage Limit" name="image" onchange="readURL(this);">
								    	        <span class="text-danger">
								    			    {{ $errors->first('image') }}
								    		    </span>
								    		    <img id="blah" src="#" alt="image" />
									        </div>
										</div>

										<div class="box-footer">
	                						<button type="button" class="btn btn-default">Cancel</button>
	                						<button type="submit" class="btn btn-info pull-right">Submit</button>
	                						
	              						</div>

									</form>

								</div>
							</div>
						</div>

                	</div>
          		</div>
        	</div>
      	</div>
    </section>

</div>

@endsection

@section('style')
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('script')
    

    <script type="text/javascript">
        $(document).ready(function(){
            $("#coupon_type").change(function(){
                var coupon_type = $("#coupon_type").val();
                //alert(coupon_type);
                if (coupon_type == 'Discount') {

                	$.get('{{ url("/coupon/discount-type")}}', function (data) {  
                    	$('#type_info_show').html(data);
                	});
                }

                if (coupon_type == 'Cash Back') {
                	$.get('{{ url("/coupon/cash-back-type")}}', function (data) {  
                    	$('#type_info_show').html(data);
                	});
                }

                if (coupon_type == 'Gift') {
                	$('#type_info_show').html(

                						"<div class='form-group'>"+
									        "<label class='control-label col-sm-3' >Gift Name:</label>"+
									        "<div class='col-xs-9 col-sm-9'>"+
								        		"<input type='number' class='form-control'  placeholder='Enter Gift Name' name='gift_name'>"+
									        "</div>"+
										"</div>"
                		);
                }

                if (coupon_type == '') {
                	$('#type_info_show').html('');
                }
                // var url = '{{ url("/type-to-assign")}}';
                // $.get(url+'?ticket_type_id='+ticketTypeId, function (data) {  
                //     $('#type_to_assign_show').html(data);
                // });

                
            });
        });


    </script>

    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/js/bootstrap-datetimepicker.min.js"></script>

    <script type="text/javascript">
        $(function () {
            $('#start_date').datetimepicker({
                format: "YYYY-MM-DD"
            });
        });
    </script> -->

    <script src="https://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

	 <script>
		$( function() {
			// $( "#datepicker" ).datepicker({ changeMonth: true, changeYear: true });
			// $( "#datepicker" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
			// $( "#datepicker1" ).datepicker({ changeMonth: true, changeYear: true });
			// $( "#datepicker1" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
			$( "#start_date" ).datepicker({ changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd", minDate: 0 });
        	$( "#start_date" ).datepicker( "setDate", "0" );
        	$( "#end_date" ).datepicker({ changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd", minDate: 0 });
        	$( "#end_date" ).datepicker( "setDate", "0" );
		} );
	 </script>

	 <script>

        function readURL(input) {
	        if (input.files && input.files[0]) {
	            var reader = new FileReader();
	            reader.onload = function (e) {
	                $('#blah')
	                        .attr('src', e.target.result)
	                        .width(100)
	                        .height(100);
	            };
	            reader.readAsDataURL(input.files[0]);
	        }
	    }
    </script>

@endsection