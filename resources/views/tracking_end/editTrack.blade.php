
  <div class="modal fade draggable-modal" id="edit_track_modal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Start edit Here</h4>
            </div>
            <div class="modal-body">
                <form action="{{ URL::to('admin/updateTrack') }}" method="post" id="update_track_form">
                    <div class="form-body">                       

                        <input name="id" id="id" value="{{$track->id}}" type="text" hidden /> 
                        <div class="form-group row">
                            <label class="control-label col-md-4">Tracking Number
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-8">
                                <input name="tracking_no" id="tracking_no" type="text" class="form-control" value="{{$track->tracking_no}}" /> 
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-4">Destination
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" name="destination" id="destination" data-required="1" class="form-control" value="{{$track->destination}}"  /> 
                            </div>
                        </div>

                        <h3 class="form-section">Sender Information</h3>

                        <div class="form-group row">
                            <label class="control-label col-md-4">Company Name
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" name="s_company_name" id="s_company_name" data-required="1" class="form-control" value="{{$track->s_company_name}}"/> 
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-4">Location/Address
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" name="s_location" id="s_location" data-required="1" class="form-control" value="{{$track->s_location}}"/> 
                            </div>
                        </div>
                        <h3 class="form-section">Recieve Information</h3>
                        <div class="form-group row">
                            <label class="control-label col-md-4">Name
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" name="r_name" id="r_name" data-required="1" class="form-control" value="{{$track->r_name}}"/> 
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-4">Address
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" name="r_adress" id="r_adress" data-required="1" class="form-control" value="{{$track->r_adress}}"/> 
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-4">Contact
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" name="r_contact" id="r_contact" data-required="1" class="form-control" value="{{$track->r_contact}}"/> 
                            </div>
                        </div>
                        <h3 class="form-section">shipment Information</h3>
                        <div class="form-group row">
                            <label class="control-label col-md-4">No. of itmes
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" name="sh_no" id="sh_no" data-required="1" class="form-control" value="{{$track->sh_no}}"/> 
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-4">COD Amount
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" name="sh_cod_amount" id="sh_cod_amount" data-required="1" class="form-control" value="{{$track->sh_cod_amount}}"/> 
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-4">Customs Value
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" name="sh_cus_value" id="sh_cus_value" data-required="1" class="form-control" value="{{$track->sh_cus_value}}"/> 
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-4">Location
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" name="sh_loc" id="sh_loc" data-required="1" class="form-control" value="{{$track->sh_loc}}"/> 
                            </div>
                        </div>
                          <div class="form-group row">
                        <label class="control-label col-md-4">Good Des
                            <span class="required"> * </span>
                        </label>
                        <div class="col-md-8">
                            <input type="text" name="good_des" id="good_des" data-required="1" class="form-control" value="{{$track->good_des}}"/> 
                        </div>
                        
                    </div>

                    <div class="form-group row">
                       <label class="control-label col-md-4">Weight
                        <span class="required"> * </span>
                    </label>
                    <div class="col-md-8">
                        <input type="text" name="weight" id="weight" data-required="1" class="form-control" value="{{$track->weight}}"/> 
                    </div>
                    

                </div>

                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Edit Track</button>
                                <button type="button" class="btn grey-salsa btn-outline" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script type="text/javascript">
    
    $('#update_track_form').on('submit', function(e) {
        e.preventDefault();
        var data = $(this).serialize();
        var url = $(this).attr('action');
        var post = $(this).attr('method');
        $.ajax({
            type : post,
            url : url,
            data: data,
            dataTy:'json',
            success:function(data){
                location.reload();
            }
        })
    });
</script>