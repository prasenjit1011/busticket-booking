<?php

namespace App\Classes;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\File;
use Carbon\Carbon;


class Common {
    public static function curl($method = 'GET', $url, $data = []) {
        $client = new Client();
        $option_key = 'query';
        if($method == 'POST') {
            $option_key = 'form_params';
        }
        try {
            $response = $client->request($method, $url, [
                $option_key => $data
            ]);
            $body = $response->getBody();
            return $body;
        } catch (RequestException $e) {
            Log::info(Psr7\str($e->getRequest()));
            if ($e->hasResponse()) {
                Log::info(Psr7\str($e->getResponse()));
            }
            return false;
        } catch (ClientException $e) {
            Log::info(Psr7\str($e->getRequest()));
            Log::info(Psr7\str($e->getResponse()));
            return false;
        }
        return false;
    }
}