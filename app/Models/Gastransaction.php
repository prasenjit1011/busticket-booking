<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Gastransaction extends Model
{
    protected $table = 'gastransactions';

    protected $fillable = [
      'order_id',
      'currency',
      'amount',
      'mn'
    ];
    public function users (  ) {
        return $this->belongsTo('App\User');
    }
}
