<div class=" header-right">
    <div class="banner">
        <div class="slider">
            <div class="callbacks_container">
                <ul class="rslides" id="slider">
                    <li>
                        <div class="banner-board-band">
                            <div class="caption">
                                <h3>Boardband Recharge</h3>
                                <p class="b-text-desc">For all his transactions, a cash back of 1.5% will be credited
                                    to his wallet</p>
                                <p><a href="#mobilew3layouts" class="btn btn-default org-btn">Start with us</a></p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="banner-bus-ticket">
                            <div class="caption">
                                <h3>Bus Ticket</h3>
                                <p class="b-text-desc">For all his transactions, a cash back of 1.5% will be credited
                                    to his wallet</p>
                                <p><a href="#mobilew3layouts" class="btn btn-default org-btn">Start with us</a></p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="banner-dth">
                            <div class="caption">
                                <h3>TV Recharge</h3>
                                <p class="b-text-desc">For all his transactions, a cash back of 1.5% will be credited
                                    to his wallet</p>
                                <p><a href="#mobilew3layouts" class="btn btn-default org-btn">Start with us</a></p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="banner-data-card">
                            <div class="caption">
                                <h3>Data Card Recharge</h3>
                                <p class="b-text-desc">For all his transactions, a cash back of 1.5% will be credited
                                    to his wallet</p>
                                <p><a href="#mobilew3layouts" class="btn btn-default org-btn">Start with us</a></p>
                            </div>
                        </div>
                    <li>
                        <div class="banner-elictric">
                            <div class="caption">
                                <h3>Electricity</h3>
                                <p class="b-text-desc">For all his transactions, a cash back of 1.5% will be credited
                                    to his wallet</p>
                                <p><a href="#mobilew3layouts" class="btn btn-default org-btn">Start with us</a></p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="banner-gas">
                            <div class="caption">
                                <h3>Gas Recharge</h3>
                                <p class="b-text-desc">For all his transactions, a cash back of 1.5% will be credited
                                    to his wallet</p>
                                <p><a href="#mobilew3layouts" class="btn btn-default org-btn">Start with us</a></p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="banner-landline">
                            <div class="caption">
                                <h3>Landline</h3>
                                <p class="b-text-desc">For all his transactions, a cash back of 1.5% will be credited
                                    to his wallet</p>
                                <p><a href="#mobilew3layouts" class="btn btn-default org-btn">Start with us</a></p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="banner-mobile">
                            <div class="caption">
                                <h3>Mobile Recharge</h3>
                                <p class="b-text-desc">For all his transactions, a cash back of 1.5% will be credited
                                    to his wallet</p>
                                <p><a href="#mobilew3layouts" class="btn btn-default org-btn">Start with us</a></p>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>