(function(){
    "use strict";

    angular.module('App', ['ng-currency', 'ngAnimate', 'ui.bootstrap', 'Auth', 'User', 'Plan', 'Stripe'])

    .config(function($interpolateProvider, $httpProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
    })

    .constant('BASE', {
        
        'API_URL': window.__env.API_URL,
        
        'ASSETS_URL': 'application/components/',

        'API_VERSION': 'api/v1/',
        
    })

    .service('BlockUI', BlockUI)

   .directive("selectPicker", Select2)

    function BlockUI() {
        this.open = function(element, message) {
            var message = message == undefined ? '<div class=" fa-spin flaticon-refresh"></div>' : message;
            $(element).block({
                message: message,
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        }
        
        this.close = function(element, time) {
            $(element).unblock();
        }
    }


    function Select2($parse, $timeout){
        return {
            restrict: 'A',
            priority: 1000,
            link: function (scope, element, attrs) {

              function refresh(newVal) {

                scope.$applyAsync(function () {

                  element.selectpicker();
                  
                  element.selectpicker('refresh'); 
                  
                });
              }

              attrs.$observe('spTheme', function (val) {
                $timeout(function () {
                  element.data('selectpicker').$button.removeClass(function (i, c) {
                    return (c.match(/(^|\s)?btn-\S+/g) || []).join(' ');
                  });
                  element.selectpicker('setStyle', val);
                });
              });

              $timeout(function () {
                element.selectpicker($parse(attrs.selectpicker)());
                element.selectpicker('refresh'); 
              });

              if (attrs.ngModel) {
                scope.$watch(attrs.ngModel, refresh, true);
              }

              if (attrs.ngDisabled) {
                scope.$watch(attrs.ngDisabled, refresh, true);
              }

              scope.$on('$destroy', function () {
                $timeout(function () {
                  element.selectpicker('destroy');
                });
              });

              if (attrs.obj) {
                scope.$watch(attrs.obj, refresh, true);
              }
              
            }
        };
    }
    
})();