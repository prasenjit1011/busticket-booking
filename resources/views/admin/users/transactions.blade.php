@extends('admin.layout.layout')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content-wrapper-inner">
                <div class="viewport-header">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="#">Users</a></li>
                            <li class="breadcrumb-item active" aria-current="page">{{ $user->name }} Transactions</li>
                        </ol>
                    </nav>
                </div>
                <div class="content-viewport">
                   
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="grid">
                                <div class="item-wrapper">
                                    <div class="tab-container">
                                        <ul class="nav nav-tabs" id="bt-tab_1" role="tablist">
                                            <li class="nav-item"><a class="nav-link active" id="bt-tab_1_1" data-toggle="tab" href="#bt-content_1_1" role="tab" aria-controls="bt-content_1_1" aria-selected="true">Wallet Transactions</a></li>
                                            <li class="nav-item"><a class="nav-link" id="bt-tab_1_2" data-toggle="tab" href="#bt-content_1_2" role="tab" aria-controls="bt-content_1_2" aria-selected="false">Dth Transactions</a></li>
                                            <li class="nav-item"><a class="nav-link" id="bt-tab_1_3" data-toggle="tab" href="#bt-content_1_3" role="tab" aria-controls="bt-content_1_3" aria-selected="false">Mobile Transactions</a></li>
                                            <li class="nav-item"><a class="nav-link" id="bt-tab_1_4" data-toggle="tab" href="#bt-content_1_4" role="tab" aria-controls="bt-content_1_3" aria-selected="false">Post Paid Transactions</a></li>
                                            <li class="nav-item"><a class="nav-link" id="bt-tab_1_5" data-toggle="tab" href="#bt-content_1_5" role="tab" aria-controls="bt-content_1_3" aria-selected="false">Cashback</a></li>
                                        </ul>
                                        <div class="tab-content" id="bt-tab_content_1">
                                            <div class="tab-pane show active" id="bt-content_1_1" role="tabpanel" aria-labelledby="bt-tab_1_1">
                                                <h6 class="mb-3">Wallet Transactions</h6>
                                                <div class="item-wrapper">
                                                        <div class="table-responsive">
                                                            <table  class="data-table table table-striped">
                                                                <thead>
                                                                <tr>
                                                                    <th>Name</th>
                                                                    <th>Order Id</th>
                                                                    <th>Mobile Number</th>
                                                                    <th>Operator</th>
                                                                    <th>Currency</th>
                                                                    <th>Amount</th>
                                                                    <th>Status</th>
                                                                    <th>Payment Mode</th>
                                                                    <th>Date</th>
                                                                    <th>Status Message</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                @foreach ($walletTransactions as $transaction)
                                                                    <tr>
                                                                        <th>
                                                                            @if (is_null($transaction->userDet))
                                                                                N/A
                                                                            @else
                                                                                {{ $transaction->userDet->name }}
                                                                            @endif
                                                                        </th>
                                                                        <td>{{ $transaction->order_id }}</td>
                                                                        <td>{{ $transaction->mn }}</td>
                                                                        <td>
                                                                            @if (is_null($transaction->operator))
                                                                                N/A
                                                                            @else
                                                                                {{ $transaction->op->name }}
                                                                            @endif
                                                                        </td>
                                                                        <td>{{ $transaction->currency }}</td>
                                                                        <td>{{  $transaction->amount }}</td>
                                                                        <td>{{ $transaction->status }}</td>
                                                                        <td>{{ $transaction->payment_mode }}</td>
                                                                        <td>{{ $transaction->created_at }}</td>
                                                                        <td>{{ $transaction->status_message }}</td>
                        
                        
                                                                    </tr>
                                                                @endforeach
                        
                        
                                                                </tbody>
                                                            </table>
                        
                                                        </div>
                                                    </div>
                                            </div>
                                            <div class="tab-pane show" id="bt-content_1_2" role="tabpanel" aria-labelledby="bt-tab_1_1">
                                                    <h6 class="mb-3">Dth Transactions</h6>
                                                    <div class="item-wrapper">
                                                            <div class="table-responsive">
                                                                <table  class="data-table table table-striped">
                                                                    <thead>
                                                                    <tr>
                                                                        <th>Name</th>
                                                                        <th>Order Id</th>
                                                                        <th>Mobile Number</th>
                                                                        <th>Operator</th>
                                                                        <th>Currency</th>
                                                                        <th>Amount</th>
                                                                        <th>Status</th>
                                                                        <th>Payment Mode</th>
                                                                        <th>Date</th>
                                                                        <th>Status Message</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    @foreach ($dthTransactions as $transaction)
                                                                        <tr>
                                                                            <th>
                                                                                @if (is_null($transaction->userDet))
                                                                                    N/A
                                                                                @else
                                                                                    {{ $transaction->userDet->name }}
                                                                                @endif
                                                                            </th>
                                                                            <td>{{ $transaction->order_id }}</td>
                                                                            <td>{{ $transaction->mn }}</td>
                                                                            <td>
                                                                                @if (is_null($transaction->operator))
                                                                                    N/A
                                                                                @else
                                                                                    {{ $transaction->op->name }}
                                                                                @endif
                                                                            </td>
                                                                            <td>{{ $transaction->currency }}</td>
                                                                            <td>{{  $transaction->amount }}</td>
                                                                            <td>{{ $transaction->status }}</td>
                                                                            <td>{{ $transaction->payment_mode }}</td>
                                                                            <td>{{ $transaction->created_at }}</td>
                                                                            <td>{{ $transaction->status_message }}</td>
                                                                        </tr>
                                                                    @endforeach
                            
                            
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                </div>
                                                <div class="tab-pane show" id="bt-content_1_3" role="tabpanel" aria-labelledby="bt-tab_1_1">
                                                        <h6 class="mb-3">Mobile Transactions</h6>
                                                        <div class="item-wrapper">
                                                                <div class="table-responsive">
                                                                    <table  class="data-table table table-striped">
                                                                        <thead>
                                                                        <tr>
                                                                            <th>Name</th>
                                                                            <th>Order Id</th>
                                                                            <th>Mobile Number</th>
                                                                            <th>Operator</th>
                                                                            <th>Currency</th>
                                                                            <th>Amount</th>
                                                                            <th>Status</th>
                                                                            <th>Payment Mode</th>
                                                                            <th>Date</th>
                                                                            <th>Status Message</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        @foreach ($mobileTransactions as $transaction)
                                                                            <tr>
                                                                                <th>
                                                                                    @if (is_null($transaction->userDet))
                                                                                        N/A
                                                                                    @else
                                                                                        {{ $transaction->userDet->name }}
                                                                                    @endif
                                                                                </th>
                                                                                <td>{{ $transaction->order_id }}</td>
                                                                                <td>{{ $transaction->mn }}</td>
                                                                                <td>
                                                                                    @if (is_null($transaction->operator))
                                                                                        N/A
                                                                                    @else
                                                                                        {{ $transaction->op->name }}
                                                                                    @endif
                                                                                </td>
                                                                                <td>{{ $transaction->currency }}</td>
                                                                                <td>{{  $transaction->amount }}</td>
                                                                                <td>{{ $transaction->status }}</td>
                                                                                <td>{{ $transaction->payment_mode }}</td>
                                                                                <td>{{ $transaction->created_at }}</td>
                                                                                <td>{{ $transaction->status_message }}</td>
                                
                                
                                                                            </tr>
                                                                        @endforeach
                                
                                
                                                                        </tbody>
                                                                    </table>
                                
                                                                </div>
                                                            </div>
                                                    </div>
                                                    <div class="tab-pane show" id="bt-content_1_4" role="tabpanel" aria-labelledby="bt-tab_1_1">
                                                            <h6 class="mb-3">Post Paid Transactions</h6>
                                                            <div class="item-wrapper">
                                                                    <div class="table-responsive">
                                                                        <table  class="data-table table table-striped">
                                                                            <thead>
                                                                            <tr>
                                                                                <th>Name</th>
                                                                                <th>Order Id</th>
                                                                                <th>Mobile Number</th>
                                                                                <th>Operator</th>
                                                                                <th>Currency</th>
                                                                                <th>Amount</th>
                                                                                <th>Status</th>
                                                                                <th>Payment Mode</th>
                                                                                <th>Date</th>
                                                                                <th>Status Message</th>
                                                                            </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                            @foreach ($postPaidTransactions as $transaction)
                                                                                <tr>
                                                                                    <th>
                                                                                        @if (is_null($transaction->userDet))
                                                                                            N/A
                                                                                        @else
                                                                                            {{ $transaction->userDet->name }}
                                                                                        @endif
                                                                                    </th>
                                                                                    <td>{{ $transaction->order_id }}</td>
                                                                                    <td>{{ $transaction->mn }}</td>
                                                                                    <td>
                                                                                        @if (is_null($transaction->operator))
                                                                                            N/A
                                                                                        @else
                                                                                            {{ $transaction->op->name }}
                                                                                        @endif
                                                                                    </td>
                                                                                    <td>{{ $transaction->currency }}</td>
                                                                                    <td>{{  $transaction->amount }}</td>
                                                                                    <td>{{ $transaction->status }}</td>
                                                                                    <td>{{ $transaction->payment_mode }}</td>
                                                                                    <td>{{ $transaction->created_at }}</td>
                                                                                    <td>{{ $transaction->status_message }}</td>
                                    
                                    
                                                                                </tr>
                                                                            @endforeach
                                    
                                    
                                                                            </tbody>
                                                                        </table>
                                    
                                                                    </div>
                                                                </div>
                                                        </div>
                                            <div class="tab-pane show" id="bt-content_1_5" role="tabpanel" aria-labelledby="bt-tab_1_1">
                                                <h6 class="mb-3">Cashback</h6>
                                                <div class="item-wrapper">
                                                    <div class="table-responsive">
                                                        <table  class="data-table table table-striped">
                                                            <thead>
                                                            <tr>
                                                                <th>Product Description</th>
                                                                <th>Amount</th>
                                                                <th>Coupon</th>
                                                                <th>Date</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @foreach ($cashbacks as $transaction)
                                                                <tr>
                                                                    <td>{{ $transaction->remark }}</td>
                                                                    <td>Rs {{ $transaction->amount }}</td>

                                                                    <td>0  Rs</td>

                                                                    <td>{{ $transaction->created_at }}</td>
                                                                </tr>
                                                            @endforeach


                                                            </tbody>
                                                        </table>

                                                    </div>
                                                </div>
                                            </div>
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>

       @endsection