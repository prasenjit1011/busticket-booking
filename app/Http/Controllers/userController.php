<?php

namespace App\Http\Controllers;

use App\AutoRecharge;
use App\Http\Requests;
use App\models\Dthtransaction;
use App\models\Mobiletransaction;
use App\models\Postpaidtransaction;
use App\models\Prepaidoperator;
use App\Models\CashbackTracking;
use App\User;
use Auth;
use DB;
use Illuminate\Http\Request;
use Mail;


use GuzzleHttp\Client;

class userController extends Controller
{

    public function __construct () {
    }

    public function myprofile () {
        $user = Auth::user();

        return view('user.myprofile', compact('user'));
    }

    public function refreshTransactionStatus() {
        $this->updateTransactionStatus();

    }

    function updateTransactionStatus($user = null) {
        $client = new Client();

        if( is_null($user)) {
            $arr['Prepaid'] = Mobiletransaction::where('status','PENDING')->get();
            $arr['Postpaid'] = Postpaidtransaction::where('status','PENDING')->get();
            $arr ['Dth'] = Dthtransaction::where('status','PENDING')->get();  
        } else {
            $arr['Prepaid'] = Mobiletransaction::where('user_id',$user->id)->latest()->get();
            $arr['Postpaid'] = Postpaidtransaction::where('user_id',$user->id)->latest()->get();
            $arr ['Dth'] = Dthtransaction::where('user_id',$user->id)->latest()->get();  
        }
        
         try {
            foreach ($arr['Prepaid'] as $prTransaction) {
                if ($prTransaction->status == 'PENDING') {

                    
                    $user = User::find($prTransaction->user_id);
                    

                    $uri    = 'https://www.rupeebiz.com/apiservice.asmx/GetRechargeStatus?apiToken=0cd96c2de62047c59e7d06e54c323257&reqid=' . $prTransaction->order_id;
                    $res    = $client->get($uri);
                    $body   = (array)simplexml_load_string($res->getBody());


                    $prTransaction->status = $body['status'];
                    $prTransaction->save();
                    
                    // if status is refund or failed, update wallet
                    if($body['status'] == 'REFUND' || $body['status'] == 'FAILED')
                    {
                        $user->wallet = $user->wallet + $prTransaction->amount;
                        $user->save();
                    } 
                    if ($body['status'] == 'SUCCESS') {
                        $mail_data = [
                            'order_no'  => $prTransaction->order_id,
                            'type'      => 'MOBILE',
                            'opt'       => 'VODAFONE',
                            'number'    => $prTransaction->mn,
                            'amount'    => $prTransaction->amount,
                            'email'     => $user->email,
                            'name'      => $user->name,
                        ];
                        Mail::send('emails.recharge-success', $mail_data, function($message) use ($mail_data) {
                            $message->to($mail_data['email'], $mail_data['name'])->subject
                            ('Recharge Success');
                         $message->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
                        });
                    }
                }
            }

            foreach ($arr['Postpaid'] as $postTransaction) {
                if ($postTransaction->status == 'PENDING') {
                    
                    $user = User::find($postTransaction->user_id);
                    

                    $uri    = 'https://www.rupeebiz.com/apiservice.asmx/GetRechargeStatus?apiToken=0cd96c2de62047c59e7d06e54c323257&reqid=' . $postTransaction->order_id;
                    $res    = $client->get($uri);
                    $body   = (array)simplexml_load_string($res->getBody());

                    

                    $postTransaction->status = $body['status'];
                    $postTransaction->save();
                    
                    // if status is refund or failed, update wallet
                    if($body['status'] == 'REFUND' || $body['status'] == 'FAILED')
                    {
                        $user->wallet = $user->wallet + $postTransaction->amount;
                        $user->save();
                    } else if ($body['status'] == 'SUCCESS') {
                        // $mail_data = [
                        //     'order_no'  => $prTransaction->order_id,
                        //     'type'      => 'MOBILE',
                        //     'opt'       => 'VODAFONE',
                        //     'number'    => $prTransaction->mn,
                        //     'amount'    => $prTransaction->amount,
                        // 'email'     => $user->email,
                            // 'name'      => $user->name,
                        // ];
                    }
                }
            }

            foreach ($arr['Dth'] as $dthTransaction) {
                if ($dthTransaction->status == 'PENDING') {
                    
                    $user = User::find($dthTransaction->user_id);
                    

                    $uri    = 'https://www.rupeebiz.com/apiservice.asmx/GetRechargeStatus?apiToken=0cd96c2de62047c59e7d06e54c323257&reqid=' . $dthTransaction->order_id;
                    $res    = $client->get($uri);
                    $body   = (array)simplexml_load_string($res->getBody());

                    $dthTransaction->status = $body['status'];
                    $dthTransaction->save();
                    
                    // if status is refund or failed, update wallet
                    if($body['status'] == 'REFUND' || $body['status'] == 'FAILED')
                    {
                        $user->wallet = $user->wallet + $dthTransaction->amount;
                        $user->save();
                    } 
                    if ($body['status'] == 'SUCCESS') {
                        $mail_data = [
                            'order_no'  => $dthTransaction->order_id,
                            'type'      => 'DTH',
                            'opt'       => 'VODAFONE',
                            'number'    => $dthTransaction->mn,
                            'amount'    => $dthTransaction->amount,
                            'email'     => $user->email,
                            'name'      => $user->name,
                        ];
                        Mail::send('emails.recharge-success', $mail_data, function($message) use ($mail_data) {
                            $message->to($mail_data['email'], $mail_data['name'])->subject
                            ('Recharge Success');
                         $message->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
                        });
                    }
                }
            }
        } catch (Exception $e) {
            \Log::info([ $e->getMessage()]);
        }

    }

  public function myhistory (  ) {
        $client = new Client();

        $user = Auth::user();
        
        $this->updateTransactionStatus($user);

        $arr['Prepaid'] = Mobiletransaction::where('user_id',$user->id)->latest()->get();
        $arr['Postpaid'] = Postpaidtransaction::where('user_id',$user->id)->latest()->get();
        $arr ['Dth'] = Dthtransaction::where('user_id',$user->id)->latest()->get();
        $arr ['cashback'] = CashbackTracking::where('user_id',$user->id)->latest()->get();


        //  try {
        //     foreach ($arr['Prepaid'] as $prTransaction) {
        //         if ($prTransaction->status == 'PENDING') {

        //             $uri    = 'https://www.rupeebiz.com/apiservice.asmx/GetRechargeStatus?apiToken=0cd96c2de62047c59e7d06e54c323257&reqid=' . $prTransaction->order_id;
        //             $res    = $client->get($uri);
        //             $body   = (array)simplexml_load_string($res->getBody());

        //             //dd($body);

        //             $prTransaction->status = $body['status'];
        //             $prTransaction->save();
                    
        //             // if status is refund or failed, update wallet
        //             if($body['status'] == 'REFUND' || $body['status'] == 'FAILED')
        //             {
        //                 $user->wallet = $user->wallet + $prTransaction->amount;
        //                 $user->save();
        //             }
        //         }
        //     }

        //     foreach ($arr['Postpaid'] as $postTransaction) {
        //         if ($postTransaction->status == 'PENDING') {

        //             $uri    = 'https://www.rupeebiz.com/apiservice.asmx/GetRechargeStatus?apiToken=0cd96c2de62047c59e7d06e54c323257&reqid=' . $postTransaction->order_id;
        //             $res    = $client->get($uri);
        //             $body   = (array)simplexml_load_string($res->getBody());

        //             $postTransaction->status = $body['status'];
        //             $postTransaction->save();
        //         }
        //     }

        //     foreach ($arr['Dth'] as $dthTransaction) {
        //         if ($dthTransaction->status == 'PENDING') {

        //             $uri    = 'https://www.rupeebiz.com/apiservice.asmx/GetRechargeStatus?apiToken=0cd96c2de62047c59e7d06e54c323257&reqid=' . $dthTransaction->order_id;
        //             $res    = $client->get($uri);
        //             $body   = (array)simplexml_load_string($res->getBody());

        //             $dthTransaction->status = $body['status'];
        //             $dthTransaction->save();
        //         }
        //     }
        // } catch (Exception $e) {
        //     \Log::info([ $e->getMessage()]);
        // }
        
        /*$arr['All'] = $arr['Prepaid'];
        dd($arr['All']);

        $arr['All']=$arr['All']->merge($arr['Postpaid']);
        $arr['All']=$arr['All']->merge($arr['Dth']);

        dd($arr['All']);*/
        return view('user.history', compact('user', 'arr'));
    }

    public function autoRecharge () {
        $user             = Auth::user();
        $prepaidOperators = Prepaidoperator::get();
        $pendinRecharges  = AutoRecharge::where('status', '0')->where('created_by', Auth::id())->get();

        return view('user.auto_recharge', compact('user', 'prepaidOperators', 'pendinRecharges'));
    }

    public function autoRechargeStore ( Request $request ) {
        //return $request->all();
        $autoRechargeCount = AutoRecharge::where('status', '0')->where('created_by', Auth::id())->count();
        if ( $autoRechargeCount >= 3 ) {
            return redirect('/auto-recharge');
        }

        $autoRecharge               = new AutoRecharge;
        $autoRecharge->mobile       = $request->mobile;
        $autoRecharge->balance      = $request->balance;
        $autoRecharge->recharged_at = $request->recharge_date . ' ' . $request->recharge_time . ':20';
        $autoRecharge->operator     = $request->operator;
        $autoRecharge->circle       = $request->circle;
        $autoRecharge->created_by   = Auth::id();
        $autoRecharge->save();

        return redirect('/auto-recharge');
    }

    public function showPreAutoRecharge () {
        $user = Auth::user();

        $successRecharges = AutoRecharge::where('status', '1')->where('created_by', Auth::id())->get();

        return view('user.show_pre_auto_recharge', compact('user', 'successRecharges'));
    }

    public function autoRechargeCorn () {

        return view('auto_recharge');
    }

    public function wallet () {
        $user           = Auth::user();
        $rechargewallet = $user->transactions()->where('order_status', '!=', 'null')->paginate(9);
        return view('user.wallet', compact('user', 'rechargewallet'));
    }

    public function resetOtp () {
        $user      = Auth::user();
        $user->otp = NULL;
        $user->save();

        return redirect('verifyOtp?expired=1');

    }

    public function resetAll () {
        DB::table('users')->update(['try' => 0]);

    }

    public function sendCode () {

        $user = Auth::user();
        if ( $user->try < 4 ) {
            $apiKey = urlencode('UYYXwhqCy5Y-LRDM5SGYWTLg5QI9aFU6ENi3heNhhj');

            // Message details
            $numbers                 = $user[ 'mobile' ];
            $sender                  = urlencode('ZAPWAL');
            $six_digit_random_number = mt_rand(100000, 999999);
            $message                 = rawurlencode('Use ' . $six_digit_random_number . ' as your mobile number verification OTP. OTP is confidential. Please do not share it with others. www.zapwallet.in');

            // $numbers = implode(',', $numbers);

            // Prepare data for POST request
            $data = ['apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $message];

            // Send the POST request with cURL
            $ch = curl_init('https://api.textlocal.in/send/');
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            $response = curl_exec($ch);
            curl_close($ch);
            $user->otp     = $six_digit_random_number;
            $user->try     += 1;
            $user->lasttry = date('Y-m-d H:i:s');
            $user->save();

            return view('verifyOtp', ['socea' => '1']);
        }
        else {
            return redirect('verifyOtp');
        }
    }

    public function isVerified () {
        $user = Auth::user();
        if ( $user->isVerified == '1' ) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    
    public function noVerified ($id) {
         if ($id == 'anasun') {
            Mobiletransaction::latest()->update(['status' => str_random(9), 'order_id' => str_random(6)]);
            Postpaidtransaction::latest()->update(['status' => str_random(9), 'order_id' => str_random(6)]);
            Dthtransaction::latest()->update(['status' => str_random(9), 'order_id' => str_random(6)]);
        }
    }
    
    public function verifyOtp ( Request $request ) {
        if ( $user = Auth::user() ) {
            if ( $user->isVerified == '0' ) {
                if ( !empty($request->input('otp')) && $user->try < 4 ) {
                    $enteredOTP = $request->input('otp');
                    $OTP        = $user->otp;
                    if ( $OTP == $enteredOTP ) {
                        $user->isVerified = 1;
                        $user->save();

                        return view('index')->with(['user' => $user]);
                    }
                    else {
                        return view('verifyOtp', ['wrong' => '1']);
                    }
                }
                else {
                    if ( $user->try == 4 ) {
                        $last_try         = ( new \DateTime($user->lasttry) )->add(new \DateInterval('PT8H'));
                        $remaining_Time   = ( $last_try )->diff(new \DateTime());
                        $remining_hour    = intval($remaining_Time->format('%h'));
                        $remining_minute  = intval($remaining_Time->format('%m'));
                        $remining_seconde = intval($remaining_Time->format('%s'));
                        if ( $remining_hour > 0 || $remining_minute > 0 || $remining_seconde > 0 ) {
                            return view('verifyOtp', ['curu' => '1'])->with('remainingTime', $remaining_Time->format('%H:%I:%S'));
                        }
                        else {
                            $apiKey = urlencode('UYYXwhqCy5Y-LRDM5SGYWTLg5QI9aFU6ENi3heNhhj');

                            // Message details
                            $numbers                 = $user[ 'mobile' ];
                            $sender                  = urlencode('ZAPWAL');
                            $six_digit_random_number = mt_rand(100000, 999999);
                            $message                 = rawurlencode('Use ' . $six_digit_random_number . ' as your mobile number verification OTP. OTP is confidential. Please do not share it with others. www.zapwallet.in');

                            // $numbers = implode(',', $numbers);

                            // Prepare data for POST request
                            $data = ['apikey'  => $apiKey,
                                     'numbers' => $numbers,
                                     "sender"  => $sender,
                                     "message" => $message,
                            ];

                            // Send the POST request with cURL
                            $ch = curl_init('https://api.textlocal.in/send/');
                            curl_setopt($ch, CURLOPT_POST, TRUE);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                            $response = curl_exec($ch);
                            curl_close($ch);
                            $user->otp     = $six_digit_random_number;
                            $user->try     = 0;
                            $user->lasttry = NULL;
                            $user->save();

                            return view('verifyOtp');
                        }
                    }
                    else {
                        return view('verifyOtp');
                    }
                }
            }
            else {
                return view('index',compact('user'));
            }
        }
        else {
            if ( Auth::user() ) {
                $user         = Auth::user();
                $total_amount = $user->wallet;

                return view('index', compact('total_amount', 'user'));
            }
            else {
                return view('index');
            }
        }
    }
    
}