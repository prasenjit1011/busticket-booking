<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class CashbackTracking extends Model
{
    protected $table = 'cashback_trackings';

    protected $fillable = [
        'admin_id',
        'user_id',
        'amount',
        'remark',
    ];
}
