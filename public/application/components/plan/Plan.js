(function(){
    "use strict";

    angular.module('Plan', ['ui.bootstrap', 'ngAnimate'])

    .factory('PlanRepository', PlanRepository)

    .factory('ItemRepository', ItemRepository)

    .service('ItemService', ItemService)

    .service('PlanService', PlanService)

    .directive('planManagement', PlanManagement)

    function PlanManagement(BASE, BlockUI) {
        return {
            restrict: 'EA',
            controller: PlanManagementController,
            templateUrl: BASE.API_URL + BASE.ASSETS_URL + 'plan/templates/index.html',
            link: function (scope, iElement, iAttrs) {

            }
        };
    }

    function PlanManagementController(BASE, $scope, $timeout, BlockUI, PlanRepository, $uibModal, ItemService, PlanService) {
        
        $scope.isLoading = true;

        $timeout(function(){
            BlockUI.open('#stripe-integration', '<div class=" fa-spin flaticon-refresh"></div> Stripe loading...');
        });

        $scope.pagination = {
            totalItems: 0,
            currentPage: 1,
            itemsPerPage: 10,
            maxSize: 5,
            search: '',
            objects: [],
        }

        $scope.setPage = function (pageNo) {
            $scope.pagination.currentPage = pageNo;
        };

        $scope.pageChanged = function() {
            $scope.data();
        };


        $scope.data = function(){

            blockUi.open('.m-wrapper', {
                overlayColor: '#000000',
                type: 'loader',
                message: 'Loading...'
            });

            let params = {
                page: $scope.pagination.currentPage,
                rows: $scope.pagination.itemsPerPage,
                search: $scope.pagination.search,
            };
            
            DomainRepository.get(params).then(function(response){

                let res  = response.data.success;
                $scope.pagination.objects = res.data.data;
                $scope.pagination.totalItems = res.data.total;
                $scope.isLoaded = true;
                blockUi.close('.m-wrapper');

            });
            
        }

        $scope.setItemsPerPage = function(num) {
            $scope.pagination.itemsPerPage = num;
            $scope.pagination.currentPage = 1; //reset to first paghe
            $scope.data();
        }

        $scope.loadPlans = function(){

            StripeConfigRepository.get().then(function(response){

                let response_data = response.data.success.data;

                BlockUI.close('#stripe-integration');
                $scope.has_keys = true;
                $scope.isLoading = false;
                $scope.stripe = response_data;

            }, function(){

                BlockUI.close('#stripe-integration');
                $scope.has_keys = false;
                $scope.isLoading = false;
            });
        }

        $scope.showListOfPlanItems = function(){
            ItemService.showItems();
        }

        $scope.createNewPlanForm = function(){

            PlanService.openNewPlanForm($scope.loadPlans);

        }


    }

    function PlanRepository(BASE, $http) {
        
        let url = BASE.API_URL + BASE.API_VERSION + 'admin/plan';

        let repo = {};

        repo.get = function() {
            return $http.get(url);
        }

        repo.store = function(params) {
            return $http.post(url, params);
        }

        return repo;

    }

    function ItemRepository(BASE, $http) {
        
        let url = BASE.API_URL + BASE.API_VERSION + 'admin/plans/items';

        let repo = {};

        repo.get = function(params) {
            return $http.get(url, {params});
        }

        repo.getById = function(id) {
            return $http.get(url + '/' + id + '/edit');
        }

        repo.store = function(params) {
            return $http.post(url, params);
        }

        repo.patch = function(params, id) {
            return $http.patch(url + '/' + id, params);
        }

        repo.destroy = function(id){
            return $http.delete(url + '/' + id);
        }

        return repo;

    }


    function PlanService(BASE, $uibModal) {
        
        this.openNewPlanForm = function(callback = false){

            $uibModal.open({
                animation: true,
                templateUrl: BASE.API_URL + BASE.ASSETS_URL +'plan/templates/modals/add-new-plan.html',
                backdrop: 'static',
                controller: function($uibModalInstance, $scope, BlockUI, $timeout, BASE, ItemRepository){
                  
                    $scope.isLoaded = false;
                    $scope.items = [];
                    $scope.plan = {
                        item_id: '',
                        name: '',
                        slug: '',
                        amount: '',
                        interval: 'month',
                    }

                    $timeout(function(){
                        BlockUI.open('.add-new-plan-modal', '<div class=" fa-spin flaticon-refresh"></div> Loading...');
                        $scope.loadItems();
                    });

                    $scope.loadItems = function(){

                        ItemRepository.get({}).then(function(response){

                            let res  = response.data.success;
                            $scope.items = res.data.data;

                            BlockUI.close('.add-new-plan-modal');
                            $scope.isLoaded = true;              

                        });
                    }

                    $scope.addPlan = function(){

                        $scope.isLoaded = false;
                        BlockUI.open('.add-new-plan-modal', '<div class=" fa-spin flaticon-refresh"></div> Loading...');

                        ItemRepository.store($scope.item).then(function(response){

                            let response_data = response.data.success;

                            swal({
                                position:"center",
                                type:"success",
                                title: response_data.message,
                                showConfirmButton:!1,
                                timer:5000
                            });

                            $scope.cancel();
                            BlockUI.close('.add-new-plan-modal');
                            $scope.isLoaded = true;

                            if (callback) {
                                callback();
                            }                            

                        }, function(response){

                            let response_error = response.data;

                            let html = '<div class="m-demo"><div class="m-demo__preview"><div class="m-list-timeline"><div class="m-list-timeline__items">';

                            if (response_error.errors) {

                                $.each(response_error.errors, function(index, val) {
                                     '<li>' + val[0] + '</li>';

                                     html += '<div class="m-list-timeline"><div class="m-list-timeline__items"><div class="m-list-timeline__item"><span class="m-list-timeline__icon flaticon-cancel"></span><span class="m-list-timeline__text">';
                                     html += val[0];
                                     html += '</span></div></div></div>';
                                });

                            }

                            if (response_error.error) {

                                 '<li>' + response_error.error.message + '</li>';

                                 html += '<div class="m-list-timeline"><div class="m-list-timeline__items"><div class="m-list-timeline__item"><span class="m-list-timeline__icon flaticon-cancel"></span><span class="m-list-timeline__text">';
                                 html += response_error.error.message;
                                 html += '</span></div></div></div>';
                    

                            }

                            html += '</div></div></div></div>';

                            swal({
                                position:"center",
                                type:"warning",
                                title: response_error.message,
                                html: html,
                                showConfirmButton:!1,
                                timer:5000
                            });

                            BlockUI.close('.add-new-plan-modal');
                            $scope.isLoaded = true;

                        });

                    }

                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('close');
                    }
                },
                
                size: 'lg'

            });
        }

    }

    function ItemService(BASE, $uibModal) {
        
        this.showItems = function(){
            $uibModal.open({
                animation: true,
                templateUrl: BASE.API_URL + BASE.ASSETS_URL +'plan/templates/modals/items.html',
                backdrop: 'static',
                controller: function($uibModalInstance, $scope, BlockUI, $timeout, BASE, ItemRepository, ItemService){
                  
                    $scope.isLoaded = false;

                    $timeout(function(){
                        BlockUI.open('.list-of-items-modal', '<div class=" fa-spin flaticon-refresh"></div> Loading...');
                        $scope.getListOfItems();
                    });


                    $scope.pagination.items = {
                        totalItems: 0,
                        currentPage: 1,
                        itemsPerPage: 10,
                        maxSize: 5,
                        search: '',
                        objects: [],
                    }

                    $scope.setPage = function (pageNo) {
                        $scope.pagination.items.currentPage = pageNo;
                    };

                    $scope.pageChanged = function() {
                        $scope.getListOfItems();
                    };

                    $scope.setItemsPerPage = function(num) {
                        $scope.pagination.items.itemsPerPage = num;
                        $scope.pagination.items.currentPage = 1; //reset to first paghe
                        $scope.getListOfItems();
                    }

                    $scope.getListOfItems = function(){

                        $scope.isLoaded = false;
                        BlockUI.open('.list-of-items-modal', '<div class=" fa-spin flaticon-refresh"></div> Loading...');

                        let params = {
                            page: $scope.pagination.items.currentPage,
                            rows: $scope.pagination.items.itemsPerPage,
                            search: $scope.pagination.items.search,
                        };

                        ItemRepository.get(params).then(function(response){

                            let res  = response.data.success;
                            $scope.pagination.items.objects = res.data.data;
                            $scope.pagination.items.totalItems = res.data.total;

                            BlockUI.close('.list-of-items-modal');
                            $scope.isLoaded = true;

                        });

                    }

                    $scope.editItem = function(item_id){
                        ItemService.editItem(item_id, $scope.getListOfItems);
                    }

                    $scope.addNewItem = function(){
                        ItemService.addNewItem($scope.getListOfItems);
                    }

                    $scope.deleteItem = function(item_id){

                        $scope.isLoaded = false;
                        BlockUI.open('.list-of-items-modal', '<div class=" fa-spin flaticon-refresh"></div> Loading...');

                        Swal.fire({
                          title: 'Are you sure?',
                          text: "Once deleted, you will not be able to recover this item for creating new plan!",
                          type: 'warning',
                          showCancelButton: true,
                          confirmButtonColor: '#3085d6',
                          cancelButtonColor: '#d33',
                          confirmButtonText: 'Yes, delete it!'
                        }).then((result) => {
                          if (result.value) {

                            ItemRepository.destroy(item_id).then(function(response){

                                swal("Deleted", "Item has been deleted!", "success");

                                $scope.isLoaded = true;
                                BlockUI.close('.list-of-items-modal');

                                $scope.getListOfItems();

                            }, function(response_error){

                                swal("Failed", "Failed to delete the item. " + response_error.data.error.message, 'error');

                                $scope.isLoaded = true;
                                BlockUI.close('.list-of-items-modal');

                            });

                          }else{
                            swal("Cancelled!"," Your Item is safe :)",'error');
                            $scope.isLoaded = true;
                            BlockUI.close('.list-of-items-modal');
                          }
                        })
                    }

                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('close');
                    }
                },
                
                size: 'lg'

            });
        }


        this.addNewItem = function(callback = false){
            $uibModal.open({
                animation: true,
                templateUrl: BASE.API_URL + BASE.ASSETS_URL +'plan/templates/modals/add-new-item.html',
                backdrop: 'static',
                controller: function($uibModalInstance, $scope, BlockUI, $timeout, BASE, ItemRepository){
                  
                    $scope.isLoaded = false;

                    $scope.item = {
                        name: '',
                    }

                    $timeout(function(){
                        BlockUI.open('.add-new-item-modal', '<div class=" fa-spin flaticon-refresh"></div> Loading...');
                        $timeout(function(){ BlockUI.close('.add-new-item-modal'); $scope.isLoaded = true; });
                    });

                    $scope.addItem = function(){

                        $scope.isLoaded = false;
                        BlockUI.open('.add-new-item-modal', '<div class=" fa-spin flaticon-refresh"></div> Loading...');

                        ItemRepository.store($scope.item).then(function(response){

                            let response_data = response.data.success;

                            swal({
                                position:"center",
                                type:"success",
                                title: response_data.message,
                                showConfirmButton:!1,
                                timer:5000
                            });

                            $scope.cancel();
                            BlockUI.close('.add-new-item-modal');
                            $scope.isLoaded = true;

                            if (callback) {
                                callback();
                            }                            

                        }, function(response){

                            let response_error = response.data;

                            let html = '<div class="m-demo"><div class="m-demo__preview"><div class="m-list-timeline"><div class="m-list-timeline__items">';

                            if (response_error.errors) {

                                $.each(response_error.errors, function(index, val) {
                                     '<li>' + val[0] + '</li>';

                                     html += '<div class="m-list-timeline"><div class="m-list-timeline__items"><div class="m-list-timeline__item"><span class="m-list-timeline__icon flaticon-cancel"></span><span class="m-list-timeline__text">';
                                     html += val[0];
                                     html += '</span></div></div></div>';
                                });

                            }

                            if (response_error.error) {

                                 '<li>' + response_error.error.message + '</li>';

                                 html += '<div class="m-list-timeline"><div class="m-list-timeline__items"><div class="m-list-timeline__item"><span class="m-list-timeline__icon flaticon-cancel"></span><span class="m-list-timeline__text">';
                                 html += response_error.error.message;
                                 html += '</span></div></div></div>';
                    

                            }

                            html += '</div></div></div></div>';

                            swal({
                                position:"center",
                                type:"warning",
                                title: response_error.message,
                                html: html,
                                showConfirmButton:!1,
                                timer:5000
                            });

                            BlockUI.close('.add-new-item-modal');
                            $scope.isLoaded = true;

                        });

                    }

                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('close');
                    }
                },
                
                size: 'md'

            });
        }


        this.editItem = function(item_id, callback = false){
            $uibModal.open({
                animation: true,
                templateUrl: BASE.API_URL + BASE.ASSETS_URL +'plan/templates/modals/edit-item.html',
                backdrop: 'static',
                controller: function($uibModalInstance, $scope, BlockUI, $timeout, BASE, ItemRepository){
                  
                    $scope.isLoaded = false;

                    $scope.item = {
                        name: '',
                    }

                    $timeout(function(){
                        BlockUI.open('.add-new-item-modal', '<div class=" fa-spin flaticon-refresh"></div> Loading...');
                        $scope.loadItem();
                    });

                    $scope.loadItem = function(){
                        ItemRepository.getById(item_id).then(function(response){
                            let response_data = response.data.success;
                            $scope.item = response_data.data;
                             BlockUI.close('.add-new-item-modal'); $scope.isLoaded = true;
                        });
                    }

                    $scope.updateItem = function(){

                        $scope.isLoaded = false;
                        BlockUI.open('.add-new-item-modal', '<div class=" fa-spin flaticon-refresh"></div> Loading...');

                        ItemRepository.patch($scope.item, item_id).then(function(response){

                            let response_data = response.data.success;

                            swal({
                                position:"center",
                                type:"success",
                                title: response_data.message,
                                showConfirmButton:!1,
                                timer:5000
                            });

                            $scope.cancel();
                            BlockUI.close('.add-new-item-modal');
                            $scope.isLoaded = true;

                            if (callback) {
                                callback();
                            }                            

                        }, function(response){

                            let response_error = response.data;

                            let html = '<div class="m-demo"><div class="m-demo__preview"><div class="m-list-timeline"><div class="m-list-timeline__items">';

                            if (response_error.errors) {

                                $.each(response_error.errors, function(index, val) {
                                     '<li>' + val[0] + '</li>';

                                     html += '<div class="m-list-timeline"><div class="m-list-timeline__items"><div class="m-list-timeline__item"><span class="m-list-timeline__icon flaticon-cancel"></span><span class="m-list-timeline__text">';
                                     html += val[0];
                                     html += '</span></div></div></div>';
                                });

                            }

                            if (response_error.error) {

                                 '<li>' + response_error.error.message + '</li>';

                                 html += '<div class="m-list-timeline"><div class="m-list-timeline__items"><div class="m-list-timeline__item"><span class="m-list-timeline__icon flaticon-cancel"></span><span class="m-list-timeline__text">';
                                 html += response_error.error.message;
                                 html += '</span></div></div></div>';
                    

                            }

                            html += '</div></div></div></div>';

                            swal({
                                position:"center",
                                type:"warning",
                                title: response_error.message,
                                html: html,
                                showConfirmButton:!1,
                                timer:5000
                            });

                            BlockUI.close('.add-new-item-modal');
                            $scope.isLoaded = true;

                        });

                    }

                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('close');
                    }
                },
                
                size: 'md'

            });
        }
    }
})();