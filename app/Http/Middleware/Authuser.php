<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Authuser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                if (Auth::check() && Auth::user()->user_type == 1)
                {
                    return redirect()->route('admin.dashboard');
                }
                return redirect()->guest('/');
            }
        }
        
        if (Auth::check() && Auth::user()->user_type == 1)
                {
                    return redirect()->route('admin.dashboard');
                }

        return $next($request);
    }
}
