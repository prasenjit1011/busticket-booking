@extends('blades.base')

{{-- @section('page title', 'Index') --}}

@section('css')
<link rel="stylesheet" href="{{ asset('css/custom.css')}}">
@endsection

@section('innerbanner')

<!-- innerbanner -->    

<div class="banner">
	<s-banner></s-banner>
</div>

<!-- //innerbanner -->

@endsection

@section('breadcrumbs')
@endsection

@section('horizontal tab')

@endsection

@section('vertical tab')

<!-- breadcrumbs -->
	<div class="w3layouts-breadcrumbs text-center">
		<div class="container">
			<span class="agile-breadcrumbs"><a href="/"><i class="fa fa-home home_1"></i></a> / <span>Sitemap</span></span>
		</div>
	</div>
	<!-- //breadcrumbs -->

	<!-- Sitemap-page -->
	    <div class="icons w3layouts-content">
		<div class="container">
			<h3 class="w3-head">Sitemap</h3>
	   <!-- sitemap -->
			<div class="sitemap">
					<div class="col-md-6 sitemap-grid">
						<h4>Online mobile Rechange and Bill payment</h4>
						<ul>
							<li><a href="/#parentVerticalTab1.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Airtel</a></li>
							<li><a href="/#parentVerticalTab1.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Aircel</a></li>
							<li><a href="/#parentVerticalTab1.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Tata Docomo</a></li>
							<li><a href="/#parentVerticalTab1.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>BSNL</a></li>
							<li><a href="/#parentVerticalTab1.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Tata Docomo</a></li>
							<li><a href="/#parentVerticalTab1.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Reliance GSM</a></li>
							<li><a href="/#parentVerticalTab1.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Reliance CDMA</a></li>
							<li><a href="/#parentVerticalTab1.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Telenor</a></li>
							<li><a href="/#parentVerticalTab1.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>MTS</a></li>
							<li><a href="/#parentVerticalTab1.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Jio</a></li>
						</ul>
						<h4>Online DataCard Recharge</h4>
						<ul>
							<li><a href="career.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Tata Photon</a></li>
							<li><a href="/#parentVerticalTab3.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>MTS MBrowse</a></li>
							<li><a href="/#parentVerticalTab3.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Airtel</a></li>
							<li><a href="/#parentVerticalTab3.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Aircel</a></li>
							<li><a href="/#parentVerticalTab3.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>BSNL</a></li>
							<li><a href="/#parentVerticalTab3.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>MTNL Delhi</a></li>
							<li><a href="/#parentVerticalTab3.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Vodafone</a></li>
							<li><a href="/#parentVerticalTab3.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Idea</a></li>
							<li><a href="/#parentVerticalTab3.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>MTNL Mumbai</a></li>
							<li><a href="/#parentVerticalTab3.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Tata Photon Whiz</a></li>
						</ul>                        
		                <h4>Online DTH(TV) Recharge</h4>
						<ul>
							<li><a href="/#parentVerticalTab2.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Airtel Digital TV Recharges</a></li>
							<li><a href="/#parentVerticalTab2.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Dish TV Recharges</a></li>
				            <li><a href="/#parentVerticalTab2.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Tata Sky Recharges</a></li>
                            <li><a href="/#parentVerticalTab2.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Reliance Digital TV Recharges</a></li>
                            <li><a href="/#parentVerticalTab2.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Sun Direct Recharges</a></li>
                            <li><a href="/#parentVerticalTab2.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Videocon D2H Recharges</a></li>
						</ul>
						<h4>Electricity</h4>
						<ul>
							<li><a href="/#parentVerticalTab4.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Electricity-1</a></li>
							<li><a href="/#parentVerticalTab4.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Electricity-2</a></li>
							<li><a href="/#parentVerticalTab4.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Electricity-3</a></li>
							<li><a href="/#parentVerticalTab4.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Electricity-4</a></li>
							<li><a href="/#parentVerticalTab4.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Electricity-5</a></li>
							<li><a href="/#parentVerticalTab4.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Electricity-6</a></li>
						</ul>
					</div>
					<div class="col-md-6 sitemap-grid">
						<h4>Landline Bill Payments</h4>
						<ul>
							<li><a href="/#parentVerticalTab5.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Act</a></li>
							<li><a href="/#parentVerticalTab5.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Airtel</a></li>	<li><a href="/#parentVerticalTab5.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>BSNL</a></li>
							<li><a href="/#parentVerticalTab5.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Reliance</a></li>

						</ul>
						<h4>Broadband Bill Payments</h4>
						<ul>
							<li><a href="/#parentVerticalTab6.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Act</a></li>
							<li><a href="/#parentVerticalTab6.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Airtel</a></li>	<li><a href="/#parentVerticalTab6.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>BSNL</a></li>
						</ul>
						<h4>Gas</h4>
						<ul>
							<li><a href="/#parentVerticalTab7.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Gas-1</a></li>
							<li><a href="/#parentVerticalTab7.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Gas-2</a></li>
							<li><a href="/#parentVerticalTab7.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Gas-3</a></li>
							<li><a href="/#parentVerticalTab7.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Gas-4</a></li>
							<li><a href="/#parentVerticalTab7.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Gas-5</a></li>
						</ul>
						<h4>Metro</h4>
						<ul>
							<li><a href="/#parentVerticalTab9.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Metro</a></li>
						</ul>
						<h4>Pages</h4>
						<ul>
							<li><a href="about.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>About</a></li>
							<li><a href="support.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Support</a></li>
							<li><a href="terms.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Terms & Conditions</a></li>
							<li><a href="{{ route('frontend.faq') }}"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Faq</a></li>
							<li><a href="feedback.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Feedback</a></li>
							<li><a href="contact.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Contact</a></li>
							<li><a href="shortcodes.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Shortcodes</a></li>
							<li><a href="icons.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Icons Page</a></li>
                        </ul>
					</div>
					<div class="clearfix"></div>
			</div>
			<!-- /sitemap -->
		</div>
	</div>
    <!--//Sitemap-page-->


<!-- subscribe -->
@endsection

@section('tab title')
    Plan
@endsection