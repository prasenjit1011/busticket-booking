import moment from 'moment'

export default {
    methods: {
        getTimeFrom(datetime) {
            if(moment(datetime).isValid()) {
                return moment(datetime).format('hh:mm')
            }
            return ""
        },

        getDateFrom(datetime) {
            if(moment(datetime).isValid()) {
                return moment(datetime).format("Do MMM YYYY")
            }
            return ""
        },

        showError(title = 'Error!',message = 'Oops! Something went wrong. Please try again...') {
            this.$modal.show('dialog', {
            title: title,
            text: '<p class="text-danger">' + message + '</p>',
            buttons: [
                {
                    title: 'Close',
                },
            ]
            })
        }
    }
}