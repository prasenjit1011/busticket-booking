(function(){
    "use strict";

    angular.module('Auth', [])

    .factory('UserRepository', UserRepository)

    .directive('register', Register)

    .directive('requestActivation', RequestActivation)

    .directive('activateAccount', ActivateAccount)

    function RequestActivation(BASE, BlockUI) {
        return {
            restrict: 'EA',
            controller: RequestActivationController,
            templateUrl: BASE.API_URL + BASE.ASSETS_URL + 'auth/templates/customer/request-activation.html',
            link: function (scope, iElement, iAttrs) {

            }
        };
    }

    function RequestActivationController(BASE, $scope, $timeout, BlockUI, UserRepository) {

        $scope.isLoading = true;
        BlockUI.open('.m-stack.m-stack--hor.m-stack--desktop', '<div class=" fa-spin flaticon-refresh"></div> loading...');

        $timeout(function(){
            $scope.isLoading = false;
            BlockUI.close('.m-stack.m-stack--hor.m-stack--desktop');
        },1000);

        $scope.email = null;

        $scope.requestActivation = function(){

            $scope.isLoading = true;
            BlockUI.open('.m-stack.m-stack--hor.m-stack--desktop', '<div class=" fa-spin flaticon-refresh"></div> loading...');

            UserRepository.requestActivation($scope.email).then(function(response){
                let response_data = response.data;

                $scope.isLoading = false;
                BlockUI.close('.m-stack.m-stack--hor.m-stack--desktop');

                // Reset the registration form
                $scope.resetForm();

                swal({
                    position:"center",
                    type:"success",
                    title:'New Activation Code has been sent',
                    html: response_data.success.message,
                    showConfirmButton:!1,
                    timer:5000
                });

                $timeout(function(){
                    document.location = '/login';
                },5500)

            }, function(response){
                
                let response_error = response.data;

                let html = '<div class="m-demo"><div class="m-demo__preview"><div class="m-list-timeline"><div class="m-list-timeline__items">';

                if (response_error.errors) {

                    $.each(response_error.errors, function(index, val) {
                         '<li>' + val[0] + '</li>';

                         html += '<div class="m-list-timeline"><div class="m-list-timeline__items"><div class="m-list-timeline__item"><span class="m-list-timeline__icon flaticon-cancel"></span><span class="m-list-timeline__text">';
                         html += val[0];
                         html += '</span></div></div></div>';
                    });

                }

                if (response_error.error) {

                     '<li>' + response_error.error.message + '</li>';

                     html += '<div class="m-list-timeline"><div class="m-list-timeline__items"><div class="m-list-timeline__item"><span class="m-list-timeline__icon flaticon-cancel"></span><span class="m-list-timeline__text">';
                     html += response_error.error.message;
                     html += '</span></div></div></div>';
        

                }

                html += '</div></div></div></div>';

                swal({
                    position:"center",
                    type:"warning",
                    title: response_error.message,
                    html: html,
                    showConfirmButton:!1,
                    timer:5000
                });

                $scope.isLoading = false;
                BlockUI.close('.m-stack.m-stack--hor.m-stack--desktop');
            })
        }

        $scope.resetForm = function(){
            $scope.registration_form = {
                email: '',
            }
        }
    }

    function Register(BASE, BlockUI) {
        return {
            restrict: 'EA',
            controller: RegisterController,
            templateUrl: BASE.API_URL + BASE.ASSETS_URL + 'auth/templates/customer/register.html',
            link: function (scope, iElement, iAttrs) {

            }
        };
    }


    function RegisterController(BASE, $scope, $timeout, BlockUI, UserRepository) {

        let _token = $('_token').val();
        $scope.isLoading = true;
        BlockUI.open('.m-stack.m-stack--hor.m-stack--desktop', '<div class=" fa-spin flaticon-refresh"></div> loading...');

        $timeout(function(){
            $scope.isLoading = false;
            BlockUI.close('.m-stack.m-stack--hor.m-stack--desktop');
        },1000);

        $scope.registration_form = {
            fullname: '',
            email: '',
            passowrd: '',
            password_confirmation: '',
            terms: '',
            _token: _token,
        }

        $scope.register = function(){

            $scope.isLoading = true;
            BlockUI.open('.m-stack.m-stack--hor.m-stack--desktop', '<div class=" fa-spin flaticon-refresh"></div> loading...');

            UserRepository.create($scope.registration_form).then(function(response){
                let response_data = response.data;

                $scope.isLoading = false;
                BlockUI.close('.m-stack.m-stack--hor.m-stack--desktop');

                // Reset the registration form
                $scope.resetForm();

                swal({
                    position:"center",
                    type:"success",
                    title:'Registration Successful',
                    html: response_data.success.message,
                    showConfirmButton:!1,
                    timer:5000
                });

                $timeout(function(){
                    document.location = '/login';
                },5500)

            }, function(response){
                
                let response_error = response.data;

                let html = '<div class="m-demo"><div class="m-demo__preview"><div class="m-list-timeline"><div class="m-list-timeline__items">';

                $.each(response_error.errors, function(index, val) {
                     '<li>' + val[0] + '</li>';

                     html += '<div class="m-list-timeline"><div class="m-list-timeline__items"><div class="m-list-timeline__item"><span class="m-list-timeline__icon flaticon-cancel"></span><span class="m-list-timeline__text">';
                     html += val[0];
                     html += '</span></div></div></div>';
                });

                html += '</div></div></div></div>';

                swal({
                    position:"center",
                    type:"warning",
                    title: response_error.message,
                    html: html,
                    showConfirmButton:!1,
                    timer:5000
                });

                $scope.isLoading = false;
                BlockUI.close('.m-stack.m-stack--hor.m-stack--desktop');
            })
        }

        $scope.resetForm = function(){
            $scope.registration_form = {
                fullname: '',
                email: '',
                passowrd: '',
                password_confirmation: '',
                terms: '',
                _token: _token,
            }
        }
    }


    function ActivateAccount(BASE, BlockUI) {
        return {
            restrict: 'EA',
            scope:{
                code: '@'
            },
            controller: ActivateAccountController,
            templateUrl: BASE.API_URL + BASE.ASSETS_URL + 'auth/templates/customer/account-activation.html',
            link: function (scope, iElement, iAttrs) {

            }
        };
    }

    function ActivateAccountController(BASE, $scope, $timeout, BlockUI, UserRepository) {

        $scope.isLoading = true;
        BlockUI.open('.m-stack.m-stack--hor.m-stack--desktop', '<div class=" fa-spin flaticon-refresh"></div> loading...');
        $scope.activation_title = 'Activating Your Account';
        $scope.retriable = false;

        $timeout(function(){
            $scope.isLoading = false;
            BlockUI.close('.m-stack.m-stack--hor.m-stack--desktop');
            $scope.activateAccount();
        },1000);


        $scope.activateAccount = function(){

            $scope.isLoading = true;
            BlockUI.open('.m-login__form.m-form', '<div class=" fa-spin flaticon-refresh"></div> loading...');

            UserRepository.activateAccount($scope.code).then(function(response){
                let response_data = response.data;

                $scope.isLoading = false;
                BlockUI.close('.m-login__form.m-form');

                $scope.activation_title = 'Account Activated';
                $scope.retriable = false;

                swal({
                    position:"center",
                    type:"success",
                    title:'New Activation Code has been sent',
                    html: response_data.success.message,
                    showConfirmButton:!1,
                    timer:5000
                });

                $timeout(function(){
                    document.location = '/login';
                },5500)

            }, function(response){
                
                let response_error = response.data;

                let html = '<div class="m-demo"><div class="m-demo__preview"><div class="m-list-timeline"><div class="m-list-timeline__items">';

                if (response_error.errors) {

                    $.each(response_error.errors, function(index, val) {
                         '<li>' + val[0] + '</li>';

                         html += '<div class="m-list-timeline"><div class="m-list-timeline__items"><div class="m-list-timeline__item"><span class="m-list-timeline__icon flaticon-cancel"></span><span class="m-list-timeline__text">';
                         html += val[0];
                         html += '</span></div></div></div>';
                    });

                }

                if (response_error.error) {

                     '<li>' + response_error.error.message + '</li>';

                     html += '<div class="m-list-timeline"><div class="m-list-timeline__items"><div class="m-list-timeline__item"><span class="m-list-timeline__icon flaticon-cancel"></span><span class="m-list-timeline__text">';
                     html += response_error.error.message;
                     html += '</span></div></div></div>';
        

                }

                html += '</div></div></div></div>';

                swal({
                    position:"center",
                    type:"warning",
                    title: response_error.message,
                    html: html,
                    showConfirmButton:!1,
                    timer:5000
                });

                $scope.activation_title = 'Account Failed to Activate';
                $scope.retriable = true;

                $scope.isLoading = false;
                BlockUI.close('.m-login__form.m-form');
            })
        }

    }


    function UserRepository($http, BASE) {

        let url = BASE.API_URL + 'user';
        let repo = {};

        repo.all = function(params){
            return $http.get(url, {params});
        }

        repo.findById = function(id, params){
            return $http.get(url + '/' + id, {params});
        }

        repo.create = function(params){
            return $http.post(url, params);
        }

        repo.update = function(id, params){
            return $http.patch(url + '/' + id, params);
        }

        repo.destroy = function(id){
            return $http.delete(url + '/' + id);
        }

        repo.activateAccount = function(code){
            return $http.get(url + '/activate/' + code);   
        }

        let activation_url = BASE.API_URL + 'activation';

        repo.requestActivation = function(email){
            return $http.get(activation_url + '/request-activation/' + email);   
        }

        return repo;
    }

})();