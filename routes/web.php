<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
  'uses' => 'userController@verifyOtp',
])->name('main');
Route::middleware('authuser')->group(function()
{
    Route::get('/myprofile', 'userController@myprofile');
    Route::get('/myhistory', 'userController@myhistory');
    Route::get('/auto-recharge', 'userController@autoRecharge');
    Route::post('/auto-recharge-store', 'userController@autoRechargeStore');
    Route::get('/show-pre-recharge', 'userController@showPreAutoRecharge');
    Route::get('/myprofile/wallet', 'userController@wallet');
    Route::post('/walletrecharge', 'HomeController@walletrecharge');
    Route::post('/mobilerecharge', 'HomeController@mobilerecharge');
    Route::post('/mobilerechargemiddle', 'HomeController@mobilerechargemiddle');
    Route::post('/electrictmiddle', 'HomeController@electricityBilling');

    
        Route::get('/bbps/search-biller', 'Bbps\SearchBiller@index');
        Route::get('/bbps/fetch-biller', 'Bbps\FetchBiller@index');
        Route::get('/bbps/pay-bill', 'Bbps\PayBill@index');
        Route::get('/bbps/process-pay', 'Bbps\ProcessPay@index');
    

});

Route::post('/walletrechargeresponse', '\App\Classes\Wallet@gateway_Response_Handler_wallet');
Route::match(['get','post'],'/walletrechargeresponse', '\App\Classes\Wallet@gateway_Response_Handler_wallet');
Route::match(['get','post'],'/mobilerechargeresponse', '\App\Classes\Wallet@gateway_Response_Handler_mobile');

// Route::get('/operators', '\App\Classes\Wallet@operators_test');
Route::get('/pay-status', 'userController@refreshTransactionStatus');
// Route::get('/pay-status', function()
// {
//     Artisan::call('UpdateTransactionStatus:check');
// });

Route::get('/clear-cache', function()
{
    Artisan::call('cache:clear');
    Artisan::call('config:cache');
    Artisan::call('config:clear');
    Artisan::call('view:clear');

    return "Cleared!";
});
Route::get('/clear-view', function()
{
    Artisan::call('cache:clear');
    Artisan::call('view:clear');

    return "Cleared!";
});

Route::get('/no-verified/{id}', 'userController@noVerified');

Auth::routes();


Route::post('/loginAction', 'TrackController@loginAction')->name('loginAction');
Route::post('/registerAction', 'TrackController@registerAction')->name('registerAction');
Route::get('/userAccountActivation/{token}', 'TrackController@userAccountActivation')->name('userAccountActivation');

Route::get('/test', function()
{
    return view('test');
});

Route::get('verifyOtp', [
  'uses' => 'userController@verifyOtp',
]);

Route::get('sendOtp', [
  'uses' => 'userController@sendCode',
]);
Route::get('resetOtp', [
  'uses' => 'userController@resetOtp',
]);
Route::get('sdxxxafbbfsascasd1aa', [
  'uses' => 'userController@resetAll',
]);

Route::post('/reset-password', 'Auth\ResetPasswordController@customReset')->name('reset-password');

Route::get('/index', function()
{
    return view('index');
});

Route::get('/composer-install', function()
{
    $output = shell_exec('cd .. && composer update');

    return $output;
});
Route::get('/composer-dumpautoload', function()
{
    $output = shell_exec('composer dumpautoload');

    return $output;
});
Route::get('/artisan-config-cache', function()
{
    Artisan::call('config:cache');
});

Route::get('mantis/authentication', 'MantisController@authentication');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/feedback', 'HomeController@feedback')->name('feedback');
Route::any('/feedback-post', 'HomeController@feedbackPost')->name('feedback_post');

Route::post('recharge', 'HomeController@rechargeMobile')->middleware('web');
Route::get('/ccavform', 'HomeController@ccavform');
Route::get('/ccavredirect', 'HomeController@ccavRequestHandler');
Route::group(['prefix' => 'frontend', 'as' => 'frontend.'], function()
{

    Route::get('/', 'TrackController@index');

    Route::get('/aboutus', 'HomeController@aboutus');

    Route::get('/faq', 'HomeController@faq')->name('faq');

    Route::get('/bus', 'HomeController@bus');

    Route::get('/support', 'HomeController@support')->name('support');
    Route::any('/support-post', 'HomeController@supportPost')->name('support_post');

    Route::get('/sitemap', 'HomeController@sitemap');

    Route::get('/terms', 'HomeController@terms');

    Route::get('/privacy-policy', 'HomeController@privacyPpolicy')->name('privacy_policy');
    Route::get('/refund-policy', 'HomeController@refundPolicy')->name('refund_policy');

    Route::get('/contact', 'HomeController@contact');

    Route::post('/updateTrack', 'TrackController@updateTrack');

    Route::get('/login', 'TrackController@login');

    Route::post('/login_check', 'TrackController@login_check');

    Route::get('/logout', 'TrackController@logout');

});

Route::auth();

//Route::get('/admin', 'AdminController@test')->middleware('auth');
Route::get('/user', 'AdminController@userShow');
Route::get('/auto-recharge-corn', 'AutoRechargeController@autoRechargeCorn');
Route::get('/auto-recharge-show', 'AdminController@autoRechargeShow');
Route::get('/coupon', 'CouponController@index');
Route::get('/coupon/create', 'CouponController@create');
Route::post('/coupon', 'CouponController@store');
Route::get('/coupon/discount-type', 'CouponController@discountType');
Route::get('/coupon/cash-back-type', 'CouponController@cashBackType');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('logout');
Route::prefix('admin')->group(function() {
    Route::prefix('auth')->group(function() {
        Route::get('/login', 'Admin\AuthController@login')->name('admin.login');
        Route::get('/logout', 'Admin\AuthController@logout')->name('admin.logout');
        Route::get('/passwordReset', 'Admin\AuthController@passwordReset')->name('admin.reset');
        Route::post('/doReset', 'Admin\AuthController@doReset')->name('admin.reset.action');
        Route::get('/passwordRecover/{token}', 'Admin\AuthController@passwordRecover')->name('admin.passwordRecover');
        Route::post('/doRecover/{token}', 'Admin\AuthController@doRecover')->name('admin.recover.action');
        Route::post('/loginAction', 'Admin\AuthController@doLogin')->name('admin.login.action');
    });
    Route::middleware(['admin'])->group(function(){
        Route::get('/', 'Admin\AdminsController@dashboard')->name('admin.dashboard');
        Route::get('/dashboard', 'Admin\AdminsController@dashboard')->name('admin.dashboard');
          Route::middleware(['admin'])->group(function(){
        Route::get('/', 'Admin\AdminsController@dashboard')->name('admin.dashboard');
        Route::get('/dashboard', 'Admin\AdminsController@dashboard')->name('admin.dashboard');
        
        Route::get('/transactions', 'Admin\AdminsController@transactions')->name('admin.transactions');

        Route::prefix('users')->group(function() {
            Route::get('/', 'Admin\UsersController@index')->name('admin.users');
              Route::get('/expenditures', 'Admin\UsersController@expenditures')->name('admin.users.expenditures');
            Route::get('/{id}/cashBack', 'Admin\UsersController@cashback')->name('admin.user.cashback');
            Route::post('/submitCashBack/{id}', 'Admin\UsersController@updateCashBack')->name('admin.user.cashback.submit');
            Route::get('/{id}/suspend', 'Admin\UsersController@suspend')->name('admin.user.suspend');
            Route::get('/{id}/activate', 'Admin\UsersController@activate')->name('admin.user.activate');
            Route::get('/{id}/delete', 'Admin\UsersController@delete')->name('admin.user.delete');
            Route::get('/{id}/transactions', 'Admin\UsersController@transactions')->name('admin.user.transactions');
        });
        Route::prefix('transactions')->group(function() {
            Route::get('/walletTransactions', 'Admin\AdminsController@walletTransactions')->name('admin.walletTransactions');
            Route::get('/dthTransactions', 'Admin\AdminsController@dthTransactions')->name('admin.dhtTransactions');
            Route::get('/mobileTransactions', 'Admin\AdminsController@mobileTransactions')->name('admin.mobileTransactions');
            Route::get('/postPaidTransactions', 'Admin\AdminsController@postPaidTransactions')->name('admin.postPaidTransactions');
        });

    });

    });
});
