@extends('blades.base')

{{--@section('page title', 'Zapwallet -  Mobile Recharge, DTH Recharge & Bus Tickets.')--}}

@section('innerbanner')
<!-- innerbanner -->	
{{-- <div class="agileits-inner-banner">
    
</div> --}}
<!-- //innerbanner -->
@endsection

@section('breadcrumbs')
@endsection

@section('horizontal tab')

@endsection
@section('css')
<link rel="stylesheet" href="css/jquery-ui.css">
<link rel="stylesheet" href="{{ asset('css/travel.css')}}">
<link rel="stylesheet" href="{{ asset('css/custom.css')}}">
<link rel="stylesheet" href="{{ asset('css/tool-tip.css')}}">
@endsection

@section('vertical tab')
    @if (session('status'))
        <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>            {{ session('status') }}
        </div>
    @endif
  
    @include('admin.partials.info')
<!--Vertical Tab-->
<div class="categories-section main-grid-border" id="mobilew3layouts">
    <div class="container">
        <div class="category-list">
            <div id="parentVerticalTab">
                <div class="agileits-tab_nav">
                <ul class="resp-tabs-list hor_1">
                    <li><i class="icon fa fa-mobile" aria-hidden="true"></i>Mobile</li>
                    <li><i class="icon fa fa-television" aria-hidden="true"></i>DTH</li> {{-- TV --}}
                    {{--<li><i class="icon fa fa-credit-card" aria-hidden="true"></i>Data Card</li> --}}
                    <li><i class="icon fa fa-lightbulb-o" aria-hidden="true"></i>Electricity</li>
                    {{--<li><i class="icon fa fa-phone" aria-hidden="true"></i>Land Line</li>--}}
                    {{--<li><i class="icon fa fa-connectdevelop" aria-hidden="true"></i>Broad Band</li>--}}
                    <li><i class="icon fa fa-flask" aria-hidden="true"></i>Gas</li>
                    <li><i class="icon fa fa-tint" aria-hidden="true"></i>Water</li>
                    <li><i class="icon fa fa-subway" aria-hidden="true"></i>Bus  Tickets</li>
                    <li><i class="fa fa-address-card" aria-hidden="true"></i>Wallet</li>
                </ul>
                </div>
                <div class="resp-tabs-container hor_1">
                    @include('services.mobiles')
                    @include('services.dth')
                    @include('services.elictric')
                    {{--@include('services.datacard')--}}
                    @include('services.gas')
                    {{--@include('services.landline')--}}
                    {{--@include('services.broadband')--}}
                    @include('services.water')
                    @include('services.bus')
                    @include('services.recharge')
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
</div>
	<!--Plug-in Initialisation-->
<bus-modal></bus-modal>
@endsection

@section('tab title')
    Plan
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {

        //Vertical Tab
        $('#parentVerticalTab').easyResponsiveTabs({
            type: 'vertical', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true, // 100% fit in a container
            closed: 'accordion', // Start closed if in accordion view
            tabidentify: 'hor_1', // The tab groups identifier
            activate: function(event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#nested-tabInfo2');
                var $name = $('span', $info);
                $name.text($tab.text());
                $info.show();
            }
        });
    });
</script>
<script>
    $(document).ready(function() {
        $("#tab2").hide();
        $("#tab3").hide();
        $("#tab4").hide();
        $(".tabs-menu a").click(function(event){
            event.preventDefault();
            var tab=$(this).attr("href");
            $(".tab-grid").not(tab).css("display","none");
            $(tab).fadeIn("slow");
        });
    });
</script>
<script src="/js/jquery-ui.js"></script>
<script>
    $(function() {
        $( "#datepicker,#datepicker1" ).datepicker();
    });
</script>
@endsection

@section('header-right')
<div class=" header-right">
    <div class="banner">
        <s-banner></s-banner>
    </div>
</div>
@endsection