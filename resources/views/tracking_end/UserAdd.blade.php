@extends('layouts.system')
@section('content')
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{url('/admin/trackadd')}}">Manage Users/Add New User</a>
            <i class="fa fa-circle"></i>
        </li>

    </ul>

</div>
<h1 class="page-title"> Add New User Page
    <small>You can add any user for track here.</small>
</h1>
<div class="row">
  <div class="col-md-12">
    <!-- BEGIN VALIDATION STATES-->
    <div class="portlet light portlet-fit portlet-form bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-settings font-red"></i>
                <span class="caption-subject font-red sbold uppercase">Add New User</span>
            </div>

        </div>
        <div class="portlet-body">
            <!-- BEGIN FORM-->
            <form action="" id="form_sample_2" class="form-horizontal">
                <div class="form-body">
                    <div class="alert alert-danger display-hide">
                        <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                        <div class="alert alert-success display-hide">
                            <button class="close" data-close="alert"></button> Your form validation is successful! </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Name
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-4">
                                    <input type="text" name="n_name" id="n_name" data-required="1" class="form-control" /> 
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Email
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-4">
                                    <input type="text" name="email" id="n_email" data-required="1" class="form-control" /> 
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Address
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-4">
                                    <input type="text" name="n_address" id="n_address" data-required="1" class="form-control" /> 
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Password
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-4">
                                    <input type="password" name="n_password" id="n_password" data-required="1" class="form-control" /> 
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Retype Password
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-4">
                                    <input type="password" name="n_password_confirm" id="n_password_confirm" data-required="1" class="form-control" /> 
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn green">Add User</button>
                                    <button type="button" class="btn grey-salsa btn-outline" onclick="form_empty()">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var ajax_adduser_url = '{{URL::to("admin/user_add")}}';
    function form_empty(){
     $("#n_name").val('');
     $("#n_email").val('');
     $("#n_address").val('');
     $("#n_password").val('');
     $("#n_password_confirm").val('');
 }

</script>


@endsection