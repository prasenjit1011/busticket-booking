import Vue from 'vue'
import Vuex from 'vuex'
import createLogger from 'vuex/dist/logger'

import modules from './modules'

Vue.use(Vuex)
const debug = process.env.NODE_ENV !== 'production'
const namespaced = true
export default new Vuex.Store({
  namespaced,
  modules,
  plugins: debug ? [createLogger()] : [],
  strict: debug
})
