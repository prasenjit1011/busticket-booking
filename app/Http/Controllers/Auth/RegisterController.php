<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Notifications\WelcomeUser;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Validator;

//require 'textlocal.class.php';

class RegisterController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct () {
        $this->middleware('guest');
    }

    public function postRegister ( Request $request ) {
        $validator = $this->register->validator($request->all());

        if ( $validator->fails() ) {
            $this->throwValidationException($request, $validator);
        }

        $this->auth->login($this->register->create($request->all()));

//        return view('verifyOtp',['register'=>'1']);
        return redirect($this->redirectPath());
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator ( array $data ) {
        return Validator::make($data, [
          'name'     => 'required|string|max:255|min:3||regex:/^[a-zA-Z]+$/u',
          'email'    => 'required|string|email|max:255|unique:users',
          'password' => 'required|string|min:6|confirmed',
          'mobile'   => 'required|numeric|digits:10|unique:users',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     *
     * @return \App\User
     */
    protected function create ( array $data ) {
        $six_digit_random_number = rand(100000, 999999);
        //dd($data);
        $user = User::create([
          'name'          => $data[ 'name' ],
          'email'         => $data[ 'email' ],
          'salutation'    => $data[ 'salutation' ],
          'mobile'        => $data[ 'mobile' ],
          'otp'           => $six_digit_random_number,
          'city'          => $data[ 'city' ],
          'zip_code'      => $data[ 'zip_code' ],
          'country'       => $data[ 'country' ],
          'address'       => $data[ 'address' ],
          'date_of_birth' => $data[ 'dateofbirth' ],
          'state'         => $data[ 'state' ],
          'isVerified'    => 1,
          'password'      => bcrypt($data[ 'password' ]),
        ]);

        /*$textlocal=new textlocal('bujjimabubbly@gmail.com','e215398a8820abd2c7a11a6cd5b1009d');
        $textlocal->sendSms('917788990011','Your car - KA01 HG 9999 - is due for service on July 24th, please text SERVICE to 92205 92205 for a callback','FORDIN');*/

        $apiKey = urlencode('UYYXwhqCy5Y-LRDM5SGYWTLg5QI9aFU6ENi3heNhhj');

        // Message details
        $numbers = $data[ 'mobile' ];
        $sender  = urlencode('ZAPWAL');
        $message = rawurlencode('Use ' . $six_digit_random_number . ' as your mobile number verification OTP. OTP is confidential. Please do not share it with others. www.zapwallet.in');

        // $numbers = implode(',', $numbers);

        // Prepare data for POST request
        $data = ['apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $message];

        // Send the POST request with cURL
        $ch = curl_init('https://api.textlocal.in/send/');
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $response = curl_exec($ch);
        curl_close($ch);

        // Process your response here
        //echo $response;

        $user->notify(new WelcomeUser());

        return $user;
    }
}
