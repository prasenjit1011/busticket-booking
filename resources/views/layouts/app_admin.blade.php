<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin Panel</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('assets/css/Admin.min.css') }}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ asset('assets/css/_all-skins.min.css') }}">

  <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
     @yield('style')
</head>
<body class="hold-transition skin-purple sidebar-mini">
<!-- <body class="hold-transition skin-green sidebar-mini"> -->
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>Zap</b>Wallet</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Admin</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
         <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
              <span class="label label-success">4</span>
            </a>
            

          </li> 
          <!-- Notifications: style can be found in dropdown.less -->
         <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">10</span>
            </a>
            


          </li>
          <!-- Tasks: style can be found in dropdown.less -->
         <li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-flag-o"></i>
              <span class="label label-danger">9</span>
            </a>
            


          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="{{ asset('assets/image/download.png') }}" class="user-image" alt="User Image">
              <!-- <img src="{{ asset('assets/image/78373.png') }}" class="user-image" alt="User Image"> -->
              <!-- <img src="{{ asset('assets/image/testimonial-Ortius.png') }}" class="user-image" alt="User Image"> -->
              <span class="hidden-xs">Administrative</span>
            </a>
            <ul class="dropdown-menu">

              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <!-- <a href="#" class="btn btn-default btn-flat">Profile</a> -->
                </div>
                <div class="pull-right">
                  <!-- <a href="http://zapwallet.in/logout" class="btn btn-danger btn-flat">Sign out</a> -->
                  
                  <a class="btn btn-danger btn-flat" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                  
                </div>
              </li>
            </ul>


          </li>
          <!-- Control Sidebar Toggle Button -->
          <!-- <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li> -->
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <!-- <div class="user-panel">
        <div class="pull-left image">
          <img src="../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Alexander Pierce</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div> -->
      <!-- search form -->
      <!-- <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form> -->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree" style="font-size: 20px;">
        <li class="header"></li>
        <!--<li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="..//"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
            <li><a href="../index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
          </ul>
        </li> -->
        <li class="treeview active">
          <a href="#">
            <i class="fa fa-users"></i>
            <span>&nbsp;User</span>
            <span class="pull-right-container">
              <!-- <span class="label label-primary pull-right">6</span> -->
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="{{ url('/user') }}" style="font-size: 16px;" ><i class="fa fa-circle-o"></i> Show All Users</a></li>
            <!-- <li class="active"><a href="{{ url('/user/create') }}" ><i class="fa fa-circle-o text-red"></i> Create User</a></li> -->
            
          </ul>
        </li>
        <!-- <li class="active">
          <a href="{{ url('/phone/create') }}">
            <i class="fa fa-th"></i> <span>Create Phone</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-green">new</small>
            </span>
          </a>
        </li> -->
        <li class="treeview active">
          <a href="#">
            <i class="fa fa-phone"></i>
            <span>&nbsp;Auto Recharge</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="{{ url('/auto-recharge-show') }}" style="font-size: 16px;"><i class="fa fa-circle-o text-red"></i> Show Recharge History</a></li>
            <!-- <li class="active"><a href="{{ url('/phone/create') }}" style="font-size: 16px;"><i class="fa fa-circle-o"></i> Create Phone</a></li> -->
            
          </ul>
        </li>


        <li class="treeview active">
          <a href="#">
            <i class="fa fa-list-ol"></i>
            <span>&nbsp;Coupon</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="{{ url('/coupon') }}" style="font-size: 16px;"><i class="fa fa-circle-o"></i> Show All Coupon</a></li>
            <li class="active"><a href="{{ url('/coupon/create') }}" style="font-size: 16px;"><i class="fa fa-circle-o text-red"></i> Create Coupon</a></li>
          </ul>
        </li>
        
        <!-- <li class="header">LABELS</li>
        <li><a href="{{ url('/lead/create') }}"><i class="fa fa-circle-o text-red"></i> <span>Lead</span></a></li> -->

        <li class="treeview active">
          <a href="#">
            <i class="fa fa-refresh"></i>
            <span>&nbsp;Lead</span>
            <span class="pull-right-container">
              <!-- <span class="label label-primary pull-right">7</span> -->
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="{{ url('/lead/create') }}" style="font-size: 16px;"><i class="fa fa-circle-o"></i> Lead Uploas</a></li>
            <li class="active"><a href="{{ url('/lead/edit') }}" style="font-size: 16px;"><i class="fa fa-circle-o text-red"></i> Lead Reset</a></li>
            <li class="active"><a href="{{ url('/lead/dialable') }}" style="font-size: 16px;"><i class="fa fa-circle-o"></i> Lead Dailable</a></li>
          </ul>
        </li>

        <li class="treeview active">
          <a href="#">
            <i class="fa fa-newspaper-o"></i>
            <span>&nbsp;Campaign</span>
            <span class="pull-right-container">
              <!-- <span class="label label-primary pull-right">7</span> -->
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="{{ url('/campaign') }}" style="font-size: 16px;"><i class="fa fa-circle-o"></i> Show All Campaigns</a></li>
            <!-- <li class="active"><a href="{{ url('/lead/edit') }}" style="font-size: 16px;"><i class="fa fa-circle-o text-red"></i> Lead Reset</a></li>
            <li class="active"><a href="{{ url('/lead/dialable') }}" style="font-size: 16px;"><i class="fa fa-circle-o"></i> Lead Dailable</a></li> -->
          </ul>
        </li>

        <!-- <li class="treeview active">
          <a href="#">
            <i class="fa fa-list-ol"></i>
            <span>&nbsp;List</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="{{ url('/list?list_id=1002') }}" style="font-size: 16px;"><i class="fa fa-circle-o"></i> Show All List</a></li>
          </ul>
        </li> -->
        
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

 

@yield('content')
  

  <footer class="main-footer" style="background: #222d32; font-family: 'Open Sans', serif;" >
    <div class="pull-right hidden-xs">
      <a href="http://zapwallet.in" target="_blank"><span style="color: #FFFFFF;">Website:</span> <b><span style="color: red;">ZapWallet</span></b></a>
    </div>
    <strong><span style="color: #FFFFFF;">Copyright &copy; {{ date('Y') }}</span> <a href="http://zapwallet.in" target="_blank"><span style="color: red;">ZapWallet</span></a>.</strong><span style="color: #FFFFFF;"> All rights
    reserved.</span>
  </footer>


  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<!-- Slimscroll -->
<!-- <script src="../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script> -->
<!-- FastClick -->
<!-- <script src="../bower_components/fastclick/lib/fastclick.js"></script> -->
<!-- AdminLTE App -->
<script src="{{ asset('assets/js/admin.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<!-- <script src="/js/demo.js"></script> -->
@yield('script')
</body>
</html>
