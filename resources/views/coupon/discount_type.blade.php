<div class='form-group'>
      <label class='control-label col-xs-3 col-sm-3'>Discount Type:</label>
      <div class='col-xs-9 col-sm-9'>
          <label class='radio-inline'><input type='radio' name='discount_type' id='flat_discount' value='Flat Discount'>Flat Discount</label>
    <label class='radio-inline'><input type='radio' name='discount_type' id='per_discount' value='Percentage Discount'>Percentage Discount</label>
      </div>
</div>

<script>
    $(document).ready(function(){

        $("#flat_discount").change(function(){
            var flat_discount = $("#flat_discount").val();
            if (flat_discount == 'Flat Discount') {
                //alert(flat_discount);
                $('#radio_info_show').html(
            		"<div class='form-group'>"+
				        "<label class='control-label col-sm-3' >Discount Amount:</label>"+
				        "<div class='col-xs-9 col-sm-9'>"+
			        		"<input type='number' class='form-control'  placeholder='Enter Discount Amount' name='flat_discount'>"+
				        "</div>"+
					"</div>"
            	);
            }     
        });

        $("#per_discount").change(function(){
            var per_discount = $("#per_discount").val();
            if (per_discount == 'Percentage Discount') {
                //alert(per_discount);
                $('#radio_info_show').html(
            		"<div class='form-group'>"+
				        "<label class='control-label col-sm-3' >Discount in Percentage:</label>"+
				        "<div class='col-xs-9 col-sm-9'>"+
			        		"<input type='number' class='form-control'  placeholder='Enter Discount in Percentage' name='per_discount'>"+
				        "</div>"+
					"</div>"+

					"<div class='form-group'>"+
				        "<label class='control-label col-sm-3'>Max Discount:</label>"+
				        "<div class='col-xs-9 col-sm-9'>"+
			        		"<input type='number' class='form-control'  placeholder='Enter Max Discount' name='max_discount'>"+
				        "</div>"+
					"</div>"
            	);
            }      
        });
    });

</script>