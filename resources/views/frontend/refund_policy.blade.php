@extends('blades.base')

{{-- @section('page title', 'Index') --}}

@section('css')
<link rel="stylesheet" href="{{ asset('css/custom.css')}}">
@endsection

@section('innerbanner')

<!-- innerbanner -->    

<div class="banner">
	<s-banner></s-banner>
</div>

<!-- //innerbanner -->

@endsection

@section('breadcrumbs')
@endsection

@section('horizontal tab')

@endsection

@section('vertical tab')

<!-- breadcrumbs -->
	<div class="w3layouts-breadcrumbs text-center">
		<div class="container">
			<span class="agile-breadcrumbs"><a href="/"><i class="fa fa-home home_1"></i></a> / <span>Terms and Conditions</span></span>
		</div>
	</div>
	<!-- //breadcrumbs -->

	<!-- Terms of use-section -->
	<div class="agile-terms w3layouts-content">
		<section class="terms-of-use">			
			<!-- terms -->
			<div class="terms">
				<div class="container">
                    <h3 class="w3-head">Privacy policy</h3>

					<h6>Refunds</h6>
					<h6>Refunds: 3 – 21 working Days.</h6>
					<p>
					All sales are final with no refund or exchange permitted. You are responsible for the payment of the mobile number or DTH account number or bill payment and all charges that result from these payments. We shall not be responsible for any payment for an incorrect mobile number or DTH account number or bill payment number. In case, money has been charged to your card or bank account or pre-paid payment instrument and a payment/service is not delivered by the service provider within 24 hours of your completion of the transaction then you may inform us regarding the same by sending an email to [care@zapwallet.in]. Please include in the email the following details â)the mobile number or DTH account number or bill payment number, service provider name, payment value, transaction date and order number. We shall investigate such incidents and if it is found that money was indeed charged to your card or bank account or pre-paid payment instrument without delivery of the payment/service then you will be refunded the money within 21 working days from the date of receipt of your email. All refunds will be credited to your card or bank account or the [ZAPWALLET.IN] Balance. We shall have the sole discretion to determine the mode of reversal from the above mentioned options.
					</p>


				</div>
			</div>
			<!-- /terms -->
		</section>
		</div>
		<!-- //Terms of use-section -->


@endsection

@section('tab title')
    Plan
@endsection