(function(){
    "use strict";

    angular.module('User', ['ui.bootstrap', 'ngAnimate'])

    .factory('UserSummaryRepository', UserSummaryRepository)

    .directive('summary', Summary)

    .directive('account', Account)

    .directive('billingAndPlan', BillingAndPlan)

    .directive('accountSelection', AccountSelection)

    function Summary(BASE, BlockUI) {
        return {
            restrict: 'EA',
            controller: SummaryController,
            templateUrl: BASE.API_URL + BASE.ASSETS_URL + 'user/templates/dashboard/index.html',
            link: function (scope, iElement, iAttrs) {

            }
        };
    }

    function SummaryController(BASE, $scope, $timeout, BlockUI, UserSummaryRepository) {
        

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $scope.isLoading = true;

        $timeout(function(){
            $scope.loadSyncOrderList();
            $scope.loadSummary();
            $scope.loadSyncedContacts();
        });


        $scope.summary = {
            contacts_collected: {
                today: 0,
                last_seven_days: 0,
                last_thirty_days: 0,
                all_time:0
            },

            xero_orders_found: {
                today: 0,
                last_seven_days: 0,
                last_thirty_days: 0,
                all_time:0
            },

            xero_orders_synced: {
                today: 0,
                last_seven_days: 0,
                last_thirty_days: 0,
                all_time:0
            },

            orders_skipped_no_matching_contact: {
                today: 0,
                last_seven_days: 0,
                last_thirty_days: 0,
                all_time:0
            },

            orders_skipped_over_plan_limit: {
                today: 0,
                last_seven_days: 0,
                last_thirty_days: 0,
                all_time:0
            },
        };


        $scope.loadSummary = function(){

            let params = {};

            $scope.isLoading = true;
            BlockUI.open('#summary.tab-pane', '<div class=" fa-spin flaticon-refresh"></div> loading...');

            UserSummaryRepository.getSummary(params).then(function(response){
                
                let response_data = response.data.success.data;

                $scope.summary = response_data;

                $timeout(function(){
                    $scope.isLoading = false;
                    BlockUI.close('#summary.tab-pane');
                });

            }, function(response){
                
                $timeout(function(){
                    $scope.isLoading = false;
                    BlockUI.close('#summary.tab-pane');
                });
            })
        }

        $scope.loadSyncOrderList = function(){

            let t,e;

            t = $("#load_sync_orders").mDatatable({
                data: {
                    type: "remote", 
                    source: {
                        read: {
                            url: "/user/demo/synced-order-list",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            map: function(t) {
                                var e = t;
                                return void 0 !== t.data && (e = t.data),
                                e
                            }
                        }
                    }, 
                    pageSize: 10,
                    serverPaging: !0,
                    serverFiltering: !0,
                    serverSorting: !0
                }, 
                layout: {
                    theme: "default", 
                    class: "", 
                    scroll: !1, 
                    footer: !1
                }, 
                sortable:!0, 
                pagination:!0, 
                toolbar:{items:{pagination:{pageSizeSelect:[10,20,30,50,100]}}},
                search: {
                    input: $("#generalSearch")
                }, 
                columns:[{
                    field: "InvoiceId", 
                    title: "Invoice ID", 
                    width: 50, 
                    sortable: !1, 
                    selector: !1, 
                    textAlign: "center"
                },{
                    field: "MatchedEmail", 
                    title: "Matched Email"
                },{
                    field:"ProcessedStatus", 
                    title:"Processed Status", 
                    template:function(t) {
                        var e= {
                            1: {
                                title: "To Be Synced", 
                                class: "m-badge--brand"
                            }
                            , 2: {
                                title: "Completed", 
                                class: " m-badge--metal"
                            }
                            , 3: {
                                title: "Unmatched", 
                                class: " m-badge--primary"
                            }
                        };
                        return'<span class="m-badge '+e[t.ProcessedStatus].class+' m-badge--wide">'+e[t.ProcessedStatus].title+"</span>"
                    }
                },{
                    field: "TotalValue", 
                    title: "Total Value", 
                    width: 110
                },{
                    field:"DateTimePassed", 
                    title:"Date/Time Passed"
                }]
            });

            e = t.getDataSourceQuery(), 
                $("#ProcessedStatus").on("change", function() {
                    t.search($(this).val(), "Status")
                }).val(void 0!==e.ProcessedStatus?e.ProcessedStatus:""),
                $("#ProcessedStatus").selectpicker();
        }

        $scope.loadSyncedContacts = function(){

            let t,e;

            t = $("#load_sync_contacts").mDatatable({
                data: {
                    type: "remote", 
                    source: {
                        read: {
                            url: "/user/demo/synced-contact-list",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            map: function(t) {
                                var e = t;
                                return void 0 !== t.data && (e = t.data),
                                e
                            }
                        }
                    }, 
                    pageSize: 10,
                    serverPaging: !0,
                    serverFiltering: !0,
                    serverSorting: !0
                }, 
                layout: {
                    theme: "default", 
                    class: "", 
                    scroll: !1, 
                    footer: !1
                }, 
                sortable:!0, 
                pagination:!0,
                toolbar:{items:{pagination:{pageSizeSelect:[10,20,30,50,100]}}},
                search: {
                    input: $("#emailSearch")
                }, 
                columns:[{
                    field: "EmailAddress", 
                    title: "Email Address", 
                    sortable: !1, 
                    selector: !1, 
                    textAlign: "center"
                },{
                    field: "Processed", 
                    title: "Processed"
                },{
                    field:"DateTimePassed", 
                    title:"Date/Time Passed"
                }]
            });
        }
        
    }

    function Account(BASE, BlockUI) {
        return {
            restrict: 'EA',
            controller: AccountController,
            templateUrl: BASE.API_URL + BASE.ASSETS_URL + 'user/templates/account/index.html',
            link: function (scope, iElement, iAttrs) {

            }
        };
    }

    function AccountController(BASE, $scope, $timeout, BlockUI, UserSummaryRepository, $uibModal) {

        $scope.isLoading = true;
        BlockUI.open('.m-portlet__body', '<div class=" fa-spin flaticon-refresh"></div> loading...');

        $timeout(function(){
            $scope.isLoading = false;
            BlockUI.close('.m-portlet__body');
        },1000);

        $scope.addXeroAccountForm = function(){
            $uibModal.open({
                animation: true,
                templateUrl: BASE.API_URL + BASE.ASSETS_URL +'user/templates/account/add-xero-account-form.html',
                backdrop: 'static',
                controller: function($uibModalInstance, $scope, BlockUI, $timeout, BASE){
                  
                    $scope.isLoaded = false;
                    BlockUI.open('.modal-body', {
                        overlayColor: '#000000',
                        type: 'loader',
                        message: 'Loading...'
                    });

                    $timeout(function(){
                        $scope.isLoaded = true;
                        BlockUI.close('.modal-body');
                    });

                    $scope.submit = function(){
                        console.log('Add Xero Account');
                    }

                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('close');
                    }
                },
                size: 'md'
            });
        }


        $scope.loadAccount = function(){

            $scope.isLoading = true;
            BlockUI.open('.m-stack.m-stack--hor.m-stack--desktop', '<div class=" fa-spin flaticon-refresh"></div> loading...');

            UserSummaryRepository.getSummary($scope.email).then(function(response){
                let response_data = response.data;

                $scope.isLoading = false;
                BlockUI.close('.m-stack.m-stack--hor.m-stack--desktop');

                // Reset the registration form
                $scope.resetForm();

                swal({
                    position:"center",
                    type:"success",
                    title:'New Activation Code has been sent',
                    html: response_data.success.message,
                    showConfirmButton:!1,
                    timer:5000
                });

                $timeout(function(){
                    document.location = '/login';
                },5500)

            }, function(response){
                
                let response_error = response.data;

                let html = '<div class="m-demo"><div class="m-demo__preview"><div class="m-list-timeline"><div class="m-list-timeline__items">';

                if (response_error.errors) {

                    $.each(response_error.errors, function(index, val) {
                         '<li>' + val[0] + '</li>';

                         html += '<div class="m-list-timeline"><div class="m-list-timeline__items"><div class="m-list-timeline__item"><span class="m-list-timeline__icon flaticon-cancel"></span><span class="m-list-timeline__text">';
                         html += val[0];
                         html += '</span></div></div></div>';
                    });

                }

                if (response_error.error) {

                     '<li>' + response_error.error.message + '</li>';

                     html += '<div class="m-list-timeline"><div class="m-list-timeline__items"><div class="m-list-timeline__item"><span class="m-list-timeline__icon flaticon-cancel"></span><span class="m-list-timeline__text">';
                     html += response_error.error.message;
                     html += '</span></div></div></div>';
        

                }

                html += '</div></div></div></div>';

                swal({
                    position:"center",
                    type:"warning",
                    title: response_error.message,
                    html: html,
                    showConfirmButton:!1,
                    timer:5000
                });

                $scope.isLoading = false;
                BlockUI.close('.m-stack.m-stack--hor.m-stack--desktop');
            })
        }
    }

    function BillingAndPlan(BASE, BlockUI) {
        return {
            restrict: 'EA',
            controller: BillingAndPlanController,
            templateUrl: BASE.API_URL + BASE.ASSETS_URL + 'user/templates/billing/index.html',
            link: function (scope, iElement, iAttrs) {

            }
        };
    }

    function BillingAndPlanController(BASE, $scope, $timeout, BlockUI, UserSummaryRepository) {
        

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $scope.isLoading = true;

        $timeout(function(){
            $scope.loadSyncOrderList();
            $scope.loadSummary();
            $scope.loadSyncedContacts();
        });

        $scope.summary = {
            contacts_collected: {
                today: 0,
                last_seven_days: 0,
                last_thirty_days: 0,
                all_time:0
            },

            xero_orders_found: {
                today: 0,
                last_seven_days: 0,
                last_thirty_days: 0,
                all_time:0
            },

            xero_orders_synced: {
                today: 0,
                last_seven_days: 0,
                last_thirty_days: 0,
                all_time:0
            },

            orders_skipped_no_matching_contact: {
                today: 0,
                last_seven_days: 0,
                last_thirty_days: 0,
                all_time:0
            },

            orders_skipped_over_plan_limit: {
                today: 0,
                last_seven_days: 0,
                last_thirty_days: 0,
                all_time:0
            },
        };


        $scope.loadSummary = function(){

            $scope.isLoading = true;
            BlockUI.open('#summary.tab-pane', '<div class=" fa-spin flaticon-refresh"></div> loading...');

            UserSummaryRepository.getSummary($scope.email).then(function(response){
                
                let response_data = response.data.success.data;

                $scope.summary = response_data;

                $timeout(function(){
                    $scope.isLoading = false;
                    BlockUI.close('#summary.tab-pane');
                });

            }, function(response){
                
                $timeout(function(){
                    $scope.isLoading = false;
                    BlockUI.close('#summary.tab-pane');
                });
            })
        }

        $scope.loadSyncOrderList = function(){

            let t,e;

            t = $("#load_sync_orders").mDatatable({
                data: {
                    type: "remote", 
                    source: {
                        read: {
                            url: "/user/demo/synced-order-list",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            map: function(t) {
                                var e = t;
                                return void 0 !== t.data && (e = t.data),
                                e
                            }
                        }
                    }, 
                    pageSize: 10,
                    serverPaging: !0,
                    serverFiltering: !0,
                    serverSorting: !0
                }, 
                layout: {
                    theme: "default", 
                    class: "", 
                    scroll: !1, 
                    footer: !1
                }, 
                sortable:!0, 
                pagination:!0, 
                toolbar:{items:{pagination:{pageSizeSelect:[10,20,30,50,100]}}},
                search: {
                    input: $("#generalSearch")
                }, 
                columns:[{
                    field: "InvoiceId", 
                    title: "Invoice ID", 
                    width: 50, 
                    sortable: !1, 
                    selector: !1, 
                    textAlign: "center"
                },{
                    field: "MatchedEmail", 
                    title: "Matched Email"
                },{
                    field:"ProcessedStatus", 
                    title:"Processed Status", 
                    template:function(t) {
                        var e= {
                            1: {
                                title: "To Be Synced", 
                                class: "m-badge--brand"
                            }
                            , 2: {
                                title: "Completed", 
                                class: " m-badge--metal"
                            }
                            , 3: {
                                title: "Unmatched", 
                                class: " m-badge--primary"
                            }
                        };
                        return'<span class="m-badge '+e[t.ProcessedStatus].class+' m-badge--wide">'+e[t.ProcessedStatus].title+"</span>"
                    }
                },{
                    field: "TotalValue", 
                    title: "Total Value", 
                    width: 110
                },{
                    field:"DateTimePassed", 
                    title:"Date/Time Passed"
                }]
            });

            e = t.getDataSourceQuery(), 
                $("#ProcessedStatus").on("change", function() {
                    t.search($(this).val(), "Status")
                }).val(void 0!==e.ProcessedStatus?e.ProcessedStatus:""),
                $("#ProcessedStatus").selectpicker();
        }

        $scope.loadSyncedContacts = function(){

            let t,e;

            t = $("#load_sync_contacts").mDatatable({
                data: {
                    type: "remote", 
                    source: {
                        read: {
                            url: "/user/demo/synced-contact-list",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            map: function(t) {
                                var e = t;
                                return void 0 !== t.data && (e = t.data),
                                e
                            }
                        }
                    }, 
                    pageSize: 10,
                    serverPaging: !0,
                    serverFiltering: !0,
                    serverSorting: !0
                }, 
                layout: {
                    theme: "default", 
                    class: "", 
                    scroll: !1, 
                    footer: !1
                }, 
                sortable:!0, 
                pagination:!0,
                toolbar:{items:{pagination:{pageSizeSelect:[10,20,30,50,100]}}},
                search: {
                    input: $("#emailSearch")
                }, 
                columns:[{
                    field: "EmailAddress", 
                    title: "Email Address", 
                    sortable: !1, 
                    selector: !1, 
                    textAlign: "center"
                },{
                    field: "Processed", 
                    title: "Processed"
                },{
                    field:"DateTimePassed", 
                    title:"Date/Time Passed"
                }]
            });
        }
        
    }    

    function AccountSelection(BASE, BlockUI) {
        return {
            restrict: 'EA',
            scope: {
                callback: '&'
            },
            controller: AccountSelectionController,
            templateUrl: BASE.API_URL + BASE.ASSETS_URL + 'user/templates/account/account-selection.html',
            link: function (scope, iElement, iAttrs) {

            }
        };
    }

    function AccountSelectionController(BASE, $scope, $timeout, BlockUI, UserSummaryRepository) {

        $scope.isLoading = true;
        BlockUI.open('.m-portlet__body', '<div class=" fa-spin flaticon-refresh"></div> loading...');

        $timeout(function(){
            $scope.isLoading = false;
            BlockUI.close('.m-portlet__body');
            $("#m_select2_1, #m_select2_1_validate").select2({placeholder:"Select a state"})
        },1000);

        $scope.loadAccount = function(){

            $scope.isLoading = true;
            BlockUI.open('.m-stack.m-stack--hor.m-stack--desktop', '<div class=" fa-spin flaticon-refresh"></div> loading...');

            UserSummaryRepository.getSummary($scope.email).then(function(response){
                let response_data = response.data;

                $scope.isLoading = false;
                BlockUI.close('.m-stack.m-stack--hor.m-stack--desktop');

                // Reset the registration form
                $scope.resetForm();

                swal({
                    position:"center",
                    type:"success",
                    title:'New Activation Code has been sent',
                    html: response_data.success.message,
                    showConfirmButton:!1,
                    timer:5000
                });

                $timeout(function(){
                    document.location = '/login';
                },5500)

            }, function(response){
                
                let response_error = response.data;

                let html = '<div class="m-demo"><div class="m-demo__preview"><div class="m-list-timeline"><div class="m-list-timeline__items">';

                if (response_error.errors) {

                    $.each(response_error.errors, function(index, val) {
                         '<li>' + val[0] + '</li>';

                         html += '<div class="m-list-timeline"><div class="m-list-timeline__items"><div class="m-list-timeline__item"><span class="m-list-timeline__icon flaticon-cancel"></span><span class="m-list-timeline__text">';
                         html += val[0];
                         html += '</span></div></div></div>';
                    });

                }

                if (response_error.error) {

                     '<li>' + response_error.error.message + '</li>';

                     html += '<div class="m-list-timeline"><div class="m-list-timeline__items"><div class="m-list-timeline__item"><span class="m-list-timeline__icon flaticon-cancel"></span><span class="m-list-timeline__text">';
                     html += response_error.error.message;
                     html += '</span></div></div></div>';
        

                }

                html += '</div></div></div></div>';

                swal({
                    position:"center",
                    type:"warning",
                    title: response_error.message,
                    html: html,
                    showConfirmButton:!1,
                    timer:5000
                });

                $scope.isLoading = false;
                BlockUI.close('.m-stack.m-stack--hor.m-stack--desktop');
            })
        }
    }    

    function UserSummaryRepository(BASE, $http) {
        
        let url = BASE.API_URL + BASE.API_VERSION + 'user/summary';

        let repo = {};

        repo.getSummary = function(params){
            return $http.get(url, {params});
        }

        return repo;
    }

})();
