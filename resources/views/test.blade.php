@extends('blades.base')

{{--@section('page title', 'Test Title')--}}

@section('innerbanner')
<!-- innerbanner -->	
<div class="agileits-inner-banner">
    
</div>
<!-- //innerbanner -->
@endsection

@section('breadcrumbs')
<!-- breadcrumbs -->
<div class="w3layouts-breadcrumbs text-center">
    <div class="container">
        <span class="agile-breadcrumbs">
            
            <a href="/">
                    <i class="fa fa-home home_1"></i>
                </a> / <span>Plans</span>
        </span>
    </div>
</div>
<!-- //breadcrumbs -->
@endsection

@section('horizontal tab')
<!--Horizontal Tab-->
<div id="parentHorizontalTab">
    <ul class="resp-tabs-list hor_1">
        <li>FULLTT</li>
        <li>TOPUP</li>
        <li>3G/4G</li>
        <li>SPL/RATE CUTTER</li>
        <li>2G</li>
    </ul>
    <div class="resp-tabs-container hor_1">
        <div>
            <ul class="w3ls-plan">
                <li>
                    <a href="pay.html">
                        <div class="agile-price">
                            <span>Rs. 15</span> 
                        </div>
                        <div class="valid-agileits">
                            <p>Full Talktime Rs.15</p>
                            <p><strong>Validity: 2days</strong></p>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                <li>
                    <a href="pay.html">
                        <div class="agile-price">
                            <span>Rs. 25</span> 
                        </div>
                        <div class="valid-agileits">
                            <p>Full Talktime Rs. 25 - 5 Local SMS free - Free SMS valid for 1 day(s)</p>
                            <p><strong>Validity: 3days</strong></p>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                    <li>
                    <a href="pay.html">
                        <div class="agile-price">
                            <span>Rs. 55</span> 
                        </div>
                        <div class="valid-agileits">
                            <p>Full Talktime Rs. 55 - 5 Local SMS free - Free SMS valid for 1 day(s)</p>
                            <p><strong>Validity: 5days</strong></p>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                <li>
                    <a href="pay.html">
                        <div class="agile-price">
                            <span>Rs. 65</span> 
                        </div>
                        <div class="valid-agileits">
                            <p>Full Talktime Rs. 65 - 5 Local SMS free - Free SMS valid for 2 day(s) - Applicable only for 4G</p>
                            <p><strong>Validity: 28days</strong></p>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                <li>
                    <a href="pay.html">
                        <div class="agile-price">
                            <span>Rs. 150</span> 
                        </div>
                        <div class="valid-agileits">
                            <p>Full Talktime Rs. 150 - Applicable only for 4G</p>
                            <p><strong>Validity: Unlimited Days</strong></p>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                <li>
                    <a href="pay.html">
                        <div class="agile-price">
                            <span>Rs. 195</span> 
                        </div>
                        <div class="valid-agileits">
                            <p>Full Talktime Rs. 198 - 5 Local and National SMS free - Free SMS valid for 2 day(s)</p>
                            <p><strong>Validity: Unlimited Days</strong></p>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                    <li>
                    <a href="pay.html">
                        <div class="agile-price">
                            <span>Rs. 500</span> 
                        </div>
                        <div class="valid-agileits">
                            <p>Full Talktime Rs. 500</p>
                            <p><strong>Validity: Unlimited Days</strong></p>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                <li>
                    <a href="pay.html">
                        <div class="agile-price">
                            <span>Rs. 555</span> 
                        </div>
                        <div class="valid-agileits">
                            <p>Full Talktime Rs. 555 - 5 Local SMS free - Free SMS valid for 3 day(s)</p>
                            <p><strong>Validity: Unlimited Days</strong></p>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                                    </ul>
        </div>
        <div>
            <ul class="w3ls-plan">
                
                <li>
                    <a href="pay.html">
                        <div class="agile-price">
                            <span>Rs. 10</span> 
                        </div>
                        <div class="valid-agileits">
                            <p>Talktime Rs. 7.70 - Applicable only for 4G</p>
                            <p><strong>Validity: Unlimited Days</strong></p>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                <li>
                    <a href="pay.html">
                        <div class="agile-price">
                            <span>Rs. 20</span> 
                        </div>
                        <div class="valid-agileits">
                            <p>Talktime Rs. 15.39 - Applicable only for 4G</p>
                            <p><strong>Validity: Unlimited Days</strong></p>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                    <li>
                    <a href="pay.html">
                        <div class="agile-price">
                            <span>Rs. 30</span> 
                        </div>
                        <div class="valid-agileits">
                            <p>Talktime Rs. 23.09 - Applicable only for 4G</p>
                            <p><strong>Validity: Unlimited Days</strong></p>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                <li>
                    <a href="pay.html">
                        <div class="agile-price">
                            <span>Rs. 50</span> 
                        </div>
                        <div class="valid-agileits">
                            <p>Talktime Rs. 40.48 - Applicable only for 4G</p>
                            <p><strong>Validity: Unlimited Days</strong></p>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                <li>
                    <a href="pay.html">
                        <div class="agile-price">
                            <span>Rs. 60</span> 
                        </div>
                        <div class="valid-agileits">
                            <p>Talktime Rs. 49.17 - Applicable only for 4G</p>
                            <p><strong>Validity: Unlimited Days</strong></p>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                <li>
                    <a href="pay.html">
                        <div class="agile-price">
                            <span>Rs. 70</span> 
                        </div>
                        <div class="valid-agileits">
                            <p>Talktime Rs. 57.87 - Applicable only for 4G</p>
                            <p><strong>Validity: Unlimited Days</strong></p>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                    <li>
                    <a href="pay.html">
                        <div class="agile-price">
                            <span>Rs. 80</span> 
                        </div>
                        <div class="valid-agileits">
                            <p>Talktime Rs. 66.70 - Applicable only for 4G</p>
                            <p><strong>Validity: Unlimited Days</strong></p>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                <li>
                    <a href="pay.html">
                        <div class="agile-price">
                            <span>Rs. 100</span> 
                        </div>
                        <div class="valid-agileits">
                            <p>Talktime Rs. 83.96 - Applicable only for 4G</p>
                            <p><strong>Validity: Unlimited Days</strong></p>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                                    </ul>
        </div>
        <div>
            <ul class="w3ls-plan">
                <li>
                    <a href="pay.html">
                        <div class="agile-price">
                            <span>Rs. 11</span> 
                        </div>
                        <div class="valid-agileits">
                            <p>10 MB - Applicable only for 4G - For 4G users only</p>
                            <p><strong>Validity: 1day</strong></p>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                <li>
                    <a href="pay.html">
                        <div class="agile-price">
                            <span>Rs. 18</span> 
                        </div>
                        <div class="valid-agileits">
                            <p>50 MB - Applicable only for 4G - For 4G users only</p>
                            <p><strong>Validity: 7days</strong></p>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                    <li>
                    <a href="pay.html">
                        <div class="agile-price">
                            <span>Rs. 39</span> 
                        </div>
                        <div class="valid-agileits">
                            <p>200 MB - Applicable only for 4G - For 4G users only</p>
                            <p><strong>Validity: 28days</strong></p>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                <li>
                    <a href="pay.html">
                        <div class="agile-price">
                            <span>Rs. 51</span> 
                        </div>
                        <div class="valid-agileits">
                            <p>100 MB - Applicable only for 4G - For 4G users only</p>
                            <p><strong>Validity: 6days</strong></p>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                <li>
                    <a href="pay.html">
                        <div class="agile-price">
                            <span>Rs. 54</span> 
                        </div>
                        <div class="valid-agileits">
                            <p>94 MB - Applicable only for 4G - For 4G users only</p>
                            <p><strong>Validity: 7days</strong></p>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                <li>
                    <a href="pay.html">
                        <div class="agile-price">
                            <span>Rs. 67</span> 
                        </div>
                        <div class="valid-agileits">
                            <p>203 MB - Applicable only for 4G - For 4G users only</p>
                            <p><strong>Validity: 7days</strong></p>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                    <li>
                    <a href="pay.html">
                        <div class="agile-price">
                            <span>Rs. 89</span> 
                        </div>
                        <div class="valid-agileits">
                            <p>250 MB - Applicable only for 4G - For 4G users only</p>
                            <p><strong>Validity: 30days</strong></p>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                <li>
                    <a href="pay.html">
                        <div class="agile-price">
                            <span>Rs. 97</span> 
                        </div>
                        <div class="valid-agileits">
                            <p>169 MB - Applicable only for 4G - For 4G users only</p>
                            <p><strong>Validity: 14days</strong></p>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                                    </ul>
        </div>
        <div>
            <ul class="w3ls-plan">
                <li>
                    <a href="pay.html">
                        <div class="agile-price">
                            <span>Rs. 7</span> 
                        </div>
                        <div class="valid-agileits">
                            <p>Local Onnet Mobile @ Rs. 0.10/min - Local Offnet Mobile @ Rs. 0.25/min - Local Onnet Landline @ Rs. 0.10/min - Local Offnet Landline @ Rs. 0.25/min - STD Onnet Mobile @ Rs. 0.10/min - STD Offnet Mobile @ Rs. 0.25/min - STD Onnet Landline @ Rs. 0.10/min - STD Offnet Landline @ Rs. 0.25/min</p>
                            <p><strong>Validity: 1day</strong></p>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                <li>
                    <a href="pay.html">
                        <div class="agile-price">
                            <span>Rs. 8</span> 
                        </div>
                        <div class="valid-agileits">
                            <p>Local Onnet Mobile @ Rs. 0.01/6 sec - Local Offnet Mobile @ Rs. 0.01/2 sec - Local Onnet Landline @ Rs. 0.01/6 sec - Local Offnet Landline @ Rs. 0.01/2 sec - STD Onnet Mobile @ Rs. 0.01/6 sec - STD Offnet Mobile @ Rs. 0.01/2 sec - STD Onnet Landline @ Rs. 0.01/6 sec - STD Offnet Landline @ Rs. 0.01/2 sec</p>
                            <p><strong>Validity: 1day</strong></p>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                    <li>
                    <a href="pay.html">
                        <div class="agile-price">
                            <span>Rs. 9</span> 
                        </div>
                        <div class="valid-agileits">
                            <p>60 Local Onnet min free</p>
                            <p><strong>Validity: 1day</strong></p>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                <li>
                    <a href="pay.html">
                        <div class="agile-price">
                            <span>Rs. 14</span> 
                        </div>
                        <div class="valid-agileits">
                            <p>150 Local and National SMS free - Applicable only for 4G</p>
                            <p><strong>Validity: 14days</strong></p>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                <li>
                    <a href="pay.html">
                        <div class="agile-price">
                            <span>Rs. 18</span> 
                        </div>
                        <div class="valid-agileits">
                            <p>Local Onnet Mobile @ Rs. 0.15/min - Local Offnet Mobile @ Rs. 0.35/min - STD Mobile @ Rs. 0.35/min</p>
                            <p><strong>Validity: 3Days</strong></p>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                <li>
                    <a href="pay.html">
                        <div class="agile-price">
                            <span>Rs. 31</span> 
                        </div>
                        <div class="valid-agileits">
                            <p>3000 Local sec free</p>
                            <p><strong>Validity: 5Days</strong></p>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                    <li>
                    <a href="pay.html">
                        <div class="agile-price">
                            <span>Rs. 37</span> 
                        </div>
                        <div class="valid-agileits">
                            <p>Local @ Rs. 0.30/min - Applicable only for 4G</p>
                            <p><strong>Validity: 15Days</strong></p>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                <li>
                    <a href="pay.html">
                        <div class="agile-price">
                            <span>Rs. 49</span> 
                        </div>
                        <div class="valid-agileits">
                            <p>4800 Local and STD Mobile sec free</p>
                            <p><strong>Validity: 7Days</strong></p>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                                    </ul>
        </div>
        <div>
            <ul class="w3ls-plan">
                <li>
                    <a href="pay.html">
                        <div class="agile-price">
                            <span>Rs. 11</span> 
                        </div>
                        <div class="valid-agileits">
                            <p>10 MB</p>
                            <p><strong>Validity: 1day</strong></p>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                <li>
                    <a href="pay.html">
                        <div class="agile-price">
                            <span>Rs. 18</span> 
                        </div>
                        <div class="valid-agileits">
                            <p>50 MB</p>
                            <p><strong>Validity: 7days</strong></p>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                    <li>
                    <a href="pay.html">
                        <div class="agile-price">
                            <span>Rs. 39</span> 
                        </div>
                        <div class="valid-agileits">
                            <p>200 MB</p>
                            <p><strong>Validity: 28days</strong></p>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                <li>
                    <a href="pay.html">
                        <div class="agile-price">
                            <span>Rs. 51</span> 
                        </div>
                        <div class="valid-agileits">
                            <p>100 MB</p>
                            <p><strong>Validity: 6days</strong></p>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                <li>
                    <a href="pay.html">
                        <div class="agile-price">
                            <span>Rs. 54</span> 
                        </div>
                        <div class="valid-agileits">
                            <p>94 MB</p>
                            <p><strong>Validity: 7days</strong></p>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                <li>
                    <a href="pay.html">
                        <div class="agile-price">
                            <span>Rs. 67</span> 
                        </div>
                        <div class="valid-agileits">
                            <p>203 MB</p>
                            <p><strong>Validity: 7days</strong></p>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                    <li>
                    <a href="pay.html">
                        <div class="agile-price">
                            <span>Rs. 89</span> 
                        </div>
                        <div class="valid-agileits">
                            <p>250 MB</p>
                            <p><strong>Validity: 30days</strong></p>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                <li>
                    <a href="pay.html">
                        <div class="agile-price">
                            <span>Rs. 97</span> 
                        </div>
                        <div class="valid-agileits">
                            <p>169 MB</p>
                            <p><strong>Validity: 14days</strong></p>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                                    </ul>
        </div>
    </div>
</div>
<!--Plug-in Initialisation-->
<script type="text/javascript">
    $(document).ready(function() {
        //Horizontal Tab
        $('#parentHorizontalTab').easyResponsiveTabs({
            type: 'default', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true, // 100% fit in a container
            tabidentify: 'hor_1', // The tab groups identifier
            activate: function(event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#nested-tabInfo');
                var $name = $('span', $info);
                $name.text($tab.text());
                $info.show();
            }
        });
    });
</script>
@endsection

@section('tab title')
    Plan
@endsection

@section('header-right')
@endsection

@section('vertical tab')
    
@endsection