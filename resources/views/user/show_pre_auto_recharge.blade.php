@extends('blades.base')

{{-- @section('page title', 'Index') --}}

@section('innerbanner')
    <!-- innerbanner -->
    {{-- <div class="agileits-inner-banner">

    </div> --}}
    <!-- //innerbanner -->
@endsection

@section('breadcrumbs')
@endsection

@section('horizontal tab')

@endsection
@section('css')
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="{{ asset('css/travel.css')}}">
    <link rel="stylesheet" href="{{ asset('css/custom.css')}}">
    <link rel="stylesheet" href="{{ asset('css/tool-tip.css')}}">
    <link rel="stylesheet" href="{{ asset('css/myprofile-style.css')}}">
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css">
@endsection

@section('vertical tab')
    <div class="container">
        <h2>Previous Auto Recharge</h2>
        <div class="profile">
            <!--<div class="row">-->
            <!--    <div class="col-md-12 col-lg-12">-->
            <!--        <div class="text-center">-->
            <!--            <img class="avatar" src="/images/profile-images/avatar.png" alt="">-->
            <!--            <p class="username">{{ $user->name }}</p>-->
            <!--        </div>-->
            <!--    </div>-->
            <!--</div>-->

        
            
            <div class="row">
    
                <div class="panel panel-info">
    	            <div class="panel-heading">
    	                <h3 class="panel-title text-center"><i class="fa fa-list-ul"></i> List of <code><b>Previous Auto Recharges</b></code></h3>
    	            </div>
    	            <div class="panel-body">
    	            	<div class="table-responsive">
    		                <table id="myTable" class="table table-bordered table-striped table-hover">
    		                    <thead>
    		                        <tr class="success">
    		                            <th>SL</th>
    		                            <th>Mobile</th>
    		                            <th>Amount</th>
    		                            <th>Operator</th>
    		                            <th>Circle</th>
    		                            <th>Recharge At</th>
    		                        </tr>
    		                    </thead>
    		                    <tbody>
    		                    <?php
    		                        $i = 0;
    		                    ?>
    		                    @foreach($successRecharges as $successRecharge)
    		                        <tr>
    		                            <td>{{ ++$i }}</td>
    		                            <td>{{ $successRecharge->mobile }}</td>
    		                            <td><strong>{{ $successRecharge->balance }}</strong></td>
    		                            <td>{{ $successRecharge->operator }}</td>
    		                            <td>{{ $successRecharge->circle }}</td>
    		                            <td>{{ $successRecharge->recharged_at }}</td>
    		                        </tr>
    		                    @endforeach
    		                    </tbody>
    		                </table>
    	            	</div>
    	            </div>
	            </div>
            </div>
            
        </div>
     
    </div>
@endsection

@section('tab title')
    Plan
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {

            //Vertical Tab
            $('#parentVerticalTab').easyResponsiveTabs({
                type: 'vertical', //Types: default, vertical, accordion
                width: 'auto', //auto or any width like 600px
                fit: true, // 100% fit in a container
                closed: 'accordion', // Start closed if in accordion view
                tabidentify: 'hor_1', // The tab groups identifier
                activate: function(event) { // Callback function if tab is switched
                    var $tab = $(this);
                    var $info = $('#nested-tabInfo2');
                    var $name = $('span', $info);
                    $name.text($tab.text());
                    $info.show();
                }
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $("#tab2").hide();
            $("#tab3").hide();
            $("#tab4").hide();
            $(".tabs-menu a").click(function(event){
                event.preventDefault();
                var tab=$(this).attr("href");
                $(".tab-grid").not(tab).css("display","none");
                $(tab).fadeIn("slow");
            });
        });
    </script>
    <script src="/js/jquery-ui.js"></script>
    <script>
        $(function() {
            $( "#datepicker,#datepicker1" ).datepicker();
        });
    </script>
    
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/js/bootstrap-datetimepicker.min.js"></script>
	<script type="text/javascript">
        $(function () {
            $('#datetimepicker3').datetimepicker({
                format: 'HH:mm'
            });
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datetimepicker({
                format: "YYYY-MM-DD"
            });
        });
    </script>
@endsection

@section('header-right')
    <div class=" header-right">
        <div class="banner">
            <s-banner></s-banner>
        </div>
    </div>
@endsection


