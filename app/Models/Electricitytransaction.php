<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Electricitytransaction extends Model
{
    protected $table = 'electricitytransactions';

    protected $fillable = [
      'order_id',
      'currency',
      'amount',
      'mn'
    ];
    public function users (  ) {
        return $this->belongsTo('App\User');
    }
}
