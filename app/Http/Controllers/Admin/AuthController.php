<?php

namespace App\Http\Controllers\Admin;

use App\Mail\PasswordReset;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
    public function login()
    {
        return view('admin.auth.login');
    }
    
    public function logout()
    {
        Auth::logout();
        return redirect()->route('admin.login');
    }

    public function doLogin(Request $request)
    {

        // Creating Rules for Email and Password
        $validatedData = $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);
        // create our user data for the authentication
        $userdata = array(
            'email' => $request->input('email') ,
            'password' => $request->input('password'),
            'user_type' => 1
        );
        // attempt to do the login

        if (Auth::attempt($userdata))
        {
            //Redirect to dashboard
            return redirect()->route('admin.dashboard')->withInput()->with('success','Login Successful');
        }
        else
        {                // validation not successful, send back to form

            return back()->with('error','Wrong Credentials');
        }
    }

    public function doReset(Request $request)
    {
        $validatedData = $request->validate([
            'email' => 'required|email',
        ]);

        $findUser = User::where('email',$request->input('email'))->first();

        if (!$findUser)
        {
            return back()->with('error','Email not registered to an account');
        }

        $token = str_random(60);

        $userUpdate = User::where('email',$request->input('email'))
            ->update([
                'remember_token' => $token
              ]);

        $user = User::where('email',$request->input('email'))->first();

        Mail::to($user)->send(new PasswordReset($user));

        return redirect()->route('admin.reset')->with('success','Check your email to recover password.');
    }

    public function passwordReset()
    {
        return view('admin.auth.reset');
    }
    public function passwordRecover($token)
    {
        $verifyToken = User::where('remember_token',$token)->first();

        if (!$verifyToken)
        {
            return redirect()->route('admin.reset')->with('error','Password reset token has expired.');
        }
        return view('admin.auth.passwordRecovery',['user'=>$verifyToken]);
    }
    public function doRecover(Request $request,$token)
    {
        $validatedData = $request->validate([
            'password' => 'required|confirmed',
            'email'=>'required',
        ]);

        $verifyToken = User::where('remember_token',$token)->where('email',$request->input('email'))->first();

        if (!$verifyToken)
        {
            return redirect()->route('admin.reset')->with('error','Password reset token has expired.');
        }

        $updatePassword = User::where('remember_token',$token)
            ->update([
                'password' => bcrypt($request->input('password')),
                'remember_token' => str_random(60)
            ]);

        if (!$updatePassword)
        {
            return back()->with('error','An unexpected error occurred.');
        }

        return redirect()->route('admin.login')->with('success','Password updated Successfully.');
    }
}
