
@extends('blades.base')

@section('tab title', 'Profile')
{{-- @section('page title', 'Index') --}}

@section('css')
<link rel="stylesheet" href="{{ asset('css/custom.css')}}">
@endsection

@section('innerbanner')

<!-- innerbanner -->    

<div class="banner">
	<s-banner></s-banner>
</div>

<!-- //innerbanner -->

@endsection



@section('breadcrumbs')

@endsection


@section('horizontal tab')

@endsection



@section('vertical tab')

	<div class="w3layouts-breadcrumbs text-center">

		<div class="container">

			<span class="agile-breadcrumbs">
                <a href="/"><i class="fa fa-home home_1"></i></a> / <span>Profile</span></span>

		</div>

	</div>

	<!-- //breadcrumbs -->

	<!-- About-page -->

	<div class="terms w3ls-about w3layouts-content">

		<div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="avatar">
                        <img src="/images/avatar.png" class="rounded-circle" alt="">
                        <h4>Michael Dam</h4>
                    </div>
                </div>
			</div>
			<div class="row p-content">
				<div class="col-md-3">
					<ul class="nav nav-pills nav-stacked">
						<li class="active"><a href="#">Home</a></li>
						<li><a href="#">Menu 1</a></li>
						<li><a href="#">Menu 2</a></li>
						<li><a href="#">Menu 3</a></li>
					</ul>
				</div>
				<div class="col-md-9">
					<div class="tab-content">
						<div id="home" class="tab-pane fade in active">
							<h3>HOME</h3>
							<p>Some content.</p>
						</div>
						<div id="menu1" class="tab-pane fade">
							<h3>Menu 1</h3>
							<p>Some content in menu 1.</p>
						</div>
						<div id="menu2" class="tab-pane fade">
							<h3>Menu 2</h3>
							<p>Some content in menu 2.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

@endsection



