<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class DeleteUnverifiedUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:deleteUnverifiedUsers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deleting Unverified Users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $unverifiedUsers = User::where('emailVerification',false)->delete();

       if (!$unverifiedUsers)
       {
           echo "failed";
       }else
       {
           echo "Users Deleted";
       }


    }
}
