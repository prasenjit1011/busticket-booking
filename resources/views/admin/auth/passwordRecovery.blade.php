<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <title>Admin Login</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="{{ asset('admins/vendors/iconfonts/mdi/css/materialdesignicons.css') }}">
    <link rel="stylesheet" href="{{ asset('admins/vendors/css/vendor.addons.css') }}">
    <!-- endinject -->
    <!-- vendor css for this page -->
    <!-- End vendor css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{ asset('admins/css/style.css') }}">
    <!-- endinject -->
    <link rel="shortcut icon" href="{{ asset('images/logo.png') }}">
</head>

<body>
<div class="authentication-theme auth-style_1">
    <div class="row">
        <div class="col-12 logo-section">
            <a href="#" class="logo"><img src="{{ asset('images/logo.png') }}" alt="logo"></a>
            <h5 style="text-align: center;padding-top: 30px">Password Recovery</h5>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-5 col-md-7 col-sm-9 col-11 mx-auto">
            <div class="grid">
                <div class="grid-body">
                    <div class="row">
                        <div class="col-lg-7 col-md-8 col-sm-9 col-12 mx-auto form-wrapper">
                            @include('admin.partials.info')
                            <form action="{{ route('admin.recover.action',$user->remember_token) }}" method="post">
                                {{ csrf_field() }}
                                <div class="form-group input-rounded">
                                    <input type="email" class="form-control" name="email" placeholder="Email">
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                    @endif
                                </div>
                                <div class="form-group input-rounded">
                                    <input type="password" class="form-control" name="password" placeholder="Password">
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                    @endif
                                </div>
                                <div class="form-group input-rounded">
                                    <input type="password" class="form-control" name="password_confirmation" placeholder="Password Confirmation">
                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                                    @endif
                                </div>
                                <button type="submit" class="btn btn-primary btn-block">Reset Password</button>
                            </form>
                            <div class="signup-link">
                                <p>Back to login ?</p><a href="{{ route('admin.login') }}"> Login</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="auth_footer">
        <p class="text-muted text-center">© Zapwallet 2019</p>
    </div>
</div>
<!--page body ends -->
<!-- SCRIPT LOADING START FORM HERE /////////////-->
<!-- plugins:js -->
<script src="{{ asset('admins/vendors/js/core.js') }}"></script>
<script src="{{ asset('admins/vendors/js/vendor.addons.js') }}"></script>
<!-- endinject -->
<!-- Vendor Js For This Page Ends-->
<!-- Vendor Js For This Page Ends-->
<script src="{{ asset('admins/js/script.js') }}"></script>
</body>

</html>