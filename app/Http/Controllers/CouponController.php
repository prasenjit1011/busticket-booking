<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use Illuminate\Support\Facades\Input;
use Auth;
use App\Coupon;
use App\Prepaidoperator;

class CouponController extends Controller
{
   public function __construct()
    {
    	$this->middleware('auth');
    }

    public function index()
    {
    	$coupons = Coupon::get();
    	return view('coupon.index', compact('coupons'));
    }

    public function create()
    {
        $prepaidOperators = Prepaidoperator::get();
    	return view('coupon.create', compact('prepaidOperators'));
    }

    public function store(Request $request)
    {
    	//return $request->all();
    	$input = Input::all();
	    $rules = [
	    	'coupon_code' => 'required',
	    	'coupon_name' => 'required',
	    	'coupon_type' => 'required'	
	    ];

	    $messages = [];
	    
    	$validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
        	
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        if ($request->image == null) {
            $coupon = new Coupon;
            $coupon->coupon_code = $request->coupon_code;
            $coupon->coupon_name = $request->coupon_name;
            $coupon->coupon_type = $request->coupon_type;
            $coupon->start_date = $request->start_date;
            $coupon->end_date = $request->end_date;
            $coupon->end_date = $request->end_date;
            $coupon->discount_type = $request->discount_type;
            $coupon->cash_back_type = $request->cash_back_type;
            $coupon->flat_discount = $request->flat_discount;
            $coupon->per_discount = $request->per_discount;
            $coupon->max_discount = $request->max_discount;
            $coupon->flat_cash_back = $request->flat_cash_back;
            $coupon->per_cash_back = $request->per_cash_back;
            $coupon->max_cash_back = $request->max_cash_back;
            $coupon->gift_name = $request->gift_name;
            $coupon->usage_limit = $request->usage_limit;
            $coupon->action = $request->action;
            $coupon->operator = $request->operator;
            $coupon->payment = $request->payment;
            $coupon->description = $request->description;
            $coupon->image = null;
            $coupon->created_by = Auth::id();
            $coupon->save();

        	return redirect('coupon');
        } else {
            if (Input::file('image')->isValid()) {
                //$destinationPath = 'uploads';
                $destinationPath = public_path('/uploads');
                $extension = Input::file('image')->getClientOriginalExtension();
                $fileName = date("Y-m-d_H-i-s").'_'.rand(11111, 99999) . '.' . $extension;
                Input::file('image')->move($destinationPath, $fileName);
            } else {
                
                return redirect()->back()->withInput();
            }
            $coupon = new Coupon;
            $coupon->coupon_code = $request->coupon_code;
            $coupon->coupon_name = $request->coupon_name;
            $coupon->coupon_type = $request->coupon_type;
            $coupon->start_date = $request->start_date;
            $coupon->end_date = $request->end_date;
            $coupon->end_date = $request->end_date;
            $coupon->discount_type = $request->discount_type;
            $coupon->cash_back_type = $request->cash_back_type;
            $coupon->flat_discount = $request->flat_discount;
            $coupon->per_discount = $request->per_discount;
            $coupon->max_discount = $request->max_discount;
            $coupon->flat_cash_back = $request->flat_cash_back;
            $coupon->per_cash_back = $request->per_cash_back;
            $coupon->max_cash_back = $request->max_cash_back;
            $coupon->gift_name = $request->gift_name;
            $coupon->usage_limit = $request->usage_limit;
            $coupon->action = $request->action;
            $coupon->operator = $request->operator;
            $coupon->payment = $request->payment;
            $coupon->description = $request->description;
            $coupon->image = $fileName;
            $coupon->created_by = Auth::id();
            $coupon->save();

            return redirect('coupon');
        }
    }

    public function edit($id)
    {
    	$coupon = Coupon::find($id);
    	
    	return view('coupon.edit', compact('coupon'));
    }

    public function update(Request $request, $id)
    {
    	$coupon = Coupon::find($id);
    	$input = Input::all();
	    $rules = [
	    	'coupon_code' => 'required',
	    	'coupon_name' => 'required',
	    	'coupon_type' => 'required'	
	    ];

	    $messages = [];
	    
    	$validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
        	
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $coupon->coupon_code = $request->coupon_code;
        $coupon->coupon_name = $request->coupon_name;
        $coupon->coupon_type = $request->coupon_type;
        $coupon->start_date = $request->start_date;
        $coupon->end_date = $request->end_date;
        $coupon->discount_in_perc = $request->discount_in_perc;
        $coupon->max_discount = $request->max_discount;
        $coupon->cash_back_in_perc = $request->cash_back_in_perc;
        $coupon->max_cash_back = $request->max_cash_back;
        $coupon->gift_name = $request->gift_name;
        $coupon->usage_limit = $request->usage_limit;
        $coupon->updated_by = Auth::id();
        $coupon->save();

    	return redirect('coupon');
    }

    public function discountType() 
    {
        return view('coupon.discount_type');
    }

    public function cashBackType() 
    {
        return view('coupon.cash_back_type');
    }
}
