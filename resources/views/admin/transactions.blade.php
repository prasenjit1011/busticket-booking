@extends('admin.layout.layout')
@section('content')

    <!-- partial -->
    <div class="page-content-wrapper">
        <div class="page-content-wrapper-inner">
            <div class="viewport-header">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Transaction Management</a></li>
                    </ol>
                </nav>
            </div>
            <div class="content-viewport">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="grid">
                            <p class="grid-header">Manage Transactions</p>
                            <div class="item-wrapper">
                                <div class="table-responsive">
                                    <table  class="data-table table table-striped">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Order Id</th>
                                            <th>Currency</th>
                                            <th>Amount</th>
                                            <th>Status</th>
                                            <th>Payment Mode</th>
                                            <th>Date</th>
                                            <th>Status Message</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($transactions as $transaction)
                                            <tr>
                                                <th>
                                                    @if (is_null($transaction->userDet))
                                                        N/A
                                                    @else
                                                        {{ $transaction->userDet->name }}
                                                    @endif
                                                </th>
                                                <td>{{ $transaction->order_id }}</td>
                                                <td>{{ $transaction->currency }}</td>
                                                <td>{{  $transaction->amount }}</td>
                                                <td>{{ $transaction->status }}</td>
                                                <td>{{ $transaction->payment_mode }}</td>
                                                <td>{{ $transaction->created_at }}</td>
                                                <td>{{ $transaction->status_message }}</td>


                                            </tr>
                                        @endforeach


                                        </tbody>
                                    </table>

                                    <div class="row">
                                        <div class="col-sm-4 hidden-xs">
                                        </div>
                                        <div class="col-sm-4 text-center"> <small class="text-muted inline m-t-sm m-b-sm">showing
                                                {{($transactions->currentpage()-1)*$transactions->perpage()+1}} - {{$transactions->currentpage()*$transactions->perpage()}} of {{$transactions->total()}} items</small> </div>
                                        <div class="col-sm-4 text-right text-center-xs">
                                            {{ $transactions->links("pagination::bootstrap-4") }}
                                        </div>
                                    </div>
                                </div>


                            </div>

                        </div>

                    </div>


                    </footer>
                </div>
            </div>
        </div>
@endsection