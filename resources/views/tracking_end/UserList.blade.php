@extends('layouts.system')
@section('content')
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{url('/admin/trackadd')}}">Manage Users/List</a>
            <i class="fa fa-circle"></i>
        </li>

    </ul>

</div>
<h1 class="page-title"> User List Page
    <small>You can see all users.</small>
</h1>

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase">List</span>
                </div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_1">
                    <thead>
                        <tr>
                         <th> No </th>
                         <th> Name </th>
                         <th> Email </th>
                         <th> Address </th>
                         <th> created_date </th>

                     </tr>
                 </thead>
                 <tbody>
                    <?php $no = 1;?>
                    @foreach($user_data as $data)
                    <tr>
                        <td><?php echo $no++;?></td>
                        <td> {{$data->name}} </td>
                        <td> {{$data->email}} </td>
                        <td> {{$data->address}} </td>
                        <td> {{$data->created_at}} </td>
                    </tr>
                    @endforeach  
                </tbody>
            </table>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>


@endsection
