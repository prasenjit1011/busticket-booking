@extends('blades.base')

{{-- @section('page title', 'Index') --}}

@section('css')
<link rel="stylesheet" href="{{ asset('css/custom.css')}}">
@endsection

@section('innerbanner')

<!-- innerbanner -->    

<div class="banner">
	<s-banner></s-banner>
</div>

<!-- //innerbanner -->

@endsection

@section('breadcrumbs')
@endsection

@section('horizontal tab')

@endsection

@section('vertical tab')

<!-- breadcrumbs -->
	<div class="w3layouts-breadcrumbs text-center">
		<div class="container">
			<span class="agile-breadcrumbs"><a href="/"><i class="fa fa-home home_1"></i></a> / <span>Terms and Conditions</span></span>
		</div>
	</div>
	<!-- //breadcrumbs -->

	<!-- Terms of use-section -->
	<div class="agile-terms w3layouts-content">
		<section class="terms-of-use">			
			<!-- terms -->
			<div class="terms">
				<div class="container">
                    <h3 class="w3-head">Terms and Conditions</h3>



					<p>In using this website you are deemed to have read and agreed to the following terms and conditions:</p>
					<p>The following terminology applies to these Terms and Conditions, Privacy Statement and Disclaimer Notice and any or all Agreements: "Client", "You" and "Your" refers to you, the person accessing this website and accepting the Company's terms and conditions. "The Company", "Ourselves", "We" and "Us", refers to [ZAPWALLET.IN]. "Party", "Parties" refers to both the Client and ourselves, or either the Client or ourselves. All terms refer to the offer, acceptance and consideration of payment necessary to undertake the process of our assistance to the Client in the most appropriate manner. Any use of the above terminology or other words in the singular, plural, capitalization and/or he/she or they, are taken as interchangeable and therefore as referring to same. By becoming a registered user of [ZAPWALLET.IN] and our affiliate sites, you agree to be contacted by [ZAPWALLET.IN] and our affiliates via communication modes including but not limited to email, SMS, MMS, and telephonic calls.</p>
					<p>You agree that [ZAPWALLET.IN] may, in its sole discretion and without prior notice, terminate your access to the Website and block your future access to the Website if [ZAPWALLET.IN] determines that you have violated the terms of these Terms of Use or any other Agreement(s). You also agree that any violation by You of the Agreement(s) will cause irreparable harm to [ZAPWALLET.IN], for which monetary damages may be inadequate, and You consent to [ZAPWALLET.IN] obtaining any injunctive or equitable relief that [ZAPWALLET.IN] deems necessary or appropriate in such circumstances. These remedies are in addition to any other remedies [ZAPWALLET.IN] may have at law or in equity.</p>
					<p>[ZAPWALLET.IN] reserves the right to refuse access to use the Services offered at the Website to new Users or to terminate access granted to existing Users at any time without according any reasons for doing so.</p>


					<h6>Payment</h6>
					<p>We accept Visa and Master Credit Card, Debit Cards and Net banking as mode of payments which are processed through our payment gateway partner [CC Avenue]. No information pertaining to payment is shared with us as you fill all the payment related information on your bank's site.</p>


					<h6>Recharge</h6>
					<p>[ZAPWALLET.IN] has tied up with a third party enabler for the recharge. [ZAPWALLET.IN] is not responsible for any delay, pricing or cancellation of recharge from mobile operators end. Though, best efforts are made to keep the service above acceptance levels.</p>
					<p>User is solely responsible for the selection of mobile operator and the recharge amount. [ZAPWALLET.IN] is not responsible for talk-time given against recharge done through [ZAPWALLET.IN].in as this is purely at mobile operator's disposal.</p>
					<p>Consent to use Contact Information</p>
					<p>You agree that we may collect, use and share certain information regarding the contacts contained in your device’s phone book (“Contact Information”) for promotional and marketing purpose and also share it with any third party in accordance with the Privacy Policy. By allowing Contact Information to be collected, you give [ZAPWALLET.IN] a right to use that Contact Information as a part of the service provided by [ZAPWALLET.IN] and you guarantee that you have any and all permissions required to share such Contact Information with us.</p>


					<h6>Refunds</h6>
					<p>All sales are final with no refund or exchange permitted. You are responsible for the payment of the mobile number or DTH account number or bill payment and all charges that result from these payments. We shall not be responsible for any payment for an incorrect mobile number or DTH account number or bill payment number. In case, money has been charged to your card or bank account or pre-paid payment instrument and a payment/service is not delivered by the service provider within 24 hours of your completion of the transaction then you may inform us regarding the same by sending an email to [care@zapwallet.in]. Please include in the email the following details â)the mobile number or DTH account number or bill payment number, service provider name, payment value, transaction date and order number. We shall investigate such incidents and if it is found that money was indeed charged to your card or bank account or pre-paid payment instrument without delivery of the payment/service then you will be refunded the money within 21 working days from the date of receipt of your email. All refunds will be credited to your card or bank account or the [ZAPWALLET.IN] Balance. We shall have the sole discretion to determine the mode of reversal from the above mentioned options.</p>


					<h6>Coupon Redemption</h6>
					<p>Coupon redemption is purely subjected to standard and specified terms and conditions mentioned by the respective retailer. Coupons are issued on behalf of the respective retailer. Hence, any damages, injuries and losses incurred by the end user by using coupon is not the responsibility of [ZAPWALLET.IN].</p>


					<h6>Exclusions and Limitations</h6>
					<p>The information on this web site is provided on an "as is" basis. To the fullest extent permitted by law, this Company:</p>
					<p>excludes all representations and warranties relating to this website and its contents or which is or may be provided by any affiliates or any other third party, including in relation to any inaccuracies or omissions in this website and/or the Company's literature; and excludes all liability for damages arising out of or in connection with your use of this website. This includes, without limitation, direct loss, loss of business or profits (whether or not the loss of such profits was foreseeable, arose in the normal course of things or you have advised this Company of the possibility of such potential loss), damage caused to your computer, computer software, systems and programs and the data thereon or any other direct or indirect, consequential and incidental damages.</p>


					<h6>Limitation of Liability</h6>
					<p>All transactions on the [ZAPWALLET.IN] website, monetary and otherwise, are non-refundable, non-exchangeable and non-reversible, save and except in the event of proven gross negligence on the part of [ZAPWALLET.IN] or its representatives, in which event the user agrees that the user shall only be entitled to a refund of the amount actually paid by the user and actually received by [ZAPWALLET.IN] with respect to the recharge done by the user.</p>
					<p>This limitation applies whether the alleged liability is based on contract, tort, negligence, strict liability, or any other basis, even if [ZAPWALLET.IN] has been advised of the possibility of such damage. Because some jurisdictions do not allow the exclusion or limitation of incidental or consequential damages, [ZAPWALLET.IN] liability in such jurisdictions shall be limited to the extent of the amount actually paid for by any user while availing any of the services on the [ZAPWALLET.IN] website.</p>


					<h6>Modification of These Terms and Conditions of Use</h6>
					<p>The Company reserves the right to change these conditions from time to time as it sees fit and your continued use of the site will signify your acceptance of any adjustment to these terms. User is required to check terms and conditions at every visit. No notification communication will be done subject to change in terms and conditions.</p>


					<h6>Force Majeure</h6>
					<p>Neither party shall be liable to the other for any failure to perform any obligation under any Agreement which is due to an event beyond the control of such party including but not limited to any Act of god, terrorism, war, Political insurgence, insurrection, riot, civil unrest, act of civil or military authority, uprising, earthquake, flood or any other natural or man-made eventuality outside of our control, which causes the termination of an agreement or contract entered into, nor which could have been reasonably foreseen. Any Party affected by such event shall forthwith inform the other Party of the same and shall use all reasonable endeavours to comply with the terms and conditions of any Agreement contained herein.</p>


					<h6>General Provision</h6>
					<p>All the terms and conditions between you and [ZAPWALLET.IN] shall be governed by the law of India. By visiting [ZAPWALLET.IN], you agree to the laws. Any dispute of any sort that might arise between you and [ZAPWALLET.IN] or its affiliates is subject to Chennai jurisdiction only.</p>

				</div>
			</div>
			<!-- /terms -->
		</section>
		</div>
		<!-- //Terms of use-section -->


@endsection

@section('tab title')
    Plan
@endsection