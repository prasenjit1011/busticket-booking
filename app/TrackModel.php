<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrackModel extends Model
{
    protected $fillable = [
        'tracking_no', 'destination', 's_company_name', 's_location', 'r_name', 'r_adress', 'r_contact', 'sh_no', 'sh_cod_amount', 'sh_cus_value', 'sh_loc', 'created_by', 'weight', 'good_des'
    ];
}
