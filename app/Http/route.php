<?php 

    Route::get('/', function(){
       return redirect('/home');
    });
    
    Route::get('login', 'LoginController@getLogin');
    Route::get('register', 'RegisterController@getRegister');
    Route::get('verifyOtp', [
        'middleware' => 'checkSession',
        'uses' => 'userController@verifyOtp'
    ]);
