<div class="w3-subscribe agileits-w3layouts">

    <div class="container">

        <div class="col-md-6 social-icons w3-agile-icons">

            <h4>Join Us</h4>

            <ul>
                <li><a href="#" class="fa fa-facebook sicon facebook"> </a></li>
                <li><a href="#" class="fa fa-twitter sicon twitter"> </a></li>
                <li><a class="fa fa-whatsapp sicon whatsapp" style=" background-color:#1eeb1e"  href="https://chat.whatsapp.com/invite/GvKV1tcvQzsEgl5FK8R1Xe"></a></li>
            </ul>

        </div>

        <div class="col-md-6 w3-agile-subscribe-right">

            <h3 class="w3ls-title">Subscribe to Our <br><span>Newsletter</span></h3>

            <form action="#" method="post">

                <input type="email" name="email" placeholder="Enter your Email..." required="">

                <input type="submit" value="Subscribe">

                <div class="clearfix"></div>

            </form>

        </div>

        <div class="clearfix"></div>

    </div>

</div>