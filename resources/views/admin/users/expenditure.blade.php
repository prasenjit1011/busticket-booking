@extends('admin.layout.layout')
@section('content')

    <!-- partial -->
    <div class="page-content-wrapper">
        <div class="page-content-wrapper-inner">
            <div class="viewport-header">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">User Expenditures</a></li>
                    </ol>
                </nav>
            </div>
            <div class="content-viewport">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="grid">
                            <p class="grid-header">User Expenditures</p>
                            <div class="item-wrapper">
                                <div class="table-responsive">
                                    @include('admin.partials.info')
                                    <table  class="data-table table table-striped" id="transactionsTable">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>User Id</th>
                                            <th>Username</th>
                                            <th>Email</th>
                                            <th>User Phone</th>
                                            <th>Money Added By user</th>
                                            <th>Money Added By Admin</th>
                                            <th>Total Earnings</th>
                                            <th>Total Recharge Made</th>
                                            <th>Wallet Balance</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($users as $user)
                                            <tr>
                                                <th>{{  $user->name }}</th>
                                                <td>{{ $user->id }}</td>
                                                <td>{{  $user->name }}</td>
                                                <td>{{  $user->email }}</td>
                                                <td>{{  $user->mobile }}</td>
                                                <td>{{ $user->successTransactions->sum('amount') }}</td>
                                                <td>{{ $user->moneyAddedByAdmin->sum('amount') }}</td>
                                                <td>{{ $user->successTransactions->sum('amount') + $user->moneyAddedByAdmin->sum('amount') }}</td>
                                                <td>{{ $user->postPaidRecharge->sum('amount') + $user->mobileRecharge->sum('amount') + $user->dthRecharge->sum('amount') }}</td>
                                                <td>{{  $user->wallet }}</td>

                                            </tr>
                                        @endforeach


                                        </tbody>
                                    </table>
                                </div>


                            </div>

                        </div>

                    </div>


                    </footer>
                </div>
            </div>
        </div>
@endsection