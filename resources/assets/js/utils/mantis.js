import mantisApi from '../apis/mantis'
import {MANTIS_CONFIG} from '../consts/service'

class Mantis {

    constructor(getListCityCompletion) {
        this.getListCityCompletion = getListCityCompletion
    }

    authentication() {
        mantisApi.auth(MANTIS_CONFIG)
    }

    getCityList(callback) {
        
        mantisApi.cityList(callback)
    }
}

export default Mantis