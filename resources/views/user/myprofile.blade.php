@extends('blades.base')

{{-- @section('page title', 'Index') --}}

@section('breadcrumbs')
@endsection

@section('horizontal tab')

@endsection
@section('css')
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="{{ asset('css/travel.css')}}">
    <link rel="stylesheet" href="{{ asset('css/custom.css')}}">
    <link rel="stylesheet" href="{{ asset('css/tool-tip.css')}}">
    <link rel="stylesheet" href="{{ asset('css/myprofile-style.css')}}">
@endsection

@section('vertical tab')
    <div class="container" style="padding-top: 100px">
        <h2>Profile</h2>
        <div class="profile">
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <div class="text-center">
                        <img class="avatar" src="/images/profile-images/avatar.png" alt="">
                        <p class="username">{{ $user->name }}</p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3 h-100">
                    <ul class="nav nav-pills nav-stacked p-tabs">
                        <li class="active"><a data-toggle="tab" href="#profile">Profile</a></li>
                        <li><a data-toggle="tab" href="#security1">Security</a></li>
                        <li><a href="/myprofile/wallet">Wallet</a></li>
                    </ul>
                </div>
                <div class="col-md-9">
                    <div class="tab-content">
                        <div id="profile" class="tab-pane fade in active">
                            <div class="p-container">
                                <form class="form-horizontal" action="">
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="email">Your Name:</label>
                                        <div class="col-sm-10 ">
                                            <div class="under-line">
                                                <p class="text-info" style="height: 32px">{{ $user->name }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="email">Your Email:</label>
                                        <div class="col-sm-10 ">
                                            <div class="under-line">
                                                <p class="text-info" style="height: 32px">{{ $user->email }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="email">Phone Number:</label>
                                        <div class="col-sm-10 ">
                                            <div class="under-line">
                                                <p class="text-info" style="height: 32px">{{$user->mobile}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="email">Country:</label>
                                        <div class="col-sm-10 ">
                                            <div class="under-line">
                                                <p class="text-info" style="height: 32px">{{$user->country}} </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="email">City:</label>
                                        <div class="col-sm-10 ">
                                            <div class="under-line">
                                                <p class="text-info" style="height: 32px">{{$user->city}} </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="email">Zip code:</label>
                                        <div class="col-sm-10 ">
                                            <div class="under-line">
                                                <p class="text-info" style="height: 32px">{{$user->zip_code}} </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="email">State:</label>
                                        <div class="col-sm-10 ">
                                            <div class="under-line">
                                                <p class="text-info" style="height: 32px">{{$user->state}} </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="email">Date of birth:</label>
                                        <div class="col-sm-10 ">
                                            <div class="under-line">
                                                <p class="text-info" style="height: 32px">{{$user->date_of_birth}} </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="email">Your Address:</label>
                                        <div class="col-sm-10 ">
                                            <div class="under-line">
                                                <p class="text-info" style="height: 32px">{{$user->address}} </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-10" style="padding-left: 230px">
                                            <button class="btn btn-primary"><a href="">Edit Your Profile</a></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div id="security" class="tab-pane fade">
                            <h3>Menu 1</h3>
                            <p>Some content in menu 1.</p>
                        </div>
                        <div id="wallet" class="tab-pane fade">
                            <h3>Menu 2</h3>
                            <p>Some content in menu 2.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
@endsection

@section('tab title')
    Plan
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {

            //Vertical Tab
            $('#parentVerticalTab').easyResponsiveTabs({
                type: 'vertical', //Types: default, vertical, accordion
                width: 'auto', //auto or any width like 600px
                fit: true, // 100% fit in a container
                closed: 'accordion', // Start closed if in accordion view
                tabidentify: 'hor_1', // The tab groups identifier
                activate: function(event) { // Callback function if tab is switched
                    var $tab = $(this);
                    var $info = $('#nested-tabInfo2');
                    var $name = $('span', $info);
                    $name.text($tab.text());
                    $info.show();
                }
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $("#tab2").hide();
            $("#tab3").hide();
            $("#tab4").hide();
            $(".tabs-menu a").click(function(event){
                event.preventDefault();
                var tab=$(this).attr("href");
                $(".tab-grid").not(tab).css("display","none");
                $(tab).fadeIn("slow");
            });
        });
    </script>
    <script src="/js/jquery-ui.js"></script>
    <script>
        $(function() {
            $( "#datepicker,#datepicker1" ).datepicker();
        });
    </script>
@endsection





