<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <title>Zap Wallet - Admin Dashboard</title>

    <link rel="stylesheet" href="{{ asset('admins/vendors/iconfonts/mdi/css/materialdesignicons.css') }}">
    <link rel="stylesheet" href="{{ asset('admins/vendors/css/vendor.addons.css') }}">

    <link rel="stylesheet" href="{{ asset('admins/vendors/iconfonts/flag-icon-css/css/flag-icon.min.css' ) }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <link rel="stylesheet" href="{{ asset('admins/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('admins/css/datatable.css') }}">

    <link rel="shortcut icon" href="{{ asset('images/logo.png') }}">
</head>

<body class="header-fixed">



<nav class="t-header">
    <div class="t-header-brand-wrapper">
        <a href="{{ route('admin.dashboard') }}"><img class="logo" src="{{ asset('images/logo.png') }}" alt=""> <img class="logo-mini" src="{{ asset('images/logo.png') }}" alt=""> </a>
        <button class="t-header-toggler t-header-desk-toggler d-none d-lg-block">
            <svg class="logo" viewBox="0 0 200 200">
                <path class="top" d="
                M 40, 80
                C 40, 80 120, 80 140, 80
                C180, 80 180, 20  90, 80
                C 60,100  30,120  30,120
              "></path>
                <path class="middle" d="
                M 40,100
                L140,100
              "></path>
                <path class="bottom" d="
                M 40,120
                C 40,120 120,120 140,120
                C180,120 180,180  90,120
                C 60,100  30, 80  30, 80
              "></path>
            </svg>
        </button>
    </div>
    <div class="t-header-content-wrapper">
        <div class="t-header-content">
            <button class="t-header-toggler t-header-mobile-toggler d-block d-lg-none"><i class="fa fa-bars"></i></button>
            <form action="#" class="t-header-search-box">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text"><i class="fa fa-search"></i></div>
                    </div>
                    <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="Search" autocomplete="off">
                </div>
            </form>
            <ul class="nav ml-auto">
                <li class="nav-item dropdown"><a class="nav-link" href="#" id="notificationDropdown" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-bell"></i></a>
                    <div class="dropdown-menu navbar-dropdown dropdown-menu-right" aria-labelledby="notificationDropdown">
                        <div class="dropdown-header">
                            <h6 class="dropdown-title">Notifications</h6>
                            <p class="dropdown-title-text">You have 4 unread notification</p>
                        </div>
                        <div class="dropdown-body">
                            <div class="dropdown-list">
                                <div class="icon-wrapper rounded-circle bg-inverse-primary text-primary"><i class="mdi mdi-alert"></i></div>
                                <div class="content-wrapper"><small class="name">Storage Full</small> <small class="content-text">Server storage almost full</small></div>
                            </div>
                            <div class="dropdown-list">
                                <div class="icon-wrapper rounded-circle bg-inverse-success text-success"><i class="mdi mdi-cloud-upload"></i></div>
                                <div class="content-wrapper"><small class="name">Upload Completed</small> <small class="content-text">3 Files uploded successfully</small></div>
                            </div>
                            <div class="dropdown-list">
                                <div class="icon-wrapper rounded-circle bg-inverse-warning text-warning"><i class="mdi mdi-security"></i></div>
                                <div class="content-wrapper"><small class="name">Authentication Required</small> <small class="content-text">Please verify your password to continue using cloud services</small></div>
                            </div>
                        </div>
                        <div class="dropdown-footer"><a href="#">View All</a></div>
                    </div>
                </li>
                <li class="nav-item dropdown"><a class="nav-link" href="#" id="messageDropdown" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-comment"></i> <span class="notification-indicator notification-indicator-primary notification-indicator-ripple"></span></a>
                    <div class="dropdown-menu navbar-dropdown dropdown-menu-right" aria-labelledby="messageDropdown">
                        <div class="dropdown-header">
                            <h6 class="dropdown-title">Messages</h6>
                            <p class="dropdown-title-text">You have 4 unread messages</p>
                        </div>
                        <div class="dropdown-body">
                            <div class="dropdown-list">
                                <div class="image-wrapper"><img class="profile-img" src="images/profile/male/image_1.png" alt="profile image">
                                    <div class="status-indicator rounded-indicator bg-success"></div>
                                </div>
                                <div class="content-wrapper"><small class="name">Clifford Gordon</small> <small class="content-text">Lorem ipsum dolor sit amet.</small></div>
                            </div>
                            <div class="dropdown-list">
                                <div class="image-wrapper"><img class="profile-img" src="images/profile/female/image_2.png" alt="profile image">
                                    <div class="status-indicator rounded-indicator bg-success"></div>
                                </div>
                                <div class="content-wrapper"><small class="name">Rachel Doyle</small> <small class="content-text">Lorem ipsum dolor sit amet.</small></div>
                            </div>
                            <div class="dropdown-list">
                                <div class="image-wrapper"><img class="profile-img" src="images/profile/male/image_3.png" alt="profile image">
                                    <div class="status-indicator rounded-indicator bg-warning"></div>
                                </div>
                                <div class="content-wrapper"><small class="name">Lewis Guzman</small> <small class="content-text">Lorem ipsum dolor sit amet.</small></div>
                            </div>
                        </div>
                        <div class="dropdown-footer"><a href="#">View All</a></div>
                    </div>
                </li>

            </ul>
        </div>
    </div>
</nav>
<!-- partial -->
<div class="page-body">
    <!-- partial:partials/_sidebar.html -->
    <div class="sidebar">
        <ul class="navigation-menu">
            <li class="nav-category-divider">MAIN</li>

            <li><a href="{{ route('admin.dashboard') }}" ><span class="link-title"> Dashboard</span> <i class="fa fa-home"></i></a>
            <li><a href="{{ route('admin.users') }}" ><span class="link-title"> User Management</span> <i class="fa fa-users"></i></a></li>
            <li><a href="{{ route('admin.users.expenditures') }}" ><span class="link-title"> User Expenditure</span> <i class="fa fa-users"></i></a></li>
            <li><a href="#transactions" data-toggle="collapse" aria-expanded="false"><span class="link-title"> Transactions Management</span> <i class="fa fa-cog"></i></a>
                <ul class="collapse navigation-submenu" id="transactions">
                    <li><a href="{{ route('admin.walletTransactions') }}">Wallet Transactions</a></li>
                    <li><a href="{{ route('admin.dhtTransactions') }}">Dth Transactions</a></li>
                    <li><a href="{{ route('admin.mobileTransactions') }}">Mobile Transactions</a></li>
                    <li><a href="{{ route('admin.postPaidTransactions') }}">Post Paid Transactions</a></li>
                </ul>
            </li>
            <li><a href="#ui-elements" data-toggle="collapse" aria-expanded="false"><span class="link-title"> Admin Management</span> <i class="fa fa-cog"></i></a>
                <ul class="collapse navigation-submenu" id="ui-elements">
                    <li><a href="">Users</a></li>
                    <li><a href="">New User</a></li>
                    <li><a href="">Badges</a></li>

                </ul>
            </li>

        <div class="sidebar_footer">
            <div class="user-account">
                <div class="user-profile-item-tittle">Settings</div>
               <a class="user-profile-item" href="#"><i class="fa fa-user-circle"></i> Profile</a>
                <a class="user-profile-item" href="#"><i class="fa fa-cog"></i> Account Settings</a>
                <a class="btn btn-primary btn-logout" href="{{ route('logout') }}">Logout</a></div>
            <div class="btn-group admin-access-level">
                <div class="avatar"><img class="profile-img" src="{{ asset('admins/images/profile/male/image_1.png') }}" alt=""></div>
                <div class="user-type-wrapper">
                    <p class="user_name">{{ Auth::user()->name }}</p>
                    <div class="d-flex align-items-center">
                        <div class="status-indicator small rounded-indicator bg-success"></div><small class="user_access_level">Admin</small></div>
                </div><i class="fa fa-arrow-right"></i></div>
        </div>
    </div>
    @yield('content')
<footer class="footer">
    <div class="row">
        <div class="col-sm-6 text-center text-sm-right order-sm-1">
            <ul class="text-gray">
                <li><a href="#">Terms of use</a></li>
                <li><a href="#">Privacy Policy</a></li>
            </ul>
        </div>
        <div class="col-sm-6 text-center text-sm-left mt-3 mt-sm-0"><small class="text-muted d-block">Copyright © 2019 Zapwallet. All rights reserved</small></div>
    </div>
</footer>
<!-- partial -->
</div>
<!-- page content ends -->
</div>
<!--page body ends -->
<!-- SCRIPT LOADING START FORM HERE /////////////-->
<!-- plugins:js -->
<script src="{{ asset('admins/vendors/js/core.js') }}"></script>
{{-- <script src="https://code.jquery.com/jquery-3.3.1.js"></script> --}}
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="{{ asset('admins/vendors/js/vendor.addons.js') }}"></script>
<!-- endinject -->
<!-- Vendor Js For This Page Ends-->
<script src="{{ asset('admins/vendors/chartjs/Chart.min.js') }}"></script>
<!-- Vendor Js For This Page Ends-->
<script src="{{ asset('admins/js/script.js') }}"></script>
</body>
<script>
    $(document).ready(function() {
        $('#transactionsTable').DataTable(
             {
              "ordering": false
            });
    } );
    </script>
</html>