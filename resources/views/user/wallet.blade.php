@extends('blades.base')

{{-- @section('page title', 'Index') --}}

@section('breadcrumbs')
@endsection

@section('horizontal tab')

@endsection
@section('css')
    <link rel="stylesheet" href="/css/jquery-ui.css">
    <link rel="stylesheet" href="{{ asset('css/travel.css')}}">
    <link rel="stylesheet" href="{{ asset('css/custom.css')}}">
    <link rel="stylesheet" href="{{ asset('css/tool-tip.css')}}">
    <link rel="stylesheet" href="{{ asset('css/myprofile-style.css')}}">
@endsection

@section('vertical tab')
    <div class="container" style="padding-top: 100px">
        <h2>Wallet</h2>
        <div class="profile">
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <div class="text-center">
                        <img class="avatar" src="/images/profile-images/avatar.png" alt="">
                        <p class="username">{{ $user->name }}</p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3 h-100">
                    <ul class="nav nav-pills nav-stacked p-tabs">
                        <li @if(\Illuminate\Support\Facades\Request::path()=='myprofile')  class="active" @endif><a href="/myprofile" >Profile</a></li>
                        <li><a data-toggle="tab" href="#security1">Security</a></li>
                        <li @if(\Illuminate\Support\Facades\Request::path()=='myprofile/wallet')  class="active" @endif><a href="/myprofile/wallet">Wallet</a></li>
                    </ul>
                </div>
                <div class="col-md-9">
                    <div class="tab-content">
                        <div id="profile" class="tab-pane fade in active">
                            <div class="p-container">
                                <form class="form-horizontal" action="">
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="email">ZapWallet balance :</label>
                                        <div class="col-sm-10 ">
                                            <p class="text-info">{{ $user->wallet }}</p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="email">Zap Bucks :</label>
                                        <div class="col-sm-10 ">
                                            <p class="text-info">{{ $user->zapbucks }}</p>
                                        </div>
                                    </div>
                                    {{--<div class="form-group">
                                        <label class="control-label col-sm-2" for="email"><a href="">View details</a></label>
                                    </div>--}}
                                </form>
                            </div>
                        </div>
                        <div id="security" class="tab-pane fade">
                            <h3>Menu 1</h3>
                            <p>Some content in menu 1.</p>
                        </div>
                        <div id="wallet" class="tab-pane fade">
                            <h3>Menu 2</h3>
                            <p>Some content in menu 2.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="history" style="padding-bottom: 30px;">
            <div class="row space-top">
                <div class="col-md-12 col-lg-12 text-center">
                    <h4>
                        {{$rechargewallet->render()}}
                    </h4>
                </div>
            </div>
            <div class="row mt-20">
                @foreach($rechargewallet as $item)
                <div class="col-md-4 mt-20">
                    <div class="h-item">
                        <div class="row ">
                            <div class="col-sm-6 text-left">
                                <h4 class="h-i-title"> {{$item->created_at->format('d/m/Y')}}</h4>
                            </div>
                            <div class="col-sm-6 text-right">
                                <h4 class="text-id">{{$item->order_id}}</h4>
                            </div>
                        </div>
                        <div class="b-info ">
                            <div class="row">
                                <div class="col-sm-6 text-left">
                                    <span class="i-title">Total Bill</span>
                                </div>
                                <div class="col-sm-6 text-right">
                                    <span class="money">{{$item->currency}} {{$item->amount}}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 text-left">
                                    <span class="i-title">Payment</span>
                                </div>
                                <div class="col-sm-6 text-right">
                                    <span class="i-info">{{$item->payment_mode}}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 text-left">
                                    <span class="i-title">Order status</span>
                                </div>
                                <div class="col-sm-6 text-right">
                                    <span class="i-info">{{$item->order_status}}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 text-left">
                                    <span class="i-title">Status message</span>
                                </div>
                                <div class="col-sm-6 text-right">
                                    <span class="i-info">{{$item->status_message}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <div class="review">

        </div>
    </div>
@endsection

@section('tab title')
    Plan
@endsection

@section('scripts')
    {{--<script type="text/javascript">
        $(document).ready(function() {

            //Vertical Tab
            $('#parentVerticalTab').easyResponsiveTabs({
                type: 'vertical', //Types: default, vertical, accordion
                width: 'auto', //auto or any width like 600px
                fit: true, // 100% fit in a container
                closed: 'accordion', // Start closed if in accordion view
                tabidentify: 'hor_1', // The tab groups identifier
                activate: function(event) { // Callback function if tab is switched
                    var $tab = $(this);
                    var $info = $('#nested-tabInfo2');
                    var $name = $('span', $info);
                    $name.text($tab.text());
                    $info.show();
                }
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $("#tab2").hide();
            $("#tab3").hide();
            $("#tab4").hide();
            $(".tabs-menu a").click(function(event){
                event.preventDefault();
                var tab=$(this).attr("href");
                $(".tab-grid").not(tab).css("display","none");
                $(tab).fadeIn("slow");
            });
        });
    </script>
    <script src="/js/jquery-ui.js"></script>
    <script>
        $(function() {
            $( "#datepicker,#datepicker1" ).datepicker();
        });
    </script>--}}
@endsection



