@extends('layouts.bbps')

{{--@section('page title', 'Zapwallet -  Mobile Recharge, DTH Recharge & Bus Tickets.')--}}

@section('innerbanner')
<!-- innerbanner -->	
{{-- <div class="agileits-inner-banner">
    
</div> --}}
<!-- //innerbanner -->
@endsection

@section('breadcrumbs')
@endsection

@section('horizontal tab')

@endsection
@section('css')
<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="css/jquery-ui.css">
<link rel="stylesheet" href="{{ asset('css/travel.css')}}">
<link rel="stylesheet" href="{{ asset('css/custom.css')}}">
<link rel="stylesheet" href="{{ asset('css/tool-tip.css')}}">
@endsection

@section('vertical tab')
    @if (session('status'))
        <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>            {{ session('status') }}
        </div>
    @endif
  
    @include('admin.partials.info')
<!--Vertical Tab-->
<div class="categories-section main-grid-border" id="mobilew3layouts">
    <div class="container">
        
        <hr>
        <section id="search-biller">
            <h4>SEARCH BILLER</h4>
            <div class="form">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="exampleCheck1" style="width: auto;">
                                    <label class="form-check-label" for="exampleCheck1">Biller Category</label>
                                  </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="exampleCheck2" style="width: auto;">
                                    <label class="form-check-label" for="exampleCheck2">Coverage</label>
                                  </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="exampleCheck3" style="width: auto;">
                                    <label class="form-check-label" for="exampleCheck3">Biller Name</label>
                                  </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="exampleCheck4" style="width: auto;">
                                    <label class="form-check-label" for="exampleCheck4">Biller ID</label>
                                  </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Biller Category</label>
                                    <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="Enter email">
                                    <small id="emailHelp" class="form-text text-muted">Populated with all Active biller categories</small>
                                </div>

                                <div class="form-group">
                                    <label>Coverage</label>
                                    <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="Enter email">
                                    <small id="emailHelp" class="form-text text-muted">Populated with all State Names and National</small>
                                </div>

                                <div class="form-group">
                                    <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="Enter email">
                                    <small id="emailHelp" class="form-text text-muted">Populated with all City Names within State </small>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Biller Name</label>
                                    <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="Enter email">
                                    
                                </div>

                                <div class="form-group">
                                    <label>Biller ID</label>
                                    <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="Enter email">
                                    
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <a class="btn btn-success submit pull-right" href="/bbps/fetch-biller">SEARCH</a>
                    </div>
                    
                </div>
            </div>
        </section>

        <section id="fetch-biller" style="display: none;">
            <h4>FETCH BILL</h4>
            <div class="form">
                <div class="row">
                    <div class="col-lg-4"></div>
                    <div class="col-lg-4">
                        <div class="row">
                        
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Biller Name</label>
                                    <input type="text" class="form-control" value="Electric Board Nigam" placeholder="Enter biller name">
                                    
                                </div>

                                <div class="form-group">
                                    <label>Payee Mobile Number</label>
                                    <input type="text" class="form-control" value="9999999999" placeholder="Enter payee mobile number">
                                </div>

                                <div class="form-group">
                                    <label>Consumer Number</label>
                                    <input type="text" class="form-control" value="12345678" placeholder="Enter consumer number">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4"></div>
                    
                    <div class="col-lg-4"></div>
                    <div class="col-lg-4">
                        <button class="btn btn-success submit pull-right" id="proceed">Proceed</button>
                    </div>
                    <div class="col-lg-4"></div>
                    
                </div>
            </div>
        </section>

	</div>
</div>
	<!--Plug-in Initialisation-->
<bus-modal></bus-modal>
@endsection

@section('tab title')
    Plan
@endsection

@section('scripts')

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<script type="text/javascript">
    $(document).ready(function() {

        //Vertical Tab
        $('#parentVerticalTab').easyResponsiveTabs({
            type: 'vertical', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true, // 100% fit in a container
            closed: 'accordion', // Start closed if in accordion view
            tabidentify: 'hor_1', // The tab groups identifier
            activate: function(event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#nested-tabInfo2');
                var $name = $('span', $info);
                $name.text($tab.text());
                $info.show();
            }
        });
    });
</script>
<script>
    $(document).ready(function() {
        $("#tab2").hide();
        $("#tab3").hide();
        $("#tab4").hide();
        $(".tabs-menu a").click(function(event){
            event.preventDefault();
            var tab=$(this).attr("href");
            $(".tab-grid").not(tab).css("display","none");
            $(tab).fadeIn("slow");
        });
    });
</script>
<script src="/js/jquery-ui.js"></script>
<script>
    $(function() {
        $( "#datepicker,#datepicker1" ).datepicker();
    });
</script>

<script>
    $(document).ready(function(){
        $('#search').click(function() {
            console.log('working')
            $('#search-biller').hide(function(){
                $('#fetch-biller').show();
            });
        });

    });
</script>
@endsection

@section('header-right')
<div class=" header-right">
    <div class="banner">
        <s-banner></s-banner>
    </div>
</div>
@endsection
