@extends('blades.base')

{{-- @section('page title', 'Index') --}}

@section('css')
<link rel="stylesheet" href="{{ asset('css/custom.css')}}">
@endsection

@section('innerbanner')

<!-- innerbanner -->    

<div class="banner">
	<s-banner></s-banner>
</div>

<!-- //innerbanner -->

@endsection

@section('breadcrumbs')
@endsection

@section('horizontal tab')

@endsection

@section('vertical tab')
<!-- //innerbanner -->

<!-- breadcrumbs -->
	<div class="w3layouts-breadcrumbs text-center">
		<div class="container">
			<span class="agile-breadcrumbs"><a href="/"><i class="fa fa-home home_1"></i></a> / <span>Contact</span></span>
		</div>
	</div>
	<!-- //breadcrumbs -->
	<!-- Contact-page -->
	<!--contact-->
	 <div class="contact-section w3layouts-content">
	    <div class="container">
            <h3 class="w3-head">Contact Us</h3>
				<div class="contact-main">
					<div class="col-md-6 map">
						<p class="loc">Our Location</p>
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d387145.86654334463!2d-74.25818682528057!3d40.70531100753592!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2sNew+York%2C+NY%2C+USA!5e0!3m2!1sen!2sin!4v1493028309728" style="border:0" allowfullscreen></iframe>
					</div>
					<div class="col-md-6 contact-in">
						<p class="sed-para"> Contact Information</p>
                        <p class="para1">For Queries and details, Get in touch with us: <a href="/frontend/support">Help</a></p>
						<div class="more-address"> 

								<address>
								  <strong>Email</strong><br>
								  <a href="mailto:care@zapwallet.in">care@zapwallet.in</a>
							   </address>
						  </div>
					</div>
						<div class="clearfix"> </div>
			      </div>

	   </div>
     </div>
<!--//contact-->
<!-- //Contact-page -->

<!-- subscribe -->
@endsection

@section('tab title')
    Plan
@endsection