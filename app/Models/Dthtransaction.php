<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Dthtransaction extends Model
{
    protected $fillable = [
      'order_id',
      'currency',
      'operator',
      'amount',
      'mn'
    ];
    public function users (  ) {
        return $this->belongsTo('App\User');
    }

    public function userDet()
    {
        return $this->belongsTo('App\User','user_id');
    }
    public function op()
    {
        return $this->belongsTo('App\Models\Operator','operator','opcode');
    }
}
