@extends('blades.base')

{{-- @section('page title', 'Index') --}}

@section('css')
<link rel="stylesheet" href="{{ asset('css/custom.css')}}">
@endsection

@section('innerbanner')

<!-- innerbanner -->    

<div class="banner">
	<s-banner></s-banner>
</div>

<!-- //innerbanner -->

@endsection

@section('breadcrumbs')
@endsection

@section('horizontal tab')

@endsection

@section('vertical tab')

<!-- breadcrumbs -->
	<div class="w3layouts-breadcrumbs text-center">
		<div class="container">
			<span class="agile-breadcrumbs"><a href="/"><i class="fa fa-home home_1"></i></a> / <span>Feedback</span></span>
		</div>
	</div>
	<!-- //breadcrumbs -->
	<!-- Feedback -->
	<div class="w3ls-feedback w3layouts-content">
		<div class="container">
			<h3 class="w3-head">Feedback</h3>
			<div class="feed-back">
				<h3>Tell us what you think of us</h3>
				<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
				
				<div class="feed-back-form">
					<form action="{{ route('feedback_post') }}" method="POST">
						{{ csrf_field() }}
					<span>User Details</span>
							<input type="text" placeholder="First Name" name="firstname" required="required" />
							<input type="text" placeholder="Last Name" name="lastname" required="required" />
							<input type="email" placeholder="Email" name="email" required="required" />
							<input type="tel" placeholder="Phone No" name="phoneno" required="required" />
							<span>Is there anything you would like to tell us?</span>
							<textarea name="message" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Message...';}" required="">Message...</textarea>
							<input type="submit" value="Send Feedback">
						</form>
				</div>
			</div>
		</div>	
	</div>
	<!-- // Feedback -->
	
<!-- subscribe -->
@endsection

@section('tab title')
    Plan
@endsection