@extends('layouts.app_admin')

@section('content')

<div class="content-wrapper">
    
    <section class="content-header">
        <h1>
            Dashboard
        	<small>Preview page</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>
    
    <section class="content">
        <div class="row">
        	<div class="col-md-12">
            	<div class="box box-success">
                	<div class="box-header with-border">
                    	<h3 class="box-title">Dashboard</h3> 
                	</div>
                	<div class="box-body">
                    	
                    	
                    	<div class="col-md-4 col-sm-12 col-xs-12">
                            <div class="info-box bg-green">
                                <span class="info-box-icon"><i class="fa fa-hand-o-right"></i></span>
                                <div class="info-box-content">
                                    <center><span class="info-box-text" style="font-size: 18px;">Users</span></center>
                                    <center><span class="info-box-number" style="font-size: 39px;">{{ $userCount }}</span></center>
                                </div>
                            </div>
                        </div>
                    	
                    	
                    	
                    	
                	</div>
          		</div>
        	</div>
      	</div>
    </section>

</div>

@endsection
