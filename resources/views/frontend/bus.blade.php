@extends('blades.base')

{{-- @section('page title', 'Index') --}}

@section('innerbanner')
<!-- innerbanner -->    
<div class="agileits-inner-banner">
        
    </div>
<!-- //innerbanner -->
@endsection

@section('breadcrumbs')
@endsection

@section('horizontal tab')

@endsection

@section('vertical tab')


<!-- breadcrumbs -->
	<div class="w3layouts-breadcrumbs text-center">
		<div class="container">
			<span class="agile-breadcrumbs"><a href="/"><i class="fa fa-home home_1"></i></a> / <span>Bus</span></span>
		</div>
	</div>
	<!-- //breadcrumbs -->
	<!-- Bus -->
	<div class="agile-bus w3layouts-content">
     <div class="container">
	<div class="col-md-5 bann-info1">
		<i class="train-icon fa fa-bus"></i>
	</div>
         <div class="col-md-5 bann-info">
		<h3>Search for Bus Tickets</h3>
        <div class="book-a-ticket">
							<div class=" bann-info">
								<form action="bus-list.html" method="post">
									<div class="ban-top">
										<div class="bnr-left">
											<label class="inputLabel">From:</label>
											<input class="city" type="text" placeholder="Enter a city or local area" required="required" />
										</div>
										
										<div class="bnr-left">
											<label class="inputLabel">To:</label>
											<input class="city" type="text" placeholder="Enter a city or local area" required="required" />
										</div>
											<div class="clearfix"></div>
									</div>
									 <div class="ban-bottom">
											<div class="bnr-right">
												<label class="inputLabel">Date of Journey:</label>
				<input class="date" id="datepicker" type="text" placeholder="dd-mm-yyyy"  required="required" />
											</div>
											<div class="bnr-right">
												<label class="inputLabel">Date of Return<span class="opt">&nbsp;(Optional):</span></label>
				<input class="date" id="datepicker1" type="text" placeholder="dd-mm-yyyy" />
											</div>
												<div class="clearfix"></div>
												<!-- start-date-piker -->
												<link rel="stylesheet" href="css/jquery-ui.css">
												<script src="/js/jquery-ui.js"></script>
													<script>
														$(function() {
														$( "#datepicker,#datepicker1" ).datepicker();
														});
													</script>
											<!-- /End-date-piker -->
										</div>
									<div class="search">
									     <input type="submit" class="submit" value="Search">
									
									</div>
								</form>
							</div>
					</div>

	</div>
        

	
	<div class="clearfix"></div>
	</div>
</div>
<!-- //Bus -->

<!-- subscribe -->
@endsection

@section('tab title')
    Plan
@endsection