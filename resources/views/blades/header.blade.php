<header>

    <div class="container">
        <div class="logo">
            <h1><a href="/" class="h-logo"><img src="{{asset('images/logo2.png')}}" alt=""></a></h1>
        </div>
        <div class="w3layouts-login">
            @guest
                <a href="javascript:void(0)" id="singin-btn" class="btn auth-btn login">Signin</a>
            @else
                <li class="dropdown">
                    <a href="javascript:void(0)" id="signout-btn" data-toggle="dropdown" role="button" aria-expanded="false"
                       aria-haspopup="true" v-pre>
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu">
                        <li>
                            <a href="/myprofile">My Profile</a>
                        </li>

                        <!--<li>-->
                        <!--    <a href="/auto-recharge">Auto Recharge</a>-->
                        <!--</li>-->
                        <li>
                            <a href="/myhistory">My history</a>
                        </li>
                        <li>
                            <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                       document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>
            @endguest
        </div>

        <div class="clearfix"></div>
        <!--Login modal-->
        <div class="modal fade" id="signin-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            &times;
                        </button>
                        <div>
                            <h1 class="modal-title" id="myModalLabel"><a href="/" class="h-logo"><img src="{{asset('images/logo2.png')}}" alt=""></a></h1>
                        </div>
                    </div> -->
                         <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 extra-w3layouts" style="display: inline-flex;">
                                <div class="hidden-xs hidden-sm">
                                    <img src="/images/login_bg.png" alt="">
                                </div>
                                <div style="width: 100%;">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" style="margin: auto;width: 200px;display: flex;padding-top: 30px;">
                                        <li class="active" style="flex:1"><a href="#Login" data-toggle="tab" style="margin: 0;border-radius: 9px;border: 2px solid #FF7900;border-top-right-radius: 0;border-bottom-right-radius: 0;text-align: center;">Login</a></li>
                                        <li style="flex:1"><a href="#Registration" data-toggle="tab" style="margin: 0;border-radius: 9px;border: 2px solid #FF7900;border-top-left-radius: 0;border-bottom-left-radius: 0;">Register</a></li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content" style="padding-top: 20px;">
                                        <div class="tab-pane active" id="Login" style="padding-top:20px;">
                                            <form class="form-horizontal" method="POST" action="{{ route('loginAction') }}" id="form-login">
                                                {{ csrf_field() }}
                                                <div class="form-group">
                                                    <div class="col-sm-8 col-sm-offset-2">
                                                        <input type="email" placeholder="Email" id="loginemail" class="form-control" name="email"
                                                            value="{{ old('email') }}" required autofocus>
                                                            @if ($errors->has('email') && $not_reset_password)
                                                                <span class="help-block">
                                                                <strong>{{ $errors->first('email') }}</strong>
                                                            </span>
                                                            @endif
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-8 col-sm-offset-2">
                                                        <input type="password" placeholder="Password" class="form-control"
                                                            name="password" required>
                                                        @if ($errors->has('password') && $not_reset_password)
                                                            <span class="help-block">
                                                            <strong>{{ $errors->first('password') }}</strong>
                                                        </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-8 col-sm-offset-2">
                                                        <label for="chkRemeber" style="padding: 0;">
                                                            <input id="chkRemeber" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }} style="width: inherit;"> Remember Me
                                                        </label>
                                                        <a class="btn btn-link pull-right" href="{{ route('password.request') }}" id="forgot-password" style="padding: 0;">
                                                            Forgot Your Password?
                                                        </a>
                                                    </div>
                                                    <div class="col-sm-8 col-sm-offset-2">
                                                    

                                                        <button type="submit" class="btn btn-primary form-control btn-lg" style="    border-radius: 10px;height: 50px;margin-top: 34px;">
                                                            Login
                                                        </button>
                                                        
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="tab-pane" id="Registration" style="padding: 0 20px;">
                                            <form class="form-horizontal" method="POST" action="{{ route('registerAction') }}">
                                                {{ csrf_field() }}
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <div class="row">
                                                            <div class="col-md-3 col-sm-4 col-xs-4">
                                                                <select class="form-control" name="salutation">
                                                                    <option>Mr.</option>
                                                                    <option>Ms.</option>
                                                                    <option>Mrs.</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-9 col-sm-8 col-xs-8">
                                                                <input id="name" type="text" class="form-control" name="name"
                                                                    placeholder="Name" value="{{ old('name') }}" required
                                                                    autofocus>
                                                                @if ($errors->has('name'))
                                                                    <span class="help-block">
                                                                    <strong>{{ $errors->first('name') }}</strong>
                                                                </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                     <!--<label for="email" class="col-sm-2 control-label">Email</label> -->
                                                    <div class="col-sm-6">
                                                        <input type="email" class="form-control" name="email" placeholder="Email"
                                                            value="{{ old('email') }}" required>
                                                        @if ($errors->has('email') && $not_reset_password)
                                                            <span class="help-block">
                                                            <strong>{{ $errors->first('email') }}</strong>
                                                        </span>
                                                        @endif
                                                    </div>
                                                     <!--<label for="mobile" class="col-sm-2 control-label">Mobile</label> -->
                                                    <div class="col-sm-6 mobile-number">
                                                        <input id="mobile" type="number" class="form-control" name="mobile"
                                                            placeholder="Mobile" value="{{ old('mobile') }}" required>
                                                        @if ($errors->has('mobile'))
                                                            <span class="help-block">
                                                            <strong>{{ $errors->first('mobile') }}</strong>
                                                        </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                     <!--<label for="country" class="col-sm-2 control-label">Country</label> -->
                                                    <div class="col-sm-6 mobile-number">
                                                        <input id="country" type="text" class="form-control" name="country"
                                                            placeholder="Country" value="{{ old('country') }}" required>
                                                        @if ($errors->has('country'))
                                                            <span class="help-block">
                                                            <strong>{{ $errors->first('zip_code') }}</strong>
                                                        </span>
                                                        @endif
                                                    </div>
                                                    <div class="col-sm-6 mobile-number">
                                                        <input id="zip_code" type="number" class="form-control" name="zip_code"
                                                            placeholder="Zip code" value="{{ old('zip_code') }}" required>
                                                        @if ($errors->has('zip_code'))
                                                            <span class="help-block">
                                                            <strong>{{ $errors->first('zip_code') }}</strong>
                                                        </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <!-- <div class="form-group">-->
                                                <!--    <label for="zip_code" class="col-sm-2 control-label">Zip code</label>-->
                                                <!--</div> -->

                                                <div class="form-group">
                                                     <!--<label for="city" class="col-sm-2 control-label">City</label> -->
                                                    <div class="col-sm-6 mobile-number">
                                                        <input id="city" type="text" class="form-control" name="city"
                                                            placeholder="City" value="{{ old('city') }}" required>
                                                        @if ($errors->has('city'))
                                                            <span class="help-block">
                                                            <strong>{{ $errors->first('city') }}</strong>
                                                        </span>
                                                        @endif
                                                    </div>
                                                    <div class="col-sm-6 mobile-number">
                                                        <input id="state" type="text" class="form-control" name="state"
                                                            placeholder="State" value="{{ old('state') }}" required>
                                                        @if ($errors->has('state'))
                                                            <span class="help-block">
                                                            <strong>{{ $errors->first('state') }}</strong>
                                                        </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <!-- <div class="form-group">-->
                                                <!--    <label for="state" class="col-sm-2 control-label">State</label>-->
                                                <!--</div> -->
                                                <div class="form-group">
                                                     <!--<label for="address" class="col-sm-2 control-label">Address</label> -->
                                                    <div class="col-sm-6 mobile-number">
                                                        <input id="address" type="text" class="form-control" name="address"
                                                            placeholder="Address" value="{{ old('address') }}" required>
                                                        @if ($errors->has('address'))
                                                            <span class="help-block">
                                                            <strong>{{ $errors->first('address') }}</strong>
                                                        </span>
                                                        @endif
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input id="datepicker" class="form-control" type="text" name="dateofbirth" placeholder="Date of birth" value="{{ old('dateofbirth') }}" required  data-provide="datepicker-inline" data-date-format="dd/mm/yyyy">

                                                        @if ($errors->has('dateofbirth'))
                                                            <span class="help-block">
                                                            <strong>{{ $errors->first('dateofbirth') }}</strong>
                                                        </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <!-- <div class="form-group">-->
                                                <!--    <label for="dateofbirth" class="col-sm-2 control-label">Date of birth</label>                                                    -->
                                                <!--</div> -->
                                                <div class="form-group">
                                                     <!--<label for="password" class="col-sm-2 control-label">Password</label> -->
                                                    <div class="col-sm-6">
                                                        <input type="password" class="form-control" name="password"
                                                            placeholder="Password" required>
                                                        @if ($errors->has('password') && $not_reset_password)
                                                            <span class="help-block">
                                                            <strong>{{ $errors->first('password') }}</strong>
                                                        </span>
                                                        @endif
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input id="password-confirm" type="password" class="form-control"
                                                            name="password_confirmation" placeholder="Confirm password" required>
                                                    </div>
                                                </div>
                                                 <div class="form-group">
                                                    <!-- <label for="password" class="col-sm-2 control-label">Password</label> -->
                                                    <div class="col-sm-12">
                                                        <label for="chkRemeber" style="padding: 0;">
                                                            <input id="chkRemeber" type="checkbox" name="shop_owner" {{ old('remember') ? 'checked' : '' }} style="width: inherit;"> I am a Shop Owner
                                                        </label>
                                                    </div>

                                                </div>
                                                <!-- <div class="form-group">-->
                                                <!--    <label for="password" class="col-sm-2 control-label">Confirm Password</label>-->
                                                <!--</div> -->
                                                <div class="row">
                                                    <div class="text-center">
                                                        <button type="submit" class="submit btn btn-primary btn-sm" style="    border-radius: 5px;">
                                                            Save & Continue
                                                        </button>
                                                        <button type="reset" class="submit btn btn-default btn-sm" style="    border-radius: 5px;">
                                                            Cancel
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--//Login modal-->
    </div>
</header>