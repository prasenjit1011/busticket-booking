<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\User;
use App\AutoRecharge;


class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function test(Request $request)
    {
        $userCount = User::count();
        
        return view('admin', compact('userCount'));
    }
    
    public function userShow(Request $request)
    {
        $users = User::get();
        
        return view('user_show', compact('users'));
    }
    
    public function autoRechargeShow()
    {
        $autoRecharges = AutoRecharge::get();
        
        return view('admin.auto_recharge_show', compact('autoRecharges'));
    }
}
