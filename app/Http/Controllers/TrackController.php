<?php

namespace App\Http\Controllers;

use App\Mail\UserRegistration;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\TrackModel;
use App\User;




class TrackController extends Controller
{
	public function index()
	{
		$user = Auth::user();

		if (!isset($user)) {
			return redirect()->intended('admin/login');
		} else {
			if ($user->role == 1) {
				return redirect()->intended('admin/login');
			} else {
				$flag = "home";
				return view('tracking_end.home', compact('flag', 'user'));
			}
		}		
	}

	public function TrackAdd()
	{
		$user = Auth::user();

		if (!isset($user)) {
			return redirect()->intended('admin/login');
		} else {
			if ($user->role == 1) {
				return redirect()->intended('admin/login');
			} else {
				$flag = "track_add";
				return view('tracking_end.TrackAdd', compact('flag', 'user'));
			}
		}		
	}
	public function TrackSearch()
	{

		$user = Auth::user();

		if (!isset($user)) {
			return redirect()->intended('admin/login');
		} else {
			if ($user->role == 1) {
				return redirect()->intended('admin/login');
			} else {
				$flag = "track_search";
				$track_data = TrackModel::get();
				return view('tracking_end.TrackSearch', compact('flag', 'track_data', 'user'));
			}
		}
	}

	public function add_user()

	{
		$user = Auth::user();

		if (!isset($user)) {
			return redirect()->intended('admin/login');
		} else {
			if ($user->role == 1) {
				return redirect()->intended('admin/login');
			} else {
				$flag = "user_add";
				return view('tracking_end.UserAdd', compact('flag', 'user'));
			}
		}
	}

	public function list_user()
	
	{

		$user = Auth::user();

		if (!isset($user)) {
			return redirect()->intended('admin/login');
		} else {
			if ($user->role == 1) {
				return redirect()->intended('admin/login');
			} else {
				$flag = "user_list";
				$user_data = User::where('role', 2)->get();
				return view('tracking_end.UserList', compact('flag', 'user', 'user_data'));
			}
		}
	}
	

	public function add_track(Request $request) {

		if ($request->ajax()) {
			Auth::login(Auth::user());
			$user_obj=Auth::user();
			$input = $request->all();
			$input['created_by'] = $user_obj['name'];
			var_dump($input);die;
			TrackModel::create($input);
			return 'success';
		}

	}

	public function user_add(Request $request) {

		if ($request->ajax()) {
			$data = $request->all();
			$email_test = User::where('email', $data['email'])->get()->first(); 
			if($email_test)
				return 'error';
			else{
				User::create([
					'name' => $data['name'],
					'email' => $data['email'],
					'address' => $data['address'],
					'password' => bcrypt($data['password']),
					'role' => 2,
					
				]);
				return 'success';

			}

		}
	}



	public function deletePackage(Request $request) {

		if ($request->ajax()) {
			$track = TrackModel::find($request->id);
			TrackModel::destroy($track->id);
			return 'success';
		}

	}

	public function getPackage(Request $request) {

		if ($request->ajax()) {
			$track = TrackModel::find($request->id);
        	// var_dump($track);die;
			return view('tracking_end.editTrack',  compact('track'));
		}

	}

	public function updateTrack(Request $request) {

		if ($request->ajax()) {

			// var_dump($request->all());die;
			$track = TrackModel::find($request->id);
			$input = $request->all();
			$track->update($input);
			return 'success';
		}

	}

	public function login(){
		$user = Auth::user();
		if (isset($user)) {
			if ($user->role == 1) {
				return view('login');
			} else {
				return redirect()->intended('admin');
			}
		} else {
			return view('login');
		}
	}

	public function loginAction(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);
        if ($validator->fails()) {
            return 'Enter any username and password.';
        }
        $credentials = request(['email', 'password']);
        $credentials['user_type'] = 2;
        $credentials['emailVerification'] = 1;
        $credentials['status'] = true;

        if(!Auth::attempt($credentials))
        {
            return redirect('/')->with('error','Wrong Credentials/Account Not Verified');
        }
        Auth::login(Auth::user());
        $user_obj=Auth::user();
        $request->session()->save();

        return redirect('/')->with('success','Login Success');

    }
    public function registerAction(Request $request)
    {
        $validatedData = $request->validate([
            'name'     => 'required|string|max:255|min:3',
            'email'    => ['required', 'max:255', 'email', 'regex:/(.*)@(gmail|yahoo|outlook|yandex)\.(com|co.in)/i', 'unique:users'],
            'password' => 'required|string|min:6|confirmed',
            'mobile'   => 'required|numeric|digits:10|unique:users',
        ]);

        $validator = Validator::make($request->all(), [
            'fname' => 'required|string',
            'midname' => 'string',
            'lname' => 'required|string',
            'email' => 'required|string|email|unique:users|unique:clients',
            'phone' => 'required|string|unique:clients|max:12|min:12',
            'gender'=> 'required',
            'password' => 'required|string|confirmed'
        ]);
        
        
        if (is_null($request->input('shop_owner')))
        {
            $shopOwner = 0;
        }else
        {
            $shopOwner = 1;
        }


        $user = User::create([
            'name'          => $request->input('name'),
            'email'         => $request->input('email'),
            'salutation'    => $request->input('salutation'),
            'mobile'        => $request->input('mobile'),
            'city'          => $request->input('city'),
            'zip_code'      => $request->input('zip_code'),
            'country'       => $request->input('country'),
            'address'       => $request->input('address'),
            'date_of_birth' => $request->input('dateofbirth'),
            'state'         => $request->input('state'),
            'shop_owner'    => $shopOwner,
            'isVerified'    => 1,
            'remember_token' => str_random(60),
            'password'      => bcrypt($request->input('password')),
        ]);

        if (!$user)
        {
            return redirect('/')->with('error','An unexpected error occurred.Please try again.');
        }

        Mail::to($user)->send(new UserRegistration($user));

        return redirect('/')->with('success','Registration Success. A verification email has been sent to you email account.');
    }
  

    public function userAccountActivation($token)
    {
        $verifyUser = User::where('remember_token',$token)->first();

        if (!$verifyUser)
        {
            return redirect('/')->with('error','Wrong Validation token.Contact Support at support@zapwallet.com');
        }

        $six_digit_random_number = rand(100000, 999999);

        $updateUser = User::where('remember_token',$token)->update([
            'otp' => $six_digit_random_number,
            'emailVerification' => 1
        ]);

        if (!$updateUser)
        {
            return redirect('/')->with('error','An unexpected error occurred.Contact Support at support@zapwallet.com');
        }

        $mobile = $verifyUser['mobile'];
       

        $apiKey = urlencode('UYYXwhqCy5Y-LRDM5SGYWTLg5QI9aFU6ENi3heNhhj');

        // Message details
        $numbers = $mobile;
        $sender  = urlencode('ZAPWAL');
        $message = rawurlencode('Use ' . $six_digit_random_number . ' as your mobile number verification OTP. OTP is confidential. Please do not share it with others. www.zapwallet.in');

        // $numbers = implode(',', $numbers);

        // Prepare data for POST request
        $data = ['apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $message];

        // Send the POST request with cURL
        $ch = curl_init('https://api.textlocal.in/send/');
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $response = curl_exec($ch);
        curl_close($ch);
        
      

        return redirect('/')->with('success','Account Verified.Please login and enter the verification code sent to you.');
    }

	public function login_check(Request $request){

		if ($request->ajax()) {
			if ($request->username == '' || $request->password == '') {
				return 'Enter any username and password.';
			}
			else {
				$login=Auth::attempt([

					"name"=>$request->get('username'),
					"password"=>$request->get('password'),
                    'user_type'=>2,
                    'status'=>true

				]);

				if($login){

					Auth::login(Auth::user());
					$user_obj=Auth::user();
					$request->session()->save();

					if($user_obj->role == "1"){                    
						return "You can't access admin page!!!";
					} else {
						return 'success';
					}

				}
				else{
					return "Invalid email or password";
				}
			}		
		}
	}

	public function logout(){

		\Illuminate\Support\Facades\Auth::logout();
		return redirect("admin/login");
	}


}
