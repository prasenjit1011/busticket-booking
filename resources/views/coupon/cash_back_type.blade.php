<div class='form-group'>
    <label class='control-label col-xs-3 col-sm-3'>Cash Back Type:</label>
    <div class='col-xs-9 col-sm-9'>
        <label class='radio-inline'><input type='radio' name='cash_back_type' id='flat_cash_back' value='Flat Cash Back'>Flat Cash Back</label>
    	<label class='radio-inline'><input type='radio' name='cash_back_type' id='per_cash_back' value='Percentage Cash Back'>Percentage Cash Back</label>
    </div>
</div>

<script>
    $(document).ready(function(){

        $("#flat_cash_back").change(function(){
            var flat_cash_back = $("#flat_cash_back").val();
            if (flat_cash_back == 'Flat Cash Back') {
                //alert(flat_cash_back);
                $('#radio_info_show').html(
            		"<div class='form-group'>"+
				        "<label class='control-label col-sm-3' >Flat Cash Back Amount:</label>"+
				        "<div class='col-xs-9 col-sm-9'>"+
			        		"<input type='number' class='form-control'  placeholder='Enter Flat Cash Back Amount' name='flat_cash_back'>"+
				        "</div>"+
					"</div>"
            	);
            }     
        });

        $("#per_cash_back").change(function(){
            var per_cash_back = $("#per_cash_back").val();
            if (per_cash_back == 'Percentage Cash Back') {
                //alert(per_discount);
                $('#radio_info_show').html(
            		"<div class='form-group'>"+
				        "<label class='control-label col-sm-3' >Cash Back in Percentage:</label>"+
				        "<div class='col-xs-9 col-sm-9'>"+
			        		"<input type='number' class='form-control'  placeholder='Enter Cash Back in Percentage' name='per_cash_back'>"+
				        "</div>"+
					"</div>"+

					"<div class='form-group'>"+
				        "<label class='control-label col-sm-3'>Max Cash Back:</label>"+
				        "<div class='col-xs-9 col-sm-9'>"+
			        		"<input type='number' class='form-control'  placeholder='Enter Max Cash Back' name='max_cash_back'>"+
				        "</div>"+
					"</div>"
            	);
            }      
        });
    });

</script>