@extends('blades.base')



{{-- @section('page title', 'Index') --}}

@section('css')
<link rel="stylesheet" href="{{ asset('css/custom.css')}}">
@endsection

@section('innerbanner')

<!-- innerbanner -->    

<div class="banner">
	<s-banner></s-banner>
</div>

<!-- //innerbanner -->

@endsection



@section('breadcrumbs')

@endsection



@section('horizontal tab')



@endsection



@section('vertical tab')





	<div class="w3layouts-breadcrumbs text-center">

		<div class="container">

			<span class="agile-breadcrumbs"><a href="/"><i class="fa fa-home home_1"></i></a> / <span>About</span></span>

		</div>

	</div>

	<!-- //breadcrumbs -->



	<!-- About-page -->

	   <div class="terms w3ls-about w3layouts-content">

		<div class="container">

			<h3 class="w3-head">About Us</h3>
			<h6>What is Zapwallet?</h6>

			<b>Quick & Fast:</b>
			<p>The term Zap means “Quick”.</p>
			<p>We tend to provide quickest recharges in just under 3 Clicks.</p>

			<b>Auto-Recharge:</b>
			<p>Zapwallet also tries to provide auto recharge to your prepaid mobile using the Auto Recharge system.</p>

			<b>Bus Tickets:</b>
			<p>Zapwallet provides Bus Tickets for 146000+ Routes and 2000+ Bus Operators.</p>

			<b>Shopkeeper Earnings:</b>
			<p>Zapwallet assures monthly earnings of around Rs.500/- to Rs.50000/- to each and every Shopkeepers who do cross the maximum amount of transactions.</p>

			<h6>Why Use Zapwallet?</h6>
			<ol start="1">
				<li>We recommend each and everyone to use zapwallet for its quickness. Recharge done in just 2 clicks.</li>
				<li>If case of any unnecessary deduction of money, refund is made within 3 – 21 working days.</li>
				<li>Quickest customer support. The First Company to provide whatsapp support.</li>
			</ol>
			<h6>We Promise:</h6>
			<ol start="1">
				<li>Good Customer Support.</li>
				<li>Quickest Service.</li>
				<li>Assured Earnings to all Shopkeepers.</li>
				<li>Discounts and Cashbacks on all Recharge and Bus tickets you book using your debit card or zapwallet.</li>
			</ol>




	</div>

</div>

    <!--//About-page-->











			<!-- start-smoth-scrolling -->

		<!-- //here ends scrolling icon -->

@endsection



@section('tab title')

    Plan

@endsection