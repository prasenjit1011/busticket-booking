<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    
    public function customReset(Request $request, PasswordBroker $passwords) {
    if ($request->ajax()) {
      $this->validate($request, ['email' => 'required|email']);

      $response = $passwords->sendResetLink($request->only('email'));

      switch ($response) {
        case PasswordBroker::RESET_LINK_SENT:
          return [
            'error' => false,
            'msg'   => 'A password link has been sent to your email address',
          ];

        case PasswordBroker::INVALID_USER:
          return [
            'error' => true,
            'msg'   => "We can't find a user with that email address",
          ];
      }
    }
    return false;
  }
}
