export const MOBILE_TABS = {
    prepaid: 'prepaid-tab',
    postpaid: 'postpaid-tab'
}

export const SERICES_TABS = {
    MOBILE: 'mobile-tab',
    TDH: 'tdh-tab',
    GAS: 'gas-tab',
    BUS: 'bus-tab',
    DATA_CARD: 'data-card-tab',
    WATER: 'water-tab',
    LAND_LINE: 'land-line-tab',
    BOARD: 'board-tab',
    ELECTRICITY: 'electricity-tab',
}

export const MANTIS_CONFIG = {
    ClientId: 7605,
    ClientSecret: '78a83fbe2544114407d54b4fe383f689'
}

export const BUS_TABS = {
    dropoff: 'dropoff',
    passenger: 'passenger',
    pickup: 'pickup',
    seat_chart: 'seat_chart',
    your_info: 'your_info',
    policy: 'policy',
}
