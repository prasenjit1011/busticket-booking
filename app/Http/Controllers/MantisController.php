<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Common;

class MantisController extends Controller
{
    public function authentication() {
        $url = 'http://api.iamgds.com/ota/Auth';
        $data = [
            'ClientId' => config('zapwall.client_id'),
            'ClientSecret' => config('zapwall.client_secret'),
        ];
        $response = Common::curl('POST', $url, $data);
        return $response->getContents();
    }
}
