<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Broadbandtransaction extends Model
{
    protected $fillable = [
      'order_id',
      'currency',
      'amount',
      'mn'
    ];
    public function users (  ) {
        return $this->belongsTo('App\User');
    }
}
