@extends('blades.base')
@section('header-right')
    <div class=" header-right test-skladoptcom">
        <div class="banner">
            <s-banner></s-banner>
        </div>
    </div>
@endsection
@section('css')
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="{{ asset('css/travel.css')}}">
    <link rel="stylesheet" href="{{ asset('css/custom.css')}}">
    <link rel="stylesheet" href="{{ asset('css/tool-tip.css')}}">
@endsection
@section('innerbanner')
    <!-- innerbanner -->
    {{-- <div class="agileits-inner-banner">

    </div> --}}
    <!-- //innerbanner -->
@endsection

@section('breadcrumbs')
@endsection


@section('header-right')
    <div class=" header-right">
        <div class="banner">
            <div class="slider">
                <div class="callbacks_container">
                    <ul class="rslides" id="slider">
                        <li>
                            <div class="banner1">
                                <div class="caption">
                                    <h3><span>Get a</span> coupon voucher on Mobile Recharge</h3>
                                    <p><a href="#mobilew3layouts" class="scroll">Recharge now</a></p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="banner2">
                                <div class="caption">
                                    <h3><span>50% off</span> on train Tickets</h3>
                                    <p><a href="train.html">Book now</a></p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="banner3">
                                <div class="caption">
                                    <h3><span>Flat Rs.200 Cash back</span>  on Movie Tickets</h3>
                                    <p><a href="movies.html">Book now</a></p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="banner4">
                                <div class="caption">
                                    <h3><span>Upto Rs.125 Discount </span> & Flat 100% Money Back</h3>
                                    <p><a href="bus.html">Book now</a></p>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('vertical tab')


<div class="container" style="margin-top: 20px;">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Reset Password</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Send Password Reset Link
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
