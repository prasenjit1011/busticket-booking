@extends('layouts.app_admin')

@section('content')

<div class="content-wrapper">
    
    <section class="content-header">
        <h1>
            Auto Recharges History
        	<small>Preview page</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Auto Recharges History</li>
        </ol>
    </section>
    
    <section class="content">
        <div class="row">
        	<div class="col-md-12">
            	<div class="box box-success">
                	<div class="box-header">
                    	<h3 class="box-title">Auto Recharges History</h3> 
                	</div>
                	<div class="box-body">

                        <div class="table-responsive"> 
                            <table id="example" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>SL</th>
                                        <th>Mobile</th>
                                        <th>Amount</th>
                                        <th>Recharge At</th>
                                        <th>Operator</th>
                                        <th>Circle</th>
                                        <th>Created At</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    $i = 0;
                                ?>
                                @foreach($autoRecharges as $autoRecharge)
                                    <?php
                                        if($autoRecharge->status == '1') {
                                            $status = 'Yes';
                                        } else {
                                            $status = 'No';
                                        }
                                    ?>
                                    <tr>
                                        <td>{{ ++$i }}</td>
                                        <td>{{ $autoRecharge->mobile }}</td>
                                        <td>{{ $autoRecharge->balance }}</td>
                                        <td>{{ $autoRecharge->recharged_at }}</td>
                                        <td>{{ $autoRecharge->operator }}</td>
                                        <td>{{ $autoRecharge->circle }}</td>
                                        <td>{{ $autoRecharge->created_at }}</td>
                                        <td>{{ $status }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                	</div>
          		</div>
        	</div>
      	</div>
    </section>

</div>

@endsection

@section('style')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">
@endsection


@section('script')
    <script src="{{ asset('https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#example').DataTable();
        } );
    </script>
@endsection