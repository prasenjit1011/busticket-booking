<?php

namespace App\Http\Controllers\Admin;

use App\Models\CashbackTracking;
use App\models\Dthtransaction;
use App\models\Mobiletransaction;
use App\models\Postpaidtransaction;
use App\models\Transaction;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
    public function index()
    {
        $users = User::where('user_type',2)->get();

        return view('admin.users.index',['users'=>$users]);
    }

    public function cashback($id)
    {
        $userDetails = User::where('id',$id)->first();

        return view('admin.users.cashback',['user'=>$userDetails]);
    }
    public function updateCashBack(Request $request,$id)
    {
        $validatedData = $request->validate([
            'balance' => 'required',
            'remark' => 'required'
        ]);

        $updateBalance = User::where('id',$id)->increment('wallet',$request->input('balance'));

        $insertTracking = CashbackTracking::create([
            'admin_id'=> Auth::user()->id,
            'user_id'=>$id,
            'remark'=>$request->input('remark'),
            'amount'=>$request->input('balance')
        ]);

        if (!$updateBalance AND !$insertTracking)
        {
            return back()->withInput()->with('error','An Unexpected error Occurred.Please Try again.');
        }

        return back()->withInput()->with('success','Cash Back value Submitted.');


    }
    public function transactions($id)
    {
        $mobileTransactions = Mobiletransaction::where('user_id',$id)->get();
        $walletTransactions = Transaction::where('user_id',$id)->get();
        $dthTransactions = Dthtransaction::where('user_id',$id)->get();
        $postPaidTransactions = Postpaidtransaction::where('user_id',$id)->get();
        $cashbacks = CashbackTracking::where('user_id',$id)->get();
        $user = User::find($id);
        return view('admin.users.transactions',[
            'mobileTransactions'=>$mobileTransactions,
            'walletTransactions'=>$walletTransactions,
            'dthTransactions'=>$dthTransactions,
            'postPaidTransactions'=>$postPaidTransactions,
            'cashbacks'=>$cashbacks,
            'user'=>$user
        ]);
    }
    public function suspend($id)
    {
        $query = User::where('id',$id)->update([
            'status'=>false
        ]);

        if (!$query)
        {
            return back()->with('error','An unexpected error occurred');
        }
        return back()->with('success','User Suspended successfully.');
    }
    public function activate($id)
    {
        $query = User::where('id',$id)->update([
            'status'=>true
        ]);

        if (!$query)
        {
            return back()->with('error','An unexpected error occurred');
        }
        return back()->with('success','User Activated successfully.');
    }
    public function delete($id)
    {
        $query = User::find($id);
        $query->delete();

        if (!$query)
        {
            return back()->with('error','An unexpected error occurred');
        }
        return back()->with('success','User Deleted successfully.');
    }
    public function expenditures()
    {
        $users = User::where('user_type',2)->get();

        return view('admin.users.expenditure',['users'=>$users]);
    }
}
