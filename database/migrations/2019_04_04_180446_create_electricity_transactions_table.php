<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateElectricityTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('electricitytransactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('order_id');
            $table->string('reqid')->nullable();
            $table->string('status')->nullable();;
            $table->string('remark')->nullable();;
            $table->string('amount')->nullable();
            $table->string('currency');
            $table->string('balance')->nullable();;
            $table->string('mn');
            $table->string('ec')->nullable();;
            $table->string('field1')->nullable();;
            $table->integer('operator')->nullable();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('electricitytransactions');
    }
}