@extends('admin.layout.layout')
@section('content')

        <!-- partial -->
        <div class="page-content-wrapper">
            <div class="page-content-wrapper-inner">
                <div class="viewport-header">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="#">User Management {{ $users->count() }}</a></a></li>
                        </ol>
                    </nav>
                </div>
                <div class="content-viewport">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="grid">
                                <p class="grid-header">User Management {{ $users->count() }}</p>
                                <div class="item-wrapper">
                                    <div class="table-responsive">
                                        @include('admin.partials.info')
                                        <table  class="data-table table table-striped" id="transactionsTable">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>User Id</th>
                                                    <th>Type</th>
                                                    <th>Wallet</th>
                                                    <th>Zapbucks</th>
                                                    <th>Mobile</th>
                                                    <th>Email</th>
                                                    <th>Country</th>
                                                    <th>City</th>
                                                    <th>Zip Code</th>
                                                    <th>State</th>
                                                    <th>Address</th>
                                                    <th>D.O.B</th>
                                                    <th>Status</th>
                                                    <th>Transactions</th>
                                                    <th>Action</th>
                                                
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($users as $user)
                                                <tr>
                                                        <th>{{  $user->name }}</th>
                                                        <th>{{ $user->id }}</th>
                                                        <th>
                                                            @if ($user->shop_owner == true)
                                                                <span>Shop Owner</span>
                                                            @else
                                                                <span>User</span>
                                                            @endif
                                                        </th>
                                                        <td>{{  $user->wallet }} - <a class="btn btn-sm btn-success" href="{{  route('admin.user.cashback',$user->id) }}"> <i class="fa fa-arrow-circle-right"></i></a></td>
                                                        <td>{{  $user->zapbucks }}</td>
                                                        <td>{{  $user->mobile }}</td>
                                                        <td>{{  $user->email }}</td>
                                                        <td>{{  $user->country }}</td>
                                                        <td>{{  $user->city }}</td>
                                                        <td>{{  $user->zip_code }}</td>
                                                        <td>{{  $user->state }}</td>
                                                        <td>{{ $user->address }}</td>
                                                        <td>{{  $user->date_of_birth }}</td>
                                                        <th>                                                          
                                                            @if ($user->status == true)
                                                            <span class="label label-success">Active</span>
                                                            @else
                                                            <span class="label label-warning">Suspended</span>
                                                            @endif
                                                        </th>
                                                        <th>
                                                            <a class="btn btn-success btn-sm rounded" href="{{ route('admin.user.transactions',$user->id) }}"><i class="fa fa-list"></i> Transactions</a>
                                                        </th>
                                                        <th>
                                                            @if ($user->status == true)
                                                                <a class="btn btn-info btn-sm rounded" href="{{ route('admin.user.suspend',$user->id) }}"><i class="fa fa-recycle"></i></a>
                                                            @else
                                                            <a class="btn btn-success btn-sm rounded" href="{{ route('admin.user.activate',$user->id) }}"><i class="fa fa-check"></i></a>
                                                            @endif
                                                            <a class="btn btn-danger btn-sm rounded" href="{{ route('admin.user.delete',$user->id) }}"><i class="fa fa-trash"></i></a>
                                                        </th>
                                                        
                                                    </tr>
                                                @endforeach
                                                
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                    
                                   
                                </div>
                                
                            </div>
                            
                        </div>
                        
                               
                            </footer>
                    </div>
                </div>
            </div>
     @endsection