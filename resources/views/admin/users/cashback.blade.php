@extends('admin.layout.layout')
@section('content')
        <div class="page-content-wrapper">
            <div class="page-content-wrapper-inner">
                <div class="viewport-header">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="#">Users</a></li>
                            <li class="breadcrumb-item active" aria-current="page">{{ $user->name }}</li>
                        </ol>
                    </nav>
                </div>
                <div class="content-viewport">
                    <div class="row">
                        
                        <div class="col-lg-12 equel-grid">
                            <div class="grid">
                                <p class="grid-header">Update Cashback for <b style="color:blue">{{  $user->name }} - Current Wallet {{  $user->wallet }}</b></p>
                                <div class="grid-body">
                                    <div class="item-wrapper">
                                            @include('admin.partials.info')
                                        <form method="POST" action="{{  route('admin.user.cashback.submit',$user->id) }}">
                                                {{ csrf_field() }}
                                            <div class="form-group row showcase_row_area">
                                                <div class="col-md-3 showcase_text_area">
                                                    <label for="inputEmail4">Cash Back Value</label>
                                                </div>
                                                <div class="col-md-9 showcase_content_area">
                                                    <input type="number" class="form-control" name="balance"  placeholder="Cash Back Value">
                                                    @if ($errors->has('balance'))
                                                    <span class="help-block">
                                                <strong>{{ $errors->first('balance') }}</strong>
                                            </span>

                                                @endif
                                                </div>
                                            </div>
                                            <div class="form-group row showcase_row_area">
                                                <div class="col-md-3 showcase_text_area">
                                                    <label for="inputEmail4">Remark</label>
                                                </div>
                                                <div class="col-md-9 showcase_content_area">
                                                    <textarea class="form-control" placeholder="Remark" name="remark"></textarea>
                                                    @if ($errors->has('balance'))
                                                        <span class="help-block">
                                                <strong>{{ $errors->first('balance') }}</strong>
                                            </span>

                                                    @endif
                                                </div>
                                            </div>
                                           
                                            <div class="form-group row showcase_row_area">
                                                    <div class="col-md-3 showcase_text_area">
                                                        
                                                    </div>
                                                    <div class="col-md-9 showcase_content_area">
                                                            <button style="text-align:center" type="submit" class="btn btn-sm btn-primary">Submit Cashback</button>
                                                    </div>
                                                </div>
                                           
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                 
                    </div>
                </div>
            </div>
        @endsection