<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Auth;
use DB;
use Illuminate\Http\Request;

class WalletController extends Controller
{

    public function __construct () {

    }

    public function isVerified () {
        $user = Auth::user();
        if ( $user->isVerified == '1' ) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    public function RechargeWallet ( Request $request ) {

        if ( Auth::user() ) {
            $amount       = $request->get('amount');
            $pre_amount   = $request->get('pre_amount');
            $total_amount = $amount + $pre_amount;
            dd($total_amount);
            $user = Auth::user();
            $id   = $user->id;
            $res  = DB::table('wallet')->where('user_id', $id)->update(['total_amount' => $total_amount]);

            return redirect()->back()->with('msg', 'Wallet updated successfully!');
        }
        else {
            return redirect()->back()->with('msg', 'Please login to recharge wallet!');
        }
        //}
    }
}