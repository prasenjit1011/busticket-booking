<footer>

    <div class="container-fluid">

        <div class="w3-agile-footer-top-at">

            <div class="col-md-2 agileits-amet-sed">

                <h4>Company</h4>

                <ul class="w3ls-nav-bottom">

                    <li><a href="{{url('/frontend/aboutus')}}">About Us</a></li>

                    {{--<li><a href="{{url('/frontend/bus')}}">bus</a></li>--}}

                    <li><a href="{{url('/frontend/support')}}">Support</a></li>

                    <li><a href="{{url('/frontend/sitemap')}}">Sitemap</a></li>

                    <li><a href="{{url('/frontend/terms')}}">Terms & Conditions</a></li>

                    <li><a href="{{url('/frontend/faq')}}">Faq</a></li>

                    <li><a href="{{url('/feedback')}}">Feedback</a></li>

                    <li><a href="{{url('/frontend/contact')}}">Contact</a></li>

                    <li><a href="{{ route('frontend.privacy_policy') }}">Privacy policy</a></li>
                    <li><a href="{{ route('frontend.refund_policy') }}">Refund policy</a></li>


                {{--    <li><a href="shortcodes.html">Shortcodes</a></li>

                    <li><a href="icons.html">Icons Page</a></li>--}}


                </ul>

            </div>

            <div class="col-md-3 agileits-amet-sed ">

                <h4>Mobile Recharges</h4>

                <ul class="w3ls-nav-bottom">

                    <li><a href="/#parentVerticalTab1">Airtel</a></li>

                    <li><a href="/#parentVerticalTab1">Aircel</a></li>

                    <li><a href="/#parentVerticalTab1">Vodafone</a></li>

                    <li><a href="/#parentVerticalTab1">BSNL</a></li>

                    <li><a href="/#parentVerticalTab1">Tata Docomo</a></li>

                    <li><a href="/#parentVerticalTab1">Reliance GSM</a></li>

                    <li><a href="/#parentVerticalTab1">Reliance CDMA</a></li>

                    <li><a href="/#parentVerticalTab1">Telenor</a></li>

                    <li><a href="/#parentVerticalTab1">MTS</a></li>

                    <li><a href="/#parentVerticalTab1">Jio</a></li>

                </ul>

            </div>

            <div class="col-md-3 agileits-amet-sed ">

                <h4>DATACARD RECHARGES</h4>

                <ul class="w3ls-nav-bottom">

                    <li><a href="/#parentVerticalTab3">Tata Photon</a></li>

                    <li><a href="/#parentVerticalTab3">MTS MBlaze</a></li>

                    <li><a href="/#parentVerticalTab3">MTS MBrowse</a></li>

                    <li><a href="/#parentVerticalTab3">Airtel</a></li>

                    <li><a href="/#parentVerticalTab3">Aircel</a></li>

                    <li><a href="/#parentVerticalTab3">BSNL</a></li>

                    <li><a href="/#parentVerticalTab3">MTNL Delhi</a></li>

                    <li><a href="/#parentVerticalTab3">Vodafone</a></li>

                    <li><a href="/#parentVerticalTab3">Idea</a></li>

                    <li><a href="/#parentVerticalTab3">MTNL Mumbai</a></li>

                    <li><a href="/#parentVerticalTab3">Tata Photon Whiz</a></li>

                </ul>

            </div>

            <div class="col-md-2 agileits-amet-sed">

                <h4>DTH Recharges</h4>

                <ul class="w3ls-nav-bottom">

                    <li><a href="/#parentVerticalTab2"> Airtel Digital TV Recharges</a></li>

                    <li><a href="/#parentVerticalTab2">Dish TV Recharges</a></li>

                    <li><a href="/#parentVerticalTab2">Tata Sky Recharges</a></li>

                    <li><a href="/#parentVerticalTab2">Reliance Digital TV Recharges</a></li>

                    <li><a href="/#parentVerticalTab2">Sun Direct Recharges</a></li>

                    <li><a href="/#parentVerticalTab2">Videocon D2H Recharges</a></li>

                </ul>

            </div>

            <div class="col-md-2 agileits-amet-sed ">

                <h4>Payment Options</h4>

                <ul class="w3ls-nav-bottom">

                    <li>Credit Cards</li>

                    <li>Debit Cards</li>

                    <li>Any Visa Debit Card (VBV)</li>

                    <li>Direct Bank Debits</li>

                    <li>Cash Cards</li>

                </ul>

            </div>

            <div class="clearfix"></div>

        </div>

    </div>

    <div class="w3l-footer-bottom">

        <div class="container-fluid">

            <div class="col-md-4 w3-footer-logo">

                <h2><a href="/" class="h-logo"><img src="{{asset('images/logo2.png')}}" alt=""></a></h2>

            </div>

            <div class="col-md-8 agileits-footer-class">

                <p>© 2017 Zapwallet. All Rights Reserved</p>

            </div>

            <div class="clearfix"></div>

        </div>

    </div>

</footer>